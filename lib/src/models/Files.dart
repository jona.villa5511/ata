class Files{

  int _id;
  String  _fileName;
  String  _fileUrl;
  String  _fileCoordenadas;
  int     _idUsuario;
  String  _fecha;
  int     _idInventario;
  int     _idProyecto;
  String  _tipo;

  Files(this._id, this._fileName, this._fileUrl, this._fileCoordenadas,
      this._idUsuario, this._fecha, this._idInventario, this._idProyecto, this._tipo);

  int get id => _id;

  set id(int value) {
    _id = value;
  }

  String get fileName => _fileName;

  int get idProyecto => _idProyecto;

  set idProyecto(int value) {
    _idProyecto = value;
  }

  int get idInventario => _idInventario;

  set idInventario(int value) {
    _idInventario = value;
  }

  String get fecha => _fecha;

  set fecha(String value) {
    _fecha = value;
  }

  int get idUsuario => _idUsuario;

  set idUsuario(int value) {
    _idUsuario = value;
  }

  String get fileCoordenadas => _fileCoordenadas;

  set fileCoordenadas(String value) {
    _fileCoordenadas = value;
  }

  String get fileUrl => _fileUrl;

  set fileUrl(String value) {
    _fileUrl = value;
  }

  set fileName(String value) {
    _fileName = value;
  }

  String get tipo => _tipo;

  set tipo(String value) {
    _tipo = value;
  }

  @override
  String toString() {
    return 'Files{_id: $_id, _fileName: $_fileName, _fileUrl: $_fileUrl, _fileCoordenadas: $_fileCoordenadas, _idUsuario: $_idUsuario, _fecha: $_fecha, _idInventario: $_idInventario, _idProyecto: $_idProyecto}';
  }
}