

import 'package:ATA/src/ui/ConfirmCardTransfer_scren.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:ATA/src/complements/globalWidgets.dart';
import 'package:ATA/src/complements/globalVariables.dart';
import 'package:ATA/src/services/APiWebServices/Inventario_wsConsults.dart';
import 'package:ATA/src/ui/AgendaCitas_scren.dart';

import 'AgendaCitas_scren.dart';
class AddCardTransferScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => AddCardTransferScreenState();

}

class AddCardTransferScreenState extends State<AddCardTransferScreen> {
  String _value;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:
      GlobalWidgets.topBar('FORMATO DE PAGO ', 'Marco', context,''),
      backgroundColor: Color.fromRGBO(207, 227, 233, 1),
      body: Material(
        child: Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    child: Column(
                      children: <Widget>[

                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,
                        ),
                        FlatButton(
                          child:
                          Column( // Replace with a Row for horizontal icon + text
                            children: <Widget>[
                              Image(image: new AssetImage('assets/images/previous.png'),
                                  width: 25, height: 25),
                              Text("Regresar",
                                textAlign: TextAlign.center,
                                style:TextStyle(fontWeight: FontWeight.bold,fontSize: 10,color: Color.fromRGBO(9, 99, 141, 1)),
                              )
                            ],
                          ),
                          onPressed: (){
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => AgendaCitasScreen()),
                            );
                          },
                        ),
                        Container(
                          child:  Text('Ingresa una nueva tarjeta', maxLines: 1,  textAlign: TextAlign.center,
                            style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,color: Color.fromRGBO(9, 99, 141, 1))
                          ),
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,

                        ),
                        FlatButton(
                          child: Image(image: new AssetImage('assets/images/tarjeta1.png'),
                              width: 150, height: 150),
                          onPressed: (){


                          },
                        ),
                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,
                        ),


                        getInputNumeric(),

                        Material(
                          elevation: 3.0,
                          borderRadius: BorderRadius.circular(30.0),
                          color: Color.fromRGBO(36, 90, 149, 1),
                          child: MaterialButton(

                            //padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            onPressed: () async {

                              print('OK');
                              if(_formKey.currentState.validate()){

                                print('correcto:');

                                Navigator.pushAndRemoveUntil(
                                  context,
                                  PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                                    return ConfirmCardTransferScreen();
                                  }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                                    return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                                    ).animate(animation), child: child,);
                                  },
                                      transitionDuration: Duration(seconds: 1)
                                  ), (Route route) => false,);


                              }else {

                                showDialog(context: context, builder: (context) => ShowMessageValidateAdd('Existen campo vacios ó el formato es incorrecto!','Favor de validar.'));
                              }
                            },
                            textColor: Colors.white,
                            child: Row( children: <Widget>[

                              new Icon(Icons.credit_card,color: Colors.white),
                              Text("Guardar",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12,color: Colors.white)),
                              new Icon(Icons.arrow_forward_ios,color: Colors.white),
                            ]),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );

  }
   Widget getInputNumeric(){
    return  Container(
      padding: EdgeInsets.fromLTRB(8, 1, 1, 1),
        child: Form(
        key: _formKey,
      child: SizedBox(

        width: double.infinity,
        child: Column(
          children: <Widget>[
            Align (
              alignment: Alignment.topLeft,
              child: Text('Numero de Tarjeta', maxLines: 1,  textAlign: TextAlign.left,
                  style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,color: Color.fromRGBO(9, 99, 141, 1))
              ),
            ),
        TextFormField(
          keyboardType: TextInputType.number,
          textCapitalization: TextCapitalization.characters,
          style: TextStyle(
            color: Colors.black,
          ),
          decoration: new InputDecoration(
            fillColor: Colors.white,
            border: new OutlineInputBorder(
              borderRadius: new BorderRadius.circular(6.0),
              borderSide: new BorderSide(),
            ),
          ),
          controller:  GlobalVariables.controllerTarjeta,
          autovalidate: true,
          maxLength:16,
          validator: (value){

              RegExp regex = new RegExp(r'^\d{16}');
              if (!regex.hasMatch(value))
                return 'Ingrese los 16 numeros de su Tarjeta';
              else
                return null;

          },
        ),

            Align (
              alignment: Alignment.topLeft,
              child: Text('Nombre y Apellido: ', maxLines: 1,  textAlign: TextAlign.left,
                  style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,color: Color.fromRGBO(9, 99, 141, 1))
              ),
            ),
            TextFormField(
              style: TextStyle(
                color: Colors.black,
              ),
              decoration: new InputDecoration(
                fillColor: Colors.white,
                border: new OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(6.0),
                  borderSide: new BorderSide(),
                ),
              ),
              controller:  GlobalVariables.controllerTarjetaNombre,
              autovalidate: true,
              validator: (value){

                RegExp regex = new RegExp(r'^[a-zA-Z ]*$');
                if (!regex.hasMatch(value))
                  return 'Ingrese solo texto';
                else
                  return null;

              },
            ),
            Align (
              alignment: Alignment.topLeft,
              child: Text('Fecha de expiraciòn', maxLines: 1,  textAlign: TextAlign.left,
                  style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,color: Color.fromRGBO(9, 99, 141, 1))
              ),
            ),
            TextFormField(
            onChanged:  (text){
              print('asas' + text);
             setState(() {
               GlobalVariables.controllerTarjetaFechaCaducidad.text =
                   GlobalVariables.controllerTarjetaFechaCaducidad.text.substring(0,2) +'/'+ GlobalVariables.controllerTarjetaFechaCaducidad.text.substring(2,4);
             });
            },
              keyboardType: TextInputType.number,
              textCapitalization: TextCapitalization.characters,
              style: TextStyle(
                color: Colors.black,
              ),
              decoration: new InputDecoration(
                fillColor: Colors.white,
                border: new OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(6.0),
                  borderSide: new BorderSide(),
                ),
              ),
              controller:  GlobalVariables.controllerTarjetaFechaCaducidad,
//              autovalidate: true,
//
//              validator: (value){
//
//                RegExp regex = new RegExp(r'^\d{5}');
//                if (!regex.hasMatch(value))
//                  return 'Fecha de caducidad';
//                else
//                  return null;
//
//              },
            ),
            Align (
              alignment: Alignment.topLeft,
              child: Text('Codigo CVV', maxLines: 1,  textAlign: TextAlign.left,
                  style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,color: Color.fromRGBO(9, 99, 141, 1))
              ),
            ),
            TextFormField(
              keyboardType: TextInputType.number,
              textCapitalization: TextCapitalization.characters,
              obscureText: true,
              style: TextStyle(
                color: Colors.black,
              ),
              decoration: new InputDecoration(
                fillColor: Colors.white,
                border: new OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(6.0),
                  borderSide: new BorderSide(),
                ),
              ),
              controller:  GlobalVariables.controllerTarjetaCVV,
              autovalidate: true,
              maxLength:3,

              validator: (value){

                RegExp regex = new RegExp(r'^\d{3}');
                if (!regex.hasMatch(value))
                  return 'Codigo de seguridad';
                else
                  return null;

              },
            ),
        ]),
        //),
      ),



        ),

    );
  }

}