import 'dart:io';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:ATA/src/blocs/login_bloc.dart';
import 'package:ATA/src/complements/globalWidgets.dart';
import 'package:ATA/src/complements/globalVariables.dart';
import 'package:geolocator/geolocator.dart';
import 'package:ATA/src/ui/AgendaCitas_scren.dart';
class BienvenidaScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => BienvenidaScreenState();

}

class BienvenidaScreenState extends State<BienvenidaScreen> {
  File _pickedImage;
  LatLng _center;

  Position currentLocation;
  final Map<String, Marker> _markers = {};

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    DateTime now1 = DateTime.now();
    var currentTime = new DateTime(now1.year, now1.month, now1.day);
    GlobalVariables.dateNow = currentTime.toString();
    print('Cargando App...');
    return Scaffold(
      appBar:
      GlobalWidgets.topBar(
          'CITAS', 'Marco Antonio Moreno Silva', context,
          '$currentTime'.substring(0, 10)),

      backgroundColor: Colors.black26,

      body: Material(
        child: Container(
          color: Colors.black12,
          child: Column(
            children: <Widget>[
              //------------Body------------
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    child: Column(
                      children: <Widget>[


            Container(
              decoration: BoxDecoration(
                  color:  Color.fromRGBO(0, 132, 186, 1),

                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(0.0),
                      bottomLeft:Radius.circular(40.0),
                      topRight: Radius.circular(0.0),
                      bottomRight: Radius.circular(40.0))),

              child:  Row(

                children: <Widget>[
//                  FlatButton(
//                    onPressed: () => {
//
//              print('dddddd'),
//              _selectDate(context),
//
//
//      },
//                    color: Color.fromRGBO(207, 227, 233, 1),
//                    padding: EdgeInsets.all(20.0),
//                    textColor: Colors.white,
//                    shape: RoundedRectangleBorder(side: BorderSide(
//                        color: Color.fromRGBO(0, 131, 186, 1),
//                        width: MediaQuery.of(context).size.width *0.02,
//                        style: BorderStyle.solid
//                    ),
//                      borderRadius: BorderRadius.circular(20),),
//                    child: Column( // Replace with a Row for horizontal icon + text
//                      children: <Widget>[
//                        new Icon(Icons.date_range,color: Color.fromRGBO(9, 99, 141, 1)),
//                        Text('AGENDAR CITA',
//                          textAlign: TextAlign.center,
//                          style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,color: Color.fromRGBO(9, 99, 141, 1)),
//                        )
//                      ],
//                    ),
//                  ),
//
//
//
//                Container(
//                        padding: EdgeInsets.all(10.0),
//                        alignment: Alignment.topCenter,
//                        child:Column( // Replace with a Row for horizontal icon + text
//                            children: <Widget>[
//                              Row( children: <Widget>[
//                                Icon(Icons.dashboard,color: Colors.white),
//                                Container(
//                                  width: MediaQuery.of(context).size.width *0.02,
//                                ),
//                                Text( '$currentTime'.substring(0, 10), maxLines: 2, textAlign: TextAlign.left, style: TextStyle(fontSize: 13, color: Colors.white)),
//
//
//
//                              ]),
//
//
//                              Row( children: <Widget>[
//                                Icon(Icons.account_circle,color: Colors.white),
//                                Text( 'Alejandro Pineda', maxLines: 3, style: TextStyle(fontSize: 13, color: Colors.white)),
//
//                              ]),
//
//
//
//                            ])
//
//                  ),
                ],
              ),
            ),

                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,

                        ),


                        Container(
                          child:  Text('¡BIENVENIDO A LA AGENDA DE CITAS!', maxLines: 1,  textAlign: TextAlign.center,
                            style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color: Color.fromRGBO(9, 99, 141, 1))
                          ),
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,

                        ),
                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,

                        ),

                        getInputDate(context),

                        Icon(Icons.arrow_downward,color: Colors.blue),
                        getImage(context)
                      ],
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),

    );
  }
  Widget getImage(BuildContext context){
    return  Container(
      padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
      child:   new Column(
        children: <Widget>[
          FlatButton(
              child:   Image(image: new AssetImage('assets/images/cita1.png')),
              onPressed: () {

                _selectDate(context);
              }
          ),
        ],
      ),
    );
  }
  Widget botonRegresar(BuildContext context) {
    return Container(

      height: MediaQuery
          .of(context)
          .size
          .height * 0.02,
    );
  }
  DateTime selectedDate = DateTime.now();

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(2015, 8),
      lastDate: DateTime(2101));

    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        print('fecha' + "${selectedDate.toLocal()}".split(' ')[0]);
        GlobalVariables.fechaCita = "${selectedDate.toLocal()}".split(' ')[0];

        GlobalVariables.caseProrroga = '1';
        Navigator.pushAndRemoveUntil(
          context,
          PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
            return AgendaCitasScreen();
          }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
            return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
            ).animate(animation), child: child,);
          },
              transitionDuration: Duration(seconds: 1)
          ), (Route route) => false,);

      });
  }

  Widget getInputDate(BuildContext context){
    return  Container(
      padding: EdgeInsets.fromLTRB(8, 1, 1, 1),
      child: SizedBox(
        height: MediaQuery.of(context).size.height * 0.08,
        width: double.infinity,
        child:  new Column(
          children: <Widget>[
            Icon(Icons.calendar_today,color: Colors.blue),
            Text("${selectedDate.toLocal()}".split(' ')[0],textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18,color: Color.fromRGBO(9, 99, 141, 1))),

//            FlatButton(
//              child: Image(image: new AssetImage('assets/images/calendar.png'),
//                  width: 35, height: 35),
//              onPressed: (){
//                _selectDate(context);
//                String fecha= "${selectedDate.toLocal()}".split(' ')[0];
//                print('FECHA CALENDARIO**** ' +fecha);
//GlobalVariables.fechaCita = "${selectedDate.toLocal()}".split(' ')[0];
//
//              },
//            ),

          ],
        ),

      ),
    );
  }
}







