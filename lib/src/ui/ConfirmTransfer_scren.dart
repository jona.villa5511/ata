

import 'dart:io';

import 'package:ATA/src/ui/Ingresar/MensajeDesacargaCita_scren.dart';
import 'package:ATA/src/ui/Ingresar/MensajeDesacargaProrroga_scren.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:ATA/src/complements/globalWidgets.dart';
import 'package:ATA/src/complements/globalVariables.dart';
import 'package:ATA/src/services/APiWebServices/Inventario_wsConsults.dart';
import 'package:ATA/src/complements/globalVariables.dart';
import 'package:ATA/src/ui/AddCardTransfer_scren.dart';
import 'package:ATA/src/ui/AgendaCitas_scren.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'AddCardTransfer_scren.dart';
import 'AddTransfer_scren.dart';
import 'AgendaCitas_scren.dart';

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:ATA/src/blocs/ATA/ATA_bloc.dart';
import 'package:ATA/src/blocs/login_bloc.dart';
import 'package:ATA/src/complements/globalWidgets.dart';
import 'package:ATA/src/complements/globalVariables.dart';
import 'package:image_picker/image_picker.dart';
import 'package:ATA/src/models/Files.dart';
import 'package:ATA/src/services/APiWebServices/Inventario_wsConsults.dart';
import 'package:ATA/src/ui/Frame/InventarioDetalle_scren.dart';
import 'package:ATA/src/ui/Frame/PdfPreviewScreen.dart';
import 'package:ATA/src/ui/Frame/ViewImageFileScreen.dart';
import 'package:open_file/open_file.dart';
import 'package:path/path.dart' as path;
import 'package:file_picker/file_picker.dart';
import 'package:path_provider/path_provider.dart';
class ConfirmTransferScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => ConfirmTransferScreenState();

}

class ConfirmTransferScreenState extends State<ConfirmTransferScreen> {
  String _value;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  File _pickedImage;
  String URLS;
  String _pathUploadFile = '';
  Map<String, String> _paths;
  String _extension;
  bool _loadingPath = false;
  bool _multiPick = false;
  FileType _pickingType = FileType.any;
  TextEditingController _controller = new TextEditingController();
  String _fileName;
  String _pathFile;
  List<StorageUploadTask> _tasks = <StorageUploadTask>[];
  StorageReference storageReference;
  final Dio _dio = Dio();
  String _progress = "-";
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
  new GlobalKey<RefreshIndicatorState>();

  final FirebaseStorage _storage =
  FirebaseStorage(storageBucket: 'gs://isae-de6da.appspot.com');

  StorageUploadTask _uploadTask;

  @override
  void dispose(){
    super.dispose();
  }

  Future<void> turnOffNotificationById(
      FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin,
      num id) async {
    await flutterLocalNotificationsPlugin.cancel(id);
  }

  Future<void> initNotification() async {

    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    final androids = AndroidInitializationSettings('@mipmap/app_icon');
    final iOSs = IOSInitializationSettings();
    final initSettingss = InitializationSettings(androids, iOSs);
    flutterLocalNotificationsPlugin.initialize(initSettingss, onSelectNotification: _onSelectNotification);
  }
  @override
  void initState(){
    super.initState();
    disablePathProviderPlatformOverride = true;

    initNotification();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => _refreshIndicatorKey.currentState.show());
  }
  Future<void> _onSelectNotification(String json) async {
    final obj = jsonDecode(json);

    if (obj['isSuccess'] && GlobalVariables.banderaDownload) {
      OpenFile.open(obj['filePath']);
      GlobalVariables.banderaDownload = false;
    } else {

    }
  }

  Future<void> _showNotification(Map<String, dynamic> downloadStatus) async {
    final android = AndroidNotificationDetails(
        'channel id',
        'channel name',
        'channel description',
        priority: Priority.High,
        importance: Importance.Max
    );
    final iOS = IOSNotificationDetails();
    final platform = NotificationDetails(android, iOS);
    final json = jsonEncode(downloadStatus);
    final isSuccess = downloadStatus['isSuccess'];

    await flutterLocalNotificationsPlugin.show(
        0, // notification id
        isSuccess ? 'Descargado' : 'Fallo',
        isSuccess ? 'Archivo se ha descargado correctamente!' : 'Ocurrio un error al Descargar.',
        platform,
        payload: json
    );
  }

  Future<Directory> _getDownloadDirectory() async {

    return await getApplicationDocumentsDirectory();
  }

  Future<bool> _requestPermissions() async {

    return true;
  }

  void _onReceiveProgress(int received, int total) {
    if (total != -1) {
      setState(() {
        _progress = (received / total * 100).toStringAsFixed(0) + "%";
      });
    }
  }

  Future<void> _startDownload(String savePath,String _fileURL) async {
    print(_fileURL);
    print(savePath);
    Map<String, dynamic> result = {
      'isSuccess': false,
      'filePath': null,
      'error': null,
    };
    print(result.toString());
    try {

      final response = await _dio.download(
          _fileURL,
          savePath,
          onReceiveProgress: _onReceiveProgress
      );

      print(response.toString());
      result['isSuccess'] = response.statusCode == 200;
      result['filePath'] = savePath;
      LoginBloc.Downloading(context,false);
    } catch (ex) {
      result['error'] = ex.toString();
    } finally {
      await _showNotification(result);
    }
  }

  Future<void> _download(String _fileURL, String _nombreArchivo) async {
    final dir = await _getDownloadDirectory();

    final isPermissionStatusGranted = await _requestPermissions();

    if (isPermissionStatusGranted) {
      final savePath = path.join(dir.path, _nombreArchivo);
      print(savePath.toString());
      await _startDownload(savePath,_fileURL);
    } else {
      // handle the scenario when user declines the permissions
    }
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:
      GlobalWidgets.topBar('CONFIRMAR TRANSFERENCIA', 'Marco', context,''),
      backgroundColor: Color.fromRGBO(207, 227, 233, 1),
      body: Material(
        child: Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    child: Column(
                      children: <Widget>[

                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,
                        ),
                        FlatButton(
                          child:
                          Column( // Replace with a Row for horizontal icon + text
                            children: <Widget>[
                              Image(image: new AssetImage('assets/images/previous.png'),
                                  width: 25, height: 25),
                              Text("Regresar",
                                textAlign: TextAlign.center,
                                style:TextStyle(fontWeight: FontWeight.bold,fontSize: 10,color: Color.fromRGBO(9, 99, 141, 1)),
                              )
                            ],
                          ),
                          onPressed: (){
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => AddTransferScreen()),
                            );
                          },
                        ),
                        Container(
                          child:  Text('Confirmar detalles', maxLines: 1,  textAlign: TextAlign.center,
                            style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,color: Color.fromRGBO(9, 99, 141, 1))
                          ),
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,
                        ),

                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,
                        ),


                        getDatos(),

                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,
                        ),

                       Container(
                         padding: EdgeInsets.fromLTRB(30, 1, 1, 1),
                              child: Row(
                          children: <Widget>[

                            Material(
                              elevation: 5.0,
                              borderRadius: BorderRadius.circular(30.0),
                              color: Color.fromRGBO(74, 191, 37, 1),
                              child: MaterialButton(

                                padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                onPressed: () async {

                                  print('cancelar');
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => AgendaCitasScreen()),
                                  );

                                },
                                textColor: Colors.white,
                                child: Row( children: <Widget>[

                                  new Icon(Icons.cancel,color: Colors.white),
                                  Text("CANCELAR",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12,color: Colors.white)),
                                  new Icon(Icons.arrow_forward_ios,color: Colors.white),
                                ]),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.05,
                            ),
                            Material(
                              elevation: 5.0,
                              borderRadius: BorderRadius.circular(30.0),
                              color: Color.fromRGBO(74, 191, 37, 1),
                              child: MaterialButton(

                                padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                onPressed: () async {

                                  print('OK');
                                  if(GlobalVariables.caseProrroga == '1'){
                                    Navigator.pushAndRemoveUntil(
                                      context,
                                      PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                                        return MensajeDescargarCitaScreen();
                                      }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                                        return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                                        ).animate(animation), child: child,);
                                      },
                                          transitionDuration: Duration(seconds: 1)
                                      ), (Route route) => false,);
                                  }else{
                                    Navigator.pushAndRemoveUntil(
                                      context,
                                      PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                                        return MensajeDescargarProrrogaScreen();
                                      }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                                        return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                                        ).animate(animation), child: child,);
                                      },
                                          transitionDuration: Duration(seconds: 1)
                                      ), (Route route) => false,);
                                  }
                                },
                                textColor: Colors.white,
                                child: Row( children: <Widget>[

                                  new Icon(Icons.check_circle,color: Colors.white),
                                  Text("CONFIRMAR",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12,color: Colors.white)),
                                  new Icon(Icons.arrow_forward_ios,color: Colors.white),
                                ]),
                              ),
                            ),
                          ],
                        )

                       ),
                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,
                        ),


                      ],
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );

  }

  Widget pagoExitoso() =>
      StreamBuilder<bool>(builder: (context, snap) {
        return AlertDialog(
          backgroundColor: Color.fromRGBO(207, 227, 233, 1),
          scrollable: true,
          title: Column(
            children: <Widget>[
              Text('Mensaje',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color.fromRGBO(9, 99, 141, 1),
                      fontSize: 23)),
              Container(
                height: MediaQuery.of(context).size.height * 0.02,

              ),
          Align (
            alignment: Alignment.bottomCenter,
            child:  Row(
                children: <Widget>[
                  Icon(Icons.check_box, color: Colors.green),

                 Text('Pago exitoso!', textAlign: TextAlign.center, style: TextStyle( fontSize: 20, fontWeight: FontWeight.w900)),
                   ],
              ),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.02,

              ),

            ],
          ),

          content: Container(
            padding: EdgeInsets.all(20),
            color: Color.fromRGBO(178, 222, 235, 1),
            child: Column(
              children: <Widget>[
                Text('Tu cita fue agendanda correctamente, Descargar acuse de confirmación de cita.', textAlign: TextAlign.center, style: TextStyle( fontSize: 15, fontWeight: FontWeight.w500)),
                Container(
                  height: MediaQuery.of(context).size.height * 0.02,

                ),
                Material(
                  elevation: 5.0,
                  borderRadius: BorderRadius.circular(30.0),
                  color: Color.fromRGBO(74, 191, 37, 1),
                  child: MaterialButton(

                    padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    onPressed: () async {

                      print('OK');
                      GlobalVariables.banderaDownload = true;
                      _download('https://firebasestorage.googleapis.com/v0/b/isae-de6da.appspot.com/o/Pruebas%2Fprueba.pdf?alt=media&token=cba811fb-043b-4c79-a73a-0c57162502d4',
                          'prueba.pdf');


                    },
                    textColor: Colors.white,
                    child: Row( children: <Widget>[

                      new Icon(Icons.file_download,color: Colors.white),
                      Text("Descargar",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12,color: Colors.white)),
                      new Icon(Icons.arrow_forward_ios,color: Colors.white),
                    ]),
                  ),
                ),

              ],
            ),
          ),
        );
      });

  Widget pagoExitosoProrroga() =>
      StreamBuilder<bool>(builder: (context, snap) {
        return AlertDialog(
          backgroundColor: Color.fromRGBO(207, 227, 233, 1),
          scrollable: true,
          title: Column(
            children: <Widget>[
              Text('Mensaje Prorroga',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color.fromRGBO(9, 99, 141, 1),
                      fontSize: 23)),
              Container(
                height: MediaQuery.of(context).size.height * 0.02,

              ),
              Align (
                alignment: Alignment.bottomCenter,
                child:  Row(
                  children: <Widget>[
                    Icon(Icons.check_box, color: Colors.green),

                    Text('Pago exitoso!', textAlign: TextAlign.center, style: TextStyle( fontSize: 20, fontWeight: FontWeight.w900)),
                  ],
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.02,

              ),

            ],
          ),

          content: Container(
            padding: EdgeInsets.all(20),
            color: Color.fromRGBO(178, 222, 235, 1),
            child: Column(
              children: <Widget>[
                Text('Tu cita con PRORROGA fue agendanda correctamente, Descargar acuse de confirmación de cita.', textAlign: TextAlign.center, style: TextStyle( fontSize: 15, fontWeight: FontWeight.w500)),
                Container(
                  height: MediaQuery.of(context).size.height * 0.02,

                ),
                Material(
                  elevation: 5.0,
                  borderRadius: BorderRadius.circular(30.0),
                  color: Color.fromRGBO(74, 191, 37, 1),
                  child: MaterialButton(

                    padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    onPressed: () async {

                      print('OK');
                      GlobalVariables.banderaDownload = true;
                      _download('https://firebasestorage.googleapis.com/v0/b/isae-v1.appspot.com/o/ATA%2Fprueba.pdf?alt=media&token=661b58ab-c0be-4532-9a78-eb5b312ba2b5',
                          'prueba.pdf');


                    },
                    textColor: Colors.white,
                    child: Row( children: <Widget>[

                      new Icon(Icons.file_download,color: Colors.white),
                      Text("Descargar",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12,color: Colors.white)),
                      new Icon(Icons.arrow_forward_ios,color: Colors.white),
                    ]),
                  ),
                ),

              ],
            ),
          ),
        );
      });


   Widget getDatos(){
    return  Container(
      padding: EdgeInsets.fromLTRB(8, 1, 1, 1),
        child: Form(
        key: _formKey,
      child: SizedBox(

        width: double.infinity,
        child: Column(
          children: <Widget>[

        Container(
        child: Column(children: <Widget>[

            Table(
                border: TableBorder.all(width: 1.8, color:  Color.fromRGBO(207, 227, 233, 1)),
                children: [

                  TableRow(children: [
                    Column(children: [
                      SizedBox(
                          width: double.infinity,
                          child: Card(
                            color: Color.fromRGBO(63, 168, 214, 1),
                            child: Padding(
                              padding: EdgeInsets.all(16.0),
                              child: Text('Cuenta destino', style: TextStyle(fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  fontSize: 14)),
                            ),
                          )
                      ),
                    ]),
                  ]),

                  TableRow(children: [
                    Column(children: [
                      Container(
                        color: Color.fromRGBO(63, 168, 214, 1),
                        padding: EdgeInsets.fromLTRB(300, 1, 1, 10),
                        alignment: Alignment.bottomRight,
                        height: MediaQuery.of(context).size.height * 0.0,
                        child: Row ( children: <Widget>[
                          Icon(Icons.compare_arrows, color: Colors.blue),
                        ]
                        ),
                      ),
                      SizedBox(
                          height: MediaQuery.of(context).size.height * 0.05,
                          width: double.infinity,
                          child:
                          Align (
                              alignment: Alignment.centerLeft,
                              child:  Text(GlobalVariables.controllerNumerodeCuenta.text, style: TextStyle(fontWeight: FontWeight.bold,
                              color: Colors.black38,
                              fontSize: 16)
                              ),
                          )
                      ),
                    ]
                    )
                  ]),

                  TableRow(children: [
                    Column(children: [
                      SizedBox(
                          width: double.infinity,
                          child: Card(
                            color: Color.fromRGBO(63, 168, 214, 1),
                            child: Padding(
                              padding: EdgeInsets.all(16.0),
                              child: Text('Nombre destinatario', style: TextStyle(fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  fontSize: 14)),
                            ),
                          )
                      ),
                    ]),
                  ]),

                  TableRow(children: [
                    Column(children: [
                      Container(
                        color: Color.fromRGBO(63, 168, 214, 1),
                        padding: EdgeInsets.fromLTRB(300, 1, 1, 10),
                        alignment: Alignment.topRight,
                        height: MediaQuery.of(context).size.height * 0.0,
                        child: Row ( children: <Widget>[
                          Icon(Icons.supervisor_account, color: Colors.blue),
                        ]
                        ),
                      ),
                      SizedBox(
                          height: MediaQuery.of(context).size.height * 0.05,
                          width: double.infinity,
                          child:
                          Align (
                            alignment: Alignment.centerLeft,
                            child:  Text(GlobalVariables.controllerNombreBeneficiario.text, style: TextStyle(fontWeight: FontWeight.bold,
                                color: Colors.black38,
                                fontSize: 16)
                            ),
                          )
                      ),
                    ]
                    )
                  ]),


                  TableRow(children: [
                    Column(children: [
                      SizedBox(
                          width: double.infinity,
                          child: Card(
                            color: Color.fromRGBO(63, 168, 214, 1),
                            child: Padding(
                              padding: EdgeInsets.all(16.0),
                              child: Text('CLABE Emisor', style: TextStyle(fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  fontSize: 14)),
                            ),
                          )
                      ),
                    ]),
                  ]),

                  TableRow(children: [
                    Column(children: [
                      Container(
                        color: Color.fromRGBO(63, 168, 214, 1),
                        padding: EdgeInsets.fromLTRB(300, 1, 1, 10),
                        alignment: Alignment.topRight,
                        height: MediaQuery.of(context).size.height * 0.0,
                        child: Row ( children: <Widget>[
                          Icon(Icons.account_balance, color: Colors.blue),
                        ]
                        ),
                      ),
                      SizedBox(
                          height: MediaQuery.of(context).size.height * 0.05,
                          width: double.infinity,
                          child:
                          Align (
                            alignment: Alignment.centerLeft,
                            child:  Text(GlobalVariables.controllerClabeInterbanc.text, style: TextStyle(fontWeight: FontWeight.bold,
                                color: Colors.black38,
                                fontSize: 16)
                            ),
                          )
                      ),
                    ]
                    )
                  ]),


                  TableRow(children: [
                    Column(children: [
                      SizedBox(
                          width: double.infinity,
                          child: Card(
                            color: Color.fromRGBO(63, 168, 214, 1),
                            child: Padding(
                              padding: EdgeInsets.all(16.0),
                              child: Text('Monto', style: TextStyle(fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  fontSize: 14)),
                            ),
                          )
                      ),
                    ]),
                  ]),

                  TableRow(children: [
                    Column(children: [
                      Container(
                        color: Color.fromRGBO(63, 168, 214, 1),
                        padding: EdgeInsets.fromLTRB(300, 1, 1, 10),
                        alignment: Alignment.topRight,
                        height: MediaQuery.of(context).size.height * 0.0,
                        child: Row ( children: <Widget>[
                          Icon(Icons.monetization_on, color: Colors.blue),
                        ]
                        ),
                      ),
                      SizedBox(
                          height: MediaQuery.of(context).size.height * 0.05,
                          width: double.infinity,
                          child:
                          Align (
                            alignment: Alignment.centerLeft,
                            child:  Text('  359.00', style: TextStyle(fontWeight: FontWeight.bold,
                                color: Colors.black38,
                                fontSize: 16)
                            ),
                          )
                      ),
                    ]
                    )
                  ]),



                  TableRow(children: [
                    Column(children: [
                      SizedBox(
                          width: double.infinity,
                          child: Card(
                            color: Color.fromRGBO(63, 168, 214, 1),
                            child: Padding(
                              padding: EdgeInsets.all(16.0),
                              child: Text('Referencia numérica', style: TextStyle(fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  fontSize: 14)),
                            ),
                          )
                      ),
                    ]),
                  ]),

                  TableRow(children: [
                    Column(children: [
                      Container(
                        color: Color.fromRGBO(63, 168, 214, 1),
                        padding: EdgeInsets.fromLTRB(300, 1, 1, 10),
                        alignment: Alignment.topRight,
                        height: MediaQuery.of(context).size.height * 0.0,
                        child: Row ( children: <Widget>[
                          Icon(Icons.confirmation_number, color: Colors.blue),
                        ]
                        ),
                      ),
                      SizedBox(
                          height: MediaQuery.of(context).size.height * 0.05,
                          width: double.infinity,
                          child:
                          Align (
                            alignment: Alignment.centerLeft,
                            child:  Text('121020', style: TextStyle(fontWeight: FontWeight.bold,
                                color: Colors.black38,
                                fontSize: 16)
                            ),
                          )
                      ),
                    ]
                    )
                  ]),

                  TableRow(children: [
                    Column(children: [
                      SizedBox(
                          width: double.infinity,
                          child: Card(
                            color: Color.fromRGBO(63, 168, 214, 1),
                            child: Padding(
                              padding: EdgeInsets.all(16.0),
                              child: Text('Tipo de beneficiario', style: TextStyle(fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  fontSize: 14)),
                            ),
                          )
                      ),
                    ]),
                  ]),

                  TableRow(children: [
                    Column(children: [
                      Container(
                        color: Color.fromRGBO(63, 168, 214, 1),
                        padding: EdgeInsets.fromLTRB(300, 1, 1, 10),
                        alignment: Alignment.topRight,
                        height: MediaQuery.of(context).size.height * 0.0,
                        child: Row ( children: <Widget>[
                          Icon(Icons.account_box, color: Colors.blue),
                        ]
                        ),
                      ),
                      SizedBox(
                          height: MediaQuery.of(context).size.height * 0.05,
                          width: double.infinity,
                          child:
                          Align (
                            alignment: Alignment.centerLeft,
                            child:  Text('Persona Física', style: TextStyle(fontWeight: FontWeight.bold,
                                color: Colors.black38,
                                fontSize: 16)
                            ),
                          )
                      ),
                    ]
                    )
                  ]),



                  TableRow(children: [
                    Column(children: [
                      SizedBox(
                          width: double.infinity,
                          child: Card(
                            color: Color.fromRGBO(63, 168, 214, 1),
                            child: Padding(
                              padding: EdgeInsets.all(16.0),
                              child: Text('Concepto de pago', style: TextStyle(fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  fontSize: 14)),
                            ),
                          )
                      ),
                    ]),
                  ]),

                  TableRow(children: [
                    Column(children: [
                      Container(
                        color: Color.fromRGBO(63, 168, 214, 1),
                        padding: EdgeInsets.fromLTRB(300, 1, 1, 10),
                        alignment: Alignment.topRight,
                        height: MediaQuery.of(context).size.height * 0.0,
                        child: Row ( children: <Widget>[
                          Icon(Icons.text_fields, color: Colors.blue),
                        ]
                        ),
                      ),
                      SizedBox(
                          height: MediaQuery.of(context).size.height * 0.05,
                          width: double.infinity,
                          child:
                          Align (
                            alignment: Alignment.centerLeft,
                            child:  Text('Pago Citas', style: TextStyle(fontWeight: FontWeight.bold,
                                color: Colors.black38,
                                fontSize: 16)
                            ),
                          )
                      ),
                    ]
                    )
                  ]),

                ]
            )
        ]),
        ),



        ]),
        //),
      ),



        ),

    );
  }

}