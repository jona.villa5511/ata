

import 'package:ATA/src/services/APiWebServices/Inventario_wsConsults.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:ATA/src/complements/globalWidgets.dart';
import 'package:ATA/src/complements/globalVariables.dart';
import 'package:ATA/src/ui/AddCardTransfer_scren.dart';
import 'package:ATA/src/ui/Bienvenida_scren.dart';
import 'package:ATA/src/ui/AddTransfer_scren.dart';


class AgendaCitasScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => AgendaCitasScreenState();

}

class AgendaCitasScreenState extends State<AgendaCitasScreen> {
  String _value;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:
      GlobalWidgets.topBar('CITAS DISPONIBLES', 'Marco', context,''),
      backgroundColor: Color.fromRGBO(207, 227, 233, 1),
      body: Material(
        child: Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    child: Column(
                      children: <Widget>[

                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,
                        ),

                        FlatButton(
                          child:
                          Column( // Replace with a Row for horizontal icon + text
                            children: <Widget>[
                              Image(image: new AssetImage('assets/images/previous.png'),
                                  width: 25, height: 25),
                              Text("Regresar",
                                textAlign: TextAlign.center,
                                style:TextStyle(fontWeight: FontWeight.bold,fontSize: 10,color: Color.fromRGBO(9, 99, 141, 1)),
                              )
                            ],
                          ),
                          onPressed: (){
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => BienvenidaScreen()),
                            );
                          },
                        ),

                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,
                        ),
                        Container(
                          child:  Text('FECHA DE LA CITA: ', maxLines: 1,  textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 14,color: Color.fromRGBO(9, 99, 141, 1))
                          ),
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,

                        ),
                        Container(
                          child:  Text(GlobalVariables.fechaCita, maxLines: 1,  textAlign: TextAlign.center,
                              style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18,color: Color.fromRGBO(9, 99, 141, 1))
                          ),
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,

                        ),
    Container(
              padding: EdgeInsets.fromLTRB(8, 1, 1, 1),
              child: SizedBox(
              height: MediaQuery.of(context).size.height * 0.08,
              width: double.infinity,
              child:  new Row(
              children: <Widget>[
Text('Cambiar Fecha: '),
              FlatButton(
              child: Image(image: new AssetImage('assets/images/calendar.png'),
              width: 35, height: 35),
              onPressed: (){
              _selectDate(context);
              String fecha= "${selectedDate.toLocal()}".split(' ')[0];
              print('FECHA CALENDARIO**** ' +fecha);
              GlobalVariables.fechaCita = "${selectedDate.toLocal()}".split(' ')[0];

              },
              ),

              ],
              ),

              ),
              ),

                         _selectHorario(),
                        getInputDate(context),
                        buttonFormaPago(),



                      ],
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );

  }
  DateTime selectedDate = DateTime.now();

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));

    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        print('fechaCambio cita' + "${selectedDate.toLocal()}".split(' ')[0]);
        GlobalVariables.fechaCita = "${selectedDate.toLocal()}".split(' ')[0];



      });
  }
  Widget buttonFormaPago(){
    return                       Container(
      color:  Colors.white,
      child: Column(

          children: <Widget>[
            Text("Eliga el tipo de pago :",
              style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,color: Colors.black),
            ),
            Container(
              height: MediaQuery
                  .of(context)
                  .size
                  .height * 0.02,
            ),

            Material(
              elevation: 5.0,
              borderRadius: BorderRadius.circular(30.0),
              color: Color.fromRGBO(36, 90, 149, 1),
              child: MaterialButton(
                minWidth: MediaQuery.of(context).size.width / 2,
                //padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                onPressed: () async {

                  print('sssss');
                if(GlobalVariables.value == ''){
                  print('Horario vacio' + GlobalVariables.value);
                  showDialog(context: context, builder: (context) => ShowMessageValidateAdd('Seleccione un horario!','No selecciono un horario.'));

                }else{
                  print('Horario '+ GlobalVariables.value);
                    Navigator.pushAndRemoveUntil(
                    context,
                    PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                      return AddCardTransferScreen();
                    }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                      return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                      ).animate(animation), child: child,);
                    },
                        transitionDuration: Duration(seconds: 1)
                    ), (Route route) => false,);
                }

                },
                textColor: Colors.white,
                child: Row( children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width * 0.05,
                  ),
                  new Icon(Icons.credit_card,color: Colors.white),
                  Text("Pago con Tarjeta de Credito /Debito",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12,color: Colors.white)),
                  new Icon(Icons.arrow_forward_ios,color: Colors.white),
                ]),
              ),
            ),

            Container(
              height: MediaQuery
                  .of(context)
                  .size
                  .height * 0.02,
            ),

            Material(
              elevation: 5.0,
              borderRadius: BorderRadius.circular(30.0),
              color: Color.fromRGBO(36, 90, 149, 1),
              child: MaterialButton(
                minWidth: MediaQuery.of(context).size.width / 2,
                //padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                onPressed: () async {

                  print('sssss');
                  if(GlobalVariables.value == ''){
                    print('Horario vacio' + GlobalVariables.value);
                    showDialog(context: context, builder: (context) => ShowMessageValidateAdd('Seleccione un horario!','No selecciono un horario.'));

                  }else {
                    Navigator.pushAndRemoveUntil(
                      context,
                      PageRouteBuilder(pageBuilder: (BuildContext context,
                          Animation animation, Animation secondaryAnimation) {
                        return AddTransferScreen();
                      },
                          transitionsBuilder: (BuildContext context,
                              Animation<double> animation,
                              Animation<double> secondaryAnimation,
                              Widget child) {
                            return SlideTransition(position: Tween<Offset>(
                              begin: Offset(1.0, 0.0), end: Offset.zero,
                            ).animate(animation), child: child,);
                          },
                          transitionDuration: Duration(seconds: 1)
                      ), (Route route) => false,);
                  }
                },
                textColor: Colors.white,
                child: Row( children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width * 0.05,
                  ),
                  new Icon(Icons.transform,color: Colors.white),
                  Text("Transferencia Electronica",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12,color: Colors.white)),
                  new Icon(Icons.arrow_forward_ios,color: Colors.white),
                ]),
              ),
            ),
          ]),
    );
  }

  Widget _selectHorario() {
    return Container(
     width: MediaQuery.of(context).size.width * 0.99,
      child: Row(
        children: <Widget>[
          new Icon(Icons.access_time,color: Colors.black),
          Container(
            width: MediaQuery.of(context).size.width * 0.01,
          ),
          Text('Horarios: '),
          Container(
              color: Colors.white24,
              child: Column(
                  children: <Widget>[
                    DropdownButton<String>(
                      items: [
                        DropdownMenuItem<String>(
                          child: Text('09:00'),
                          value: '09:00',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('10:00'),
                          value: '10:00',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('11:00'),
                          value: '11:00',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('12:00'),
                          value: '12:00',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('13:00'),
                          value: '13:00',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('14:00'),
                          value: '14:00',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('15:00'),
                          value: '15:00',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('16:00'),
                          value: '16:00',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('17:00'),
                          value: '17:00',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('18:00'),
                          value: '18:00',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('19:00'),
                          value: '19:00',
                        ),
                      ],
                      onChanged: (String value) {
                        setState(() {
                          print('Value '+ value);


                          GlobalVariables.value  = value;
                          _value = value;
                        });
                      },
                      hint: Text('Selecciona un horario',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12,color: Colors.black)),
                      value: _value,
                    ),

                  ])
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.05,
          ),
          Text('09:00 a 19:00 Hrs. '),

        ],
      ),
    );

  }


  Widget botonRegresar(BuildContext context) {
    return Container(
      height: MediaQuery
          .of(context)
          .size
          .height * 0.02,
    );
  }
  Widget getInputDate(BuildContext context){
    return  Container(
      padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
      child:   new Column(
          children: <Widget>[
            FlatButton(
              child:   Image(image: new AssetImage('assets/images/calendario.png')),
              onPressed: () {}
            ),
            Image(image: new AssetImage('assets/images/acota.png'), width: 200, height: 200),

          ],
        ),
    );
  }
}