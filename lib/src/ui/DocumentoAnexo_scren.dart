import 'dart:convert';
import 'dart:io';


import 'package:ATA/src/services/APiWebServices/Inventario_wsConsults.dart';
import 'package:ATA/src/ui/Bienvenida_scren.dart';
import 'package:ATA/src/ui/Frame/PdfPreviewScreen.dart';
import 'package:ATA/src/ui/Frame/ViewImageFileScreen.dart';
import 'package:ATA/src/ui/Ingresar/BienvenidaIngresar_scren.dart';
import 'package:ATA/src/ui/Ingresar/TramiteProrrogaNuevaCita_scren.dart';
import 'package:ATA/src/ui/login_screen.dart';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:ATA/src/blocs/login_bloc.dart';
import 'package:ATA/src/complements/globalWidgets.dart';
import 'package:ATA/src/complements/globalVariables.dart';
import 'package:geolocator/geolocator.dart';
import 'package:ATA/src/ui/AgendaCitas_scren.dart';
import 'package:image_picker/image_picker.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as path;
class DocumentoAnexoScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => DocumentoAnexoScreenState();

}

class DocumentoAnexoScreenState extends State<DocumentoAnexoScreen> {
  File _pickedImage;
  String URLS;
  String _pathUploadFile = '';
  String fileName ='';
  Map<String, String> _paths;
  String _extension;
  bool _loadingPath = false;
  bool _multiPick = false;
  FileType _pickingType = FileType.any;
  TextEditingController _controller = new TextEditingController();
  String _fileName;
  String _pathFile;
  List<StorageUploadTask> _tasks = <StorageUploadTask>[];
  StorageReference storageReference;
  final Dio _dio = Dio();
  String _progress = "-";
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  String _uploadedFileURL;

  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
  new GlobalKey<RefreshIndicatorState>();

  Position currentLocation;
  final Map<String, Marker> _markers = {};

  @override
  void dispose(){
    super.dispose();
  }
  Future<void> turnOffNotification(
      FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin) async {
    await flutterLocalNotificationsPlugin.cancelAll();
  }

  Future<void> turnOffNotificationById(
      FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin,
      num id) async {
    await flutterLocalNotificationsPlugin.cancel(id);
  }

  Future<void> initNotification() async {

    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    final android = AndroidInitializationSettings('@mipmap/app_icon');
    final iOS = IOSInitializationSettings();
    final initSettings = InitializationSettings(android, iOS);
    flutterLocalNotificationsPlugin.initialize(initSettings, onSelectNotification: _onSelectNotification);
  }

  @override
  void initState() {
    super.initState();
    disablePathProviderPlatformOverride = true;

    initNotification();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => _refreshIndicatorKey.currentState.show());

  }
  Future<void> _onSelectNotification(String json) async {
    final obj = jsonDecode(json);

    if (obj['isSuccess'] && GlobalVariables.banderaDownload) {
      OpenFile.open(obj['filePath']);
      GlobalVariables.banderaDownload = false;
    } else {

    }
  }

  Future<void> _showNotification(Map<String, dynamic> downloadStatus) async {
    final android = AndroidNotificationDetails(
        'channel id',
        'channel name',
        'channel description',
        priority: Priority.High,
        importance: Importance.Max
    );
    final iOS = IOSNotificationDetails();
    final platform = NotificationDetails(android, iOS);
    final json = jsonEncode(downloadStatus);
    final isSuccess = downloadStatus['isSuccess'];

    await flutterLocalNotificationsPlugin.show(
        0, // notification id
        isSuccess ? 'Descargado' : 'Fallo',
        isSuccess ? 'Archivo se ha descargado correctamente!' : 'Ocurrio un error al Descargar.',
        platform,
        payload: json
    );
  }

  Future<Directory> _getDownloadDirectory() async {

    return await getApplicationDocumentsDirectory();
  }

  Future<bool> _requestPermissions() async {

    return true;
  }

  void _onReceiveProgress(int received, int total) {
    if (total != -1) {
      setState(() {
        _progress = (received / total * 100).toStringAsFixed(0) + "%";
      });
    }
  }

  Future<void> _startDownload(String savePath,String _fileURL) async {
    Map<String, dynamic> result = {
      'isSuccess': false,
      'filePath': null,
      'error': null,
    };

    try {
      final response = await _dio.download(
          _fileURL,
          savePath,
          onReceiveProgress: _onReceiveProgress
      );
      result['isSuccess'] = response.statusCode == 200;
      result['filePath'] = savePath;
      LoginBloc.Downloading(context,false);
    } catch (ex) {
      result['error'] = ex.toString();
    } finally {
      await _showNotification(result);
    }
  }

  Future<void> _download(String _fileURL, String _nombreArchivo) async {
    final dir = await _getDownloadDirectory();

    final isPermissionStatusGranted = await _requestPermissions();

    if (isPermissionStatusGranted) {
      final savePath = path.join(dir.path, _nombreArchivo);
      await _startDownload(savePath,_fileURL);
    } else {
      // handle the scenario when user declines the permissions
    }
  }
  void pickImage(BuildContext context) async {
    var picture = await ImagePicker.pickImage(source: ImageSource.gallery);
    this.setState(() {
      _pickedImage = picture;
    });


    String fileName = _pickedImage.path
        .split('/')
        .last;

    print(fileName);
  }
  void _openFileExplorer() async {
    setState(() => _loadingPath = true);
    try {
      if (_multiPick) {
        _pathFile = null;
        _paths = await FilePicker.getMultiFilePath(
            type: _pickingType,
            allowedExtensions: (_extension?.isNotEmpty ?? false)
                ? _extension?.replaceAll(' ', '')?.split(',')
                : null);
      } else {
        _paths = null;
        _pathFile = await FilePicker.getFilePath(
            type: _pickingType,
            allowedExtensions: (_extension?.isNotEmpty ?? false)
                ? _extension?.replaceAll(' ', '')?.split(',')
                : null);
        print('path ' + _pathFile);
        File file = new File(_pathFile);
        fileName = file.path.split('/').last;

        print(fileName);
        if(_pathFile.contains('pdf') || _pathFile.contains('xls')){
          print('es correecto el formato ');

          LoginBloc.loadingI(context,true);

          Directory documentDirectory =
          await getApplicationDocumentsDirectory();
          String documentPath = documentDirectory.path;
          _pathUploadFile = "$documentPath/$fileName";
          File temp = File(_pathFile);
          File newFile = await temp.copy(_pathUploadFile);
          print('path:: ' + _pathUploadFile);

          print('fileFinal:: ' + newFile.path);


          StorageReference storageReference = FirebaseStorage.instance
              .ref()
              .child('ATA/$fileName');
          StorageUploadTask uploadTask = storageReference.putFile(temp);
          await uploadTask.onComplete;
          print('File Uploaded');

          var dowurl = await (await uploadTask.onComplete).ref.getDownloadURL();
          _uploadedFileURL = dowurl.toString();

          print('urllllllllll  ' +_uploadedFileURL);



            LoginBloc.loadingI(context, false);





        }else{
          print('formato incorrecto');
          showDialog(context: context, builder: (context) => ShowMessage('Formato incorrecto solo admite PDF ò XLSX.'));
        }



      }
    } catch (e) {
      print("Error al cargar::: " + e.toString());
    }
    if (!mounted) return;
    setState(() {
      _loadingPath = false;
      _fileName = _pathFile != null
          ? _pathFile.split('/').last
          : _paths != null ? _paths.keys.toString() : '...';
    });
  }


  @override
  Widget build(BuildContext context) {
    DateTime now1 = DateTime.now();
    var currentTime = new DateTime(now1.year, now1.month, now1.day);
    GlobalVariables.dateNow = currentTime.toString();
    print('Cargando App...');
    return Scaffold(
      appBar:
      GlobalWidgets.topBar(
          'TIPO DE PERSONA', ' Silva', context,
          '$currentTime'.substring(0, 10)),

      backgroundColor: Colors.black26,

      body: Material(
        child: Container(
          color: Colors.black12,
          child: Column(
            children: <Widget>[
              //------------Body------------
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        FlatButton(
                          child:
                          Column( // Replace with a Row for horizontal icon + text
                            children: <Widget>[
                              Image(image: new AssetImage(
                                  'assets/images/previous.png'),
                                  width: 25, height: 25),
                              Text("Regresar",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontWeight: FontWeight.bold,
                                    fontSize: 10,
                                    color: Color.fromRGBO(9, 99, 141, 1)),
                              )
                            ],
                          ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) =>
                                  LoginScreen()),
                            );
                          },
                        ),

                        Container(
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(0, 132, 186, 1),

                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(0.0),
                                  bottomLeft: Radius.circular(40.0),
                                  topRight: Radius.circular(0.0),
                                  bottomRight: Radius.circular(40.0))),

                          child: Row(

                            children: <Widget>[
                            ],
                          ),
                        ),

                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,

                        ),


                        Container(
                            child: Text('¡PERSONA FISICA Y MORAL!', maxLines: 1,
                                textAlign: TextAlign.center,
                                style: TextStyle(fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                    color: Color.fromRGBO(9, 99, 141, 1))
                            ), height: MediaQuery
                            .of(context)
                            .size
                            .height * 0.04),
                        getImage(context),

                        FlatButton(
                          child:
                          Column( // Replace with a Row for horizontal icon + text
                            children: <Widget>[
                              Container(
                                  child:  Icon(Icons.file_upload, color: Colors.blue, size: 50.4,)
                                  , height: MediaQuery
                                  .of(context)
                                  .size
                                  .height * 0.08),

                              Text("Anexar el documento 1 antes enviado en custión a registro de datos.",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontWeight: FontWeight.bold,
                                    fontSize: 11,
                                    color: Color.fromRGBO(9, 99, 141, 1)),
                              )
                            ],
                          ),
                          onPressed: () {
                           // pickImage(context);
                            _openFileExplorer();
                          },
                        ),
                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,

                        ),

                        Container(

                          color: Color.fromRGBO(207, 227, 233, 1),// cOLORE DE FONDO CAMBIAR
                          child: Column(
                            children: <Widget>[

                              //for (var index = 0; index < GlobalVariables.listaFilesInven.length; index++)(

                              getLoadFilesDataBase(context),
                              Container(
                                height: MediaQuery.of(context).size.height *0.10,
                              ),
    if(fileName.isNotEmpty)(
                              Material(
                                elevation: 5.0,
                                borderRadius: BorderRadius.circular(30.0),
                                color: Color.fromRGBO(74, 191, 37, 1),
                                child: MaterialButton(

                                  padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                  onPressed: () async {

                                    print('OK');


                                    Navigator.pushAndRemoveUntil(
                                      context,
                                      PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                                        return BienvenidaScreen();
                                      }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                                        return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                                        ).animate(animation), child: child,);
                                      },
                                          transitionDuration: Duration(seconds: 1)
                                      ), (Route route) => false,);

                                  },
                                  textColor: Colors.white,
                                  child: Row( children: <Widget>[
                                    Container(
                                      width: MediaQuery.of(context).size.width *0.02,
                                    ),
                                    new Icon(Icons.check_circle_outline,color: Colors.white),
                                    Container(
                                      width: MediaQuery.of(context).size.width *0.15,
                                    ),
                                    Text("Continuar",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,color: Colors.white)),
                                    Container(
                                      width: MediaQuery.of(context).size.width *0.15,
                                    ),
                                    new Icon(Icons.arrow_forward_ios,color: Colors.white),
                                  ]),
                                ),
                              )
    ),

                            ],

                          ),
                        ),

                      ],
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),

    );
  }
  Widget getLoadFilesDataBase(BuildContext context ) {
    double iconSize = 15;
    // String fileName = GlobalVariables.listaFilesInven[index].fileName.split('/').last;
    return   Container(
      decoration: BoxDecoration(
          color: Color.fromRGBO(65, 190, 223, 1).withOpacity(0.2),
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10.0),
              bottomRight: Radius.circular(10.0))),
      child:

      Table(
        border: TableBorder.all(
          color: Color.fromRGBO(207, 227, 233, 1),
        ),
        columnWidths: {

          0: FlexColumnWidth(1.5),
          // 0: FlexColumnWidth(4.501), // - is ok
          // 0: FlexColumnWidth(4.499), //- ok as well
          1: FlexColumnWidth(1),
        },
        children: [
          TableRow( children: [
            Column(children:[
              Center(child:Container(
                  padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child:  Text('NOMBRE',textAlign: TextAlign.center, style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.black,)) )),
            ]),
            Column(children:[
              Center(child:Container(
                  padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: Text('TIPO',textAlign: TextAlign.center, style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold, color: Colors.black,)) )),
            ]),
            Column(children:[
              Center(child:Container(
                  padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: Text('FECHA',textAlign: TextAlign.center, style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold, color: Colors.black,)) )),
            ]),
            Column(children:[
              Center(child:Container(
                  padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child:Text('ACCIÒN',textAlign: TextAlign.center, style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold, color: Colors.black,)) )),
            ]),
          ]),

if(fileName.isNotEmpty)(
              TableRow( children: [
                //Text(GlobalVariables.listaFilesInven[index].fileName, maxLines: 2, textAlign: TextAlign.center, style: TextStyle(fontSize: 11, color: Colors.black)),
                Center(child:Container(
                  padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                  child: Text(fileName, maxLines: 4, textAlign: TextAlign.center, style: TextStyle(fontSize: 11, color: Colors.black,)),)),

                Center(child:Container(
                    padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                    child: _pathUploadFile.split('/').last.contains('pdf') ? Icon(Icons.picture_as_pdf,color: Color.fromRGBO(9, 99, 141, 1)) : _pathUploadFile.split('/').last.contains('png') ?  Icon(Icons.image,color: Color.fromRGBO(9, 99, 141, 1))  :  Icon(Icons.explicit,color: Color.fromRGBO(9, 99, 141, 1)) )),

                Center(child:Container(
                  padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                  child: Text(GlobalVariables.dateNow.substring(0,10), maxLines: 2, textAlign: TextAlign.center, style: TextStyle(fontSize: 11, color: Colors.black)),)),


                PopupMenuButton<String>(
                  onSelected: (result){
                    handleClick(result);

                  },
                  itemBuilder: (BuildContext context) {
                    return {'Descargar', 'Eliminar','Vista Previa'}.map((String choice) {

                      return PopupMenuItem<String>(

                        value: choice,
                        child:  ListTile(
                          leading: choice == 'Descargar' ? Icon(Icons.file_download,color: Color.fromRGBO(12, 160, 219, 1)) : choice == 'Eliminar' ? Icon(Icons.delete_sweep,color: Color.fromRGBO(12, 160, 219, 1)) : Icon(Icons.visibility,color: Color.fromRGBO(12, 160, 219, 1)),
                          title:
                          Center(child:Container(
                            child: Text(choice,  style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Color.fromRGBO(12, 160, 219, 1))),)),
                        ),
                      );
                    }).toList();
                  },
                ),
              ])


),
        ],
      ),

    );
  }
  Future<void> handleClick(String value) async {
    switch (value) {
      case 'Descargar':

        LoginBloc.Downloading(context,true);

        GlobalVariables.banderaDownload = true;
        _download(_uploadedFileURL, fileName);

        break;
      case 'Eliminar':
        showAlertDialogDeleteFiles(context);
        break;
      case 'Vista Previa':
        print('Vista previa....' + _uploadedFileURL == null ? '' : _uploadedFileURL);
        GlobalVariables.urlPathPDF = _uploadedFileURL == null ? '' : _uploadedFileURL;
        GlobalVariables.nombrePDF = fileName == null ? '' : fileName;


        if(fileName.contains('pdf')) {
          getPDF();
          LoginBloc.loadingI(context,true);
          Future.delayed(Duration(seconds: 3), () => {
            LoginBloc.loadingI(context, false),


            Navigator.pushAndRemoveUntil(
              context,
              PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                return PdfPreviewScreen();
              }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                ).animate(animation), child: child,);
              },
                  transitionDuration: Duration(seconds: 1)
              ), (Route route) => false,)
          });

        } else if(fileName.contains('xls')) {
          LoginBloc.loadingI(context,false);
          showDialog(context: context, builder: (context) => ShowMessageVistaExcel('No es posible mostrar vista previa del archivo.','Descargar achivo para visualizarlo'));
        } else{
          LoginBloc.loadingI(context,true);


          Future.delayed(Duration(seconds: 1), () => {
            LoginBloc.loadingI(context,false),
            //showDialog(context: context, builder: (context) => ShowPreviewImageDataBase(index,File(GlobalVariables.listaFilesInven[index].fileUrl),latlng,markers))

            Navigator.pushReplacement(context,
                PageRouteBuilder( pageBuilder: (context, anim1, anim2) => ViewImageFileScreenScreen(),
                  transitionsBuilder: (context, anim1,anim2, child) => Container(child: child),
                  transitionDuration: Duration(seconds: 2),
                )
            )
          });
        }
        break;
    }
  }
  getPDF() async{
    print('entro getPFD ' +GlobalVariables.urlPathPDF);
    GlobalVariables.doc = await PDFDocument.fromURL(GlobalVariables.urlPathPDF);

  }
  showAlertDialogDeleteFiles(BuildContext context) {

    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancelar"),
      onPressed:  () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Aceptar"),
      onPressed:  () {



        LoginBloc.loadingI(context,true);
        String filePath = 'ATA/$fileName';
        print('File deltete :.: ' + filePath);
        StorageReference storageReferance = FirebaseStorage.instance.ref();
        storageReferance.child(filePath).delete().then((_) => print('Successfully deleted $filePath storage item' ));

        Future.delayed(Duration(seconds: 4), () => {
          LoginBloc.loadingI(context, false),


        });
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Alerta"),
      content: Text("¿Esta seguro de eliminar evidencia?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  Widget ShowMessageVistaExcel(String message, String text2) =>
      StreamBuilder<bool>(builder: (context, snap) {


        return AlertDialog(
          backgroundColor: Color.fromRGBO(207, 227, 233, 1),
          scrollable: true,
          title: new Text('Alerta'),

          content: Container(


            color: Color.fromRGBO(178, 222, 235, 1),
            child: Column(
              children: <Widget>[
                Text(message, style: TextStyle( fontSize: 12, fontWeight: FontWeight.bold)),
                Text(text2, style: TextStyle( fontSize: 12, fontWeight: FontWeight.bold)),
                Icon(Icons.report_problem, color: Colors.blue)
              ],
            ),
          ),
        );
      });

  Widget getImage(BuildContext context){
    return  Container(
      color: Color.fromRGBO(178, 222, 235, 1),
      padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
      child:   new Column(
        children: <Widget>[
          FlatButton(
              child:   Image(image: new AssetImage('assets/images/persona1.png'),
                  width: 100, height: 100),
              onPressed: () {


              }
          ),
        ],
      ),
    );
  }
}







