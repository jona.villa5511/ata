import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'dart:ui';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_signature_pad/flutter_signature_pad.dart';
import 'package:ATA/src/complements/globalVariables.dart';
import 'package:ATA/src/complements/globalWidgets.dart';
import 'package:ATA/src/ui/Frame/Firma2Screen.dart';
import 'package:ATA/src/ui/Frame/Firma4Screen.dart';
import 'package:path_provider/path_provider.dart';

class Firma3Screen extends StatefulWidget {

  @override
  _Firma3ScreenState createState() => _Firma3ScreenState();
}

class _WatermarkPaint extends CustomPainter {
  final String price;
  final String watermark;

  _WatermarkPaint(this.price, this.watermark);

  @override
  void paint(ui.Canvas canvas, ui.Size size) {
    canvas.drawCircle(Offset(size.width / 2, size.height / 2), 10.8, Paint()..color = Colors.blue);
  }

  @override
  bool shouldRepaint(_WatermarkPaint oldDelegate) {
    return oldDelegate != this;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is _WatermarkPaint && runtimeType == other.runtimeType && price == other.price && watermark == other.watermark;

  @override
  int get hashCode => price.hashCode ^ watermark.hashCode;
}

class _Firma3ScreenState extends State<Firma3Screen> {

//  ByteData _img = ByteData(0);
  var color = Colors.blue;
  var strokeWidth = 5.0;
  final _sign = GlobalKey<SignatureState>();
  StorageReference storageReference;

  Future<File> createFileOfPdfUrl() async {
    Completer<File> completer = Completer();
    print("Start download file from internet!");
    try {
      var dir = await getApplicationDocumentsDirectory();
      print("Download files");
      final file = File('${dir.path}/firma3.png');
      await file.writeAsBytes(GlobalVariables.firma3.buffer.asUint8List(GlobalVariables.firma3.offsetInBytes, GlobalVariables.firma3.lengthInBytes));
      completer.complete(file);
    } catch (e) {
      throw Exception('Error parsing asset file!');
    }

    return completer.future;
  }
  @override
  void initState(){
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
  }

  @override
  dispose(){
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    //SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight]);

    return     new Scaffold(
      appBar: new AppBar(
        title: Text('Firma del Enlace Informàtico'),
        elevation: 0.0,
        leading: GlobalWidgets.addLeadingIcon(),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(12, 160, 219, 1),
      ),
      body: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(10),
            alignment: Alignment.topCenter,
            decoration: BoxDecoration(
                color:  Color.fromRGBO(9, 99, 141, 1),
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(0.0),
                    bottomRight: Radius.circular(0.0))),

            child:  Row ( children: <Widget>[
              new Icon(Icons.work,color: Colors.white),
              Text('Proyecto: ' + GlobalVariables.nameProyectFile, maxLines: 1, textAlign: TextAlign.center, style:
              TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.white)),
              Container(
                width: MediaQuery.of(context).size.width * 0.02,
              ),
//              new Icon(Icons.library_books,color: Colors.white),
//              Text('Inventario: ' + GlobalVariables.nameInvenFolioFile, maxLines: 1, textAlign: TextAlign.center, style:
//              TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.white)),

            ]
            ),
          ),
          Expanded(
            child: Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Signature(
                  color: Colors.black,
                  key: _sign,
                  onSign: () {
                    final sign = _sign.currentState;
                    debugPrint('${sign.points.length} points in the signature');
                  },

                  strokeWidth: strokeWidth,
                ),
              ),
              color: Colors.black12,
            ),
          ),
          GlobalVariables.firma3.buffer.lengthInBytes == 0 ? Container() : LimitedBox(maxHeight: 200.0, child: Image.memory(GlobalVariables.firma3.buffer.asUint8List())),
          Column(
            children: <Widget>[
              Row(

                mainAxisAlignment: MainAxisAlignment.center,
                children: [

                  Material(

                      borderRadius: BorderRadius.circular(30.0),

                      child: MaterialButton(
                          minWidth: MediaQuery.of(context).size.width / 2,
                          color: Colors.green,
                          onPressed: () async {
                            final sign = _sign.currentState;
                            //retrieve image data, do whatever you want with it (send to server, save locally...)
                            final image = await sign.getData();
                            var data = await image.toByteData(format: ui.ImageByteFormat.png);
                            sign.clear();
                            final encoded = base64.encode(data.buffer.asUint8List());
                            setState(() {
                              GlobalVariables.firma3 = data;
                            });


                           showAlertSaveFirma3(context);
                          },
                          child: Icon(Icons.save))),
                  Material(

                      borderRadius: BorderRadius.circular(30.0),

                      child: MaterialButton(

                          minWidth: MediaQuery.of(context).size.width / 2,
                          color: Colors.grey,
                          onPressed: () {
                            final sign = _sign.currentState;
                            sign.clear();
                            setState(() {
                              GlobalVariables.firma3 = ByteData(0);
                            });
                            debugPrint("cleared");
                          },
                          child: Icon(Icons.delete_outline)

                      )),

                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FlatButton(
                    child:
                    Column( // Replace with a Row for horizontal icon + text
                      children: <Widget>[
                        Image(image: new AssetImage('assets/images/previous.png'),
                            width: 25, height: 25),
                        Text("Regresar",
                          textAlign: TextAlign.center,
                          style:TextStyle(fontWeight: FontWeight.bold,fontSize: 10,color: Color.fromRGBO(9, 99, 141, 1)),
                        )
                      ],
                    ),
                    onPressed: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Firma2Screen()),
                      );
                    },
                  ),


                ],
              ),

            ],
          )
        ],
      ),
    );
  }


  showAlertSaveFirma3(BuildContext context) {

    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancelar"),
      onPressed:  () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Aceptar"),
      onPressed:  () {
        createFileOfPdfUrl().then((f) async {
          setState(() {
            GlobalVariables.remotePDFpathFirma3 = f.path;
          });
        });
        Navigator.pushReplacement(
            context,
            PageRouteBuilder(pageBuilder: (context, anim1, anim2) => Firma4Screen(),
              transitionsBuilder: (context, anim1, anim2, child) => Container(child: child),//(context, anim1, anim2, child) => FadeTransition(opacity: anim1, child: child),
              transitionDuration: Duration(seconds: 1),
            )
        );
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Confirmar"),
      content: Text("¿Esta seguro de guardar la Firma del Enlace Informàtico?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

}
