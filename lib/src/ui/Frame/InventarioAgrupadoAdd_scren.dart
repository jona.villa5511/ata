import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:ATA/src/blocs/ATA/ATA_bloc.dart';
import 'package:ATA/src/blocs/login_bloc.dart';
import 'package:ATA/src/complements/globalWidgets.dart';
import 'package:ATA/src/complements/globalVariables.dart';
import 'package:ATA/src/models/InventarioAgrupado.dart';
import 'package:ATA/src/services/APiWebServices/Inventario_wsConsults.dart';
import 'package:ATA/src/ui/Frame/InventarioAdd_scren.dart';
import 'package:ATA/src/ui/Frame/InventarioDetalleAddAgrupacion_scren.dart';

class InventarioAgrupadoAddScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => InventarioAgrupadoAddScreenState();

}

class InventarioAgrupadoAddScreenState extends State<InventarioAgrupadoAddScreen> {


  @override
  void initState(){
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    DateTime now1 = DateTime.now();
    var currentTime = new DateTime(now1.year, now1.month, now1.day);
    GlobalVariables.dateNow = currentTime.toString();
    //print('Fecha del dia  $currentTime');
    return Scaffold(
      //key: GlobalVariables.scaffoldKeyIMCR,
      appBar:
      //PreferredSize(
        //  preferredSize: Size.fromHeight(85.0),
          //   child:
      GlobalWidgets.topBar('INVENTARIO AGRUPADO','Marco Antonio', context, '$currentTime'.substring(0,10)),

          //),
      backgroundColor: Color.fromRGBO(207, 227, 233, 1),


      body: Material(
        child: Container(
          color: Color.fromRGBO(207, 227, 233, 1),
          child: Column(
            children: <Widget>[
              //------------Body------------
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        GlobalWidgets.menuHome(context,'$currentTime'.substring(0,10),botonRegresar(context)),
                        FlatButton(
                          child:
                          Column( // Replace with a Row for horizontal icon + text
                            children: <Widget>[
                              Image(image: new AssetImage('assets/images/previous.png'),
                                  width: 25, height: 25),
                              Text("Regresar",
                                textAlign: TextAlign.center,
                                style:TextStyle(fontWeight: FontWeight.bold,fontSize: 10,color: Color.fromRGBO(9, 99, 141, 1)),
                              )
                            ],
                          ),
                          onPressed: (){
                            limpiar();

                            Navigator.pushAndRemoveUntil(
                              context,
                              PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                                return InventarioAddScreen();
                              }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                                return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                                ).animate(animation), child: child,);
                              },
                                  transitionDuration: Duration(seconds: 1)
                              ), (Route route) => false,);

                          },
                        ),

                    Container(

                          color: Color.fromRGBO(12, 160, 219, 1), // cOLORE DE FONDO CAMBIAR
                          child: Column(
                            children: <Widget>[
                              //--------var es------------
                              Container(
                                color: Color.fromRGBO(207, 227, 233, 1),
                                child: getAgrupado(context, GlobalVariables.listAgrupado),
                              ),


                            ],
                          ),
                        ),


                      ],
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );


  }

}
Widget botonRegresar(BuildContext context) {
  return Container(

    height: MediaQuery.of(context).size.height * 0.02,
  );
}
Widget getAgrupado(BuildContext context , List<InventarioAgrupado> listaAgrupado) {


  return Container(
    color: Color.fromRGBO(178, 222, 235, 1),

    padding: EdgeInsets.all(20),

    child: Column(children: <Widget>[

    for(var index = 0; index < listaAgrupado.length; index++)(


        InkWell(
          onTap: () {

        GlobalVariables.idAgrupador = GlobalVariables.listAgrupado[index].id;
        GlobalVariables.nameAgrupador = GlobalVariables.listAgrupado[index].descripcion;
        print('Detalle Campos ..........    ' + GlobalVariables.listAgrupado[index].id.toString() + ' Agrpador Add :: ' +  GlobalVariables.nameAgrupador);
        LoginBloc.loadingI(context,true);
        ImcRBloc.loadInventarioDetalleAgrupado(context,1);
        Future.delayed(Duration(seconds: 3), () => {
          LoginBloc.loadingI(context,false),

          Navigator.pushAndRemoveUntil(
            context,
            PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
              return InventarioDetalleAddAgrupacionScreen();
            }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
              return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
              ).animate(animation), child: child,);
            },
                transitionDuration: Duration(seconds: 1)
            ), (Route route) => false,)
        });


          }, // handle your onTap here
          child: Container(
          padding: EdgeInsets.all(10),
        alignment: Alignment.topLeft,
        decoration: BoxDecoration(
            color:  (index % 2) == 0 ? Color.fromRGBO(2, 142, 201, 1): Color.fromRGBO(65, 190, 223, 1),
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(0.0),
                bottomRight: Radius.circular(0.0))),
        height: MediaQuery.of(context).size.height * 0.08,
        child: Row(children: <Widget>[

          Text(listaAgrupado[index].descripcion,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontSize: 13)),
          new Icon(Icons.add_circle_outline,color: Colors.white),

        ]),
          ),
     // ),
  )
    ),
      Container(
        height: MediaQuery.of(context).size.height * 0.02,
      ),
      Material(
        elevation: 5.0,
        //borderRadius: BorderRadius.circular(30.0),
        color:  Color.fromRGBO(74, 191, 37, 1),
        child: MaterialButton(
          minWidth: MediaQuery.of(context).size.width / 2,
          onPressed: () async {

            print("Guardar::::...... ");



            if(GlobalVariables.controllerAPELLIDOS.text == '' ||
                GlobalVariables.controllerAPELLIDOS2.text == '' ||
                GlobalVariables.controllerNOMBRES.text == '' ||
                GlobalVariables.controllerNOMBRECOMPLETO.text == '' ||
                GlobalVariables.controllerNUMEMPLEADO.text == '' ||
                GlobalVariables.controllerVIP.text == '' ||
                GlobalVariables.controllerPUESTO.text == '' ||
                GlobalVariables.controllerDIRECCION.text == '' ||
                GlobalVariables.controllerSUBDIRECCION.text == '' ||
                GlobalVariables.controllerCLAVESUBDIRECCION.text == '' ||
                GlobalVariables.controllerGERENCIA.text == '' ||
                GlobalVariables.controllerCLAVEGERENCIA.text == '' ||
                GlobalVariables.controllerDEPTO.text == '' ||
                GlobalVariables.controllerCLAVECENTROTRABAJO.text == '' ||
                GlobalVariables.controllerCORREO.text == '' ||
                GlobalVariables.controllerTELEFONO.text == '' ||
                GlobalVariables.controllerEXT.text == '' ||
                GlobalVariables.controllerUBICACION.text == '' ||
                GlobalVariables.controllerESTADO.text == '' ||
                GlobalVariables.controllerCP.text == '' ||
                GlobalVariables.controllerCOLONIA.text == '' ||
                GlobalVariables.controllerUBICACIONCOMPLETA.text == '' ||
            GlobalVariables.controllerZONA.text == '' ||
            GlobalVariables.controllerLOCALIDAD.text == '' ||
            GlobalVariables.controllerEDIFICIO.text == '' ||
            GlobalVariables.controllerPISO.text == '' ||
            GlobalVariables.controllerAREA.text == ''




            ){
              showDialog(context: context, builder: (context) => ShowMessage('Existen campos vacios, favor de validar!'));
            }else{
              showDialog(context: context, builder: (context) => ShowMessageSave('Inventario guardado correctamente!'));

            //            LoginBloc.loadingI(context,true);
              Future.delayed(Duration(seconds: 3), () => {

                Navigator.pushAndRemoveUntil(
                  context,
                  PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                    return InventarioAgrupadoAddScreen();
                  }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                    return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                    ).animate(animation), child: child,);
                  },
                      transitionDuration: Duration(seconds: 1)
                  ), (Route route) => false,),
                ImcRBloc.insertInvet(context)
              });


              //limpiar();
            }



          },
          textColor: Colors.white,
          child: Text("Guardar",
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
      ),
      Container(
        height: MediaQuery.of(context).size.height * 0.02,
      ),
    ]),
  );
}




Future<bool> _getFutureBool() {
  return Future.delayed(Duration(milliseconds: 5000))
      .then((onValue) => true);
}

