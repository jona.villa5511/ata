import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ATA/src/complements/globalVariables.dart';
import 'package:ATA/src/complements/globalWidgets.dart';
import 'package:ATA/src/ui/Frame/InventarioAdd_scren.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';

import 'InventarioDetalle_scren.dart';


class PdfFinalScreen extends StatefulWidget {


  @override
  _PdfFinalScreenState createState() => _PdfFinalScreenState();
}

class _PdfFinalScreenState extends State<PdfFinalScreen> {


  String URLS;



  Future<String> get _localPath async {
    Directory tempDir = await getTemporaryDirectory();

    return tempDir.path;
  }
  Future<File> get _localFile async {
    final path = await _localPath;
    _showAlertDialog('$path/'+GlobalVariables.nombrePDF);
    print('rutaaaaaaaaaaaa ver ::....$path/'+GlobalVariables.nombrePDF);
    return File('$path/'+GlobalVariables.nombrePDF);
  }
  Future<File> writeCounter(Uint8List stream) async {
    final file = await _localFile;

    // Write the file
    return file.writeAsBytes(stream);
  }
  Future<Uint8List> fetchPost() async {
    final response = await http.get(GlobalVariables.urlPathPDF);
    final responseJson = response.bodyBytes;

    return responseJson;
  }

  loadPdf() async {
    writeCounter(await fetchPost());
    URLS = (await _localFile).path;

    if (!mounted) return;

    setState(() {});
  }

  @override
  void initState(){
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
  }

  @override
  dispose(){
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
  }

  void _showAlertDialog(String path) {
    showDialog(
        context: context,
        builder: (buildcontext) {
          return AlertDialog(
            title: Text("Titulo del alert"),
            content: Text(path),
            actions: <Widget>[
              RaisedButton(
                child: Text("CERRAR", style: TextStyle(color: Colors.white),),
                onPressed: (){ Navigator.of(context).pop(); },
              )
            ],
          );
        }
    );
  }

  @override
  Widget build(BuildContext context) {

    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp,DeviceOrientation.portraitDown]);
    final screenSize = MediaQuery.of(context).size;
    final screenWidth = screenSize.width;
    final screenHeight = screenSize.height;
    return Scaffold(
      appBar: AppBar(
        title: Text( GlobalVariables.nombrePDF),
        elevation: 0.0,
        leading: GlobalWidgets.addLeadingIcon(),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(12, 160, 219, 1),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.close),
            onPressed: () {

    if(  GlobalVariables.banderafirmaAdd =='1'){
      Future.delayed(Duration(seconds: 1), () => {

        Navigator.pushAndRemoveUntil(
          context,
          PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
            return InventarioAddScreen();
          }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
            return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
            ).animate(animation), child: child,);
          },
              transitionDuration: Duration(seconds: 1)
          ), (Route route) => false,)

      });
    }else{
      Future.delayed(Duration(seconds: 1), () => {

        Navigator.pushAndRemoveUntil(
          context,
          PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
            return InventarioDetalleScreen();
          }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
            return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
            ).animate(animation), child: child,);
          },
              transitionDuration: Duration(seconds: 1)
          ), (Route route) => false,)

      });
    }

            },
          ),
        ],
      ),
      body: Stack(
        children: <Widget>[
          Container(
              child:PDFViewer(document: GlobalVariables.doc,
            showPicker: false,
            showIndicator: false,
            showNavigation: true,
            indicatorBackground: Colors.blue,
          ))

        ],
      ),

    );
  }
}
