import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ATA/src/complements/globalVariables.dart';
import 'package:ATA/src/complements/globalWidgets.dart';
import 'package:ATA/src/ui/Frame/InventarioLoadFiles_scren.dart';
import 'package:photo_view/photo_view.dart';

class ViewImageFileScreenScreen extends StatefulWidget {



  @override
  _ViewImageFileScreenScreenState createState() => _ViewImageFileScreenScreenState();
}

class _ViewImageFileScreenScreenState extends State<ViewImageFileScreenScreen> {
 ImageChunkEvent _progress;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  Text(GlobalVariables.nombrePDF, maxLines: 1, textAlign: TextAlign.center, style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.white)),
        elevation: 0.0,
        leading: GlobalWidgets.addLeadingIcon(),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(12, 160, 219, 1),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.close),
            onPressed: () {

              Navigator.pushReplacement(
                  context,
                  PageRouteBuilder( pageBuilder: (context, anim1, anim2) => InventarioLoadFilesScreen(),
                    transitionsBuilder: (context, anim1,anim2, child) => Container(child: child),
                    transitionDuration: Duration(seconds: 2),
                  )
              );

            },
          ),
        ],
      ),
      body: GestureDetector(
        child: Center(
            child: PhotoView(
              loadingBuilder: (context, progress) => Center(
                child: Container(
                  width: 20.0,
                  height: 20.0,
                  child: CircularProgressIndicator(
                    value: _progress == null
                        ? null
                        : _progress.cumulativeBytesLoaded /
                        _progress.expectedTotalBytes,
                  ),
                ),
              ),
              imageProvider: NetworkImage( GlobalVariables.urlPathPDF),
            )
        ),
        onTap: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}