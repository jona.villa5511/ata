

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:ATA/src/blocs/ATA/ATA_bloc.dart';
import 'package:ATA/src/complements/globalVariables.dart';
import 'package:ATA/src/ui/Frame/Inventario_scren.dart';
class MensajeInicioSesionScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => MensajeInicioSesionScreenState();

}

class MensajeInicioSesionScreenState extends State<MensajeInicioSesionScreen> {
  //String remotePDFpath = "";
  StorageReference storageReference;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp,DeviceOrientation.portraitDown]);
    DateTime now1 = DateTime.now();
    var currentTime = new DateTime(now1.year, now1.month, now1.day);
    GlobalVariables.dateNow = currentTime.toString();
    //print('Fecha del dia  $currentTime');
    return Scaffold(
      //key: GlobalVariables.scaffoldKeyIMCR,

      backgroundColor: Color.fromRGBO(207, 227, 233, 1),


      body: Material(
        child: Container(
          color: Color.fromRGBO(207, 227, 233, 1),
          child: Column(
            children: <Widget>[

                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.10,
                        // child: Text('Para comenzar tus trabjos asignados ATA/ App solicitarà una fotografia al organismo asignado (fachada o puerta principal del inmueble). Esto con el fin de reportar tu asistencia en sitio.', style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color:  Colors.black)),
                        ),

                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.05,

                        ),

              Center ( child: Container(
                alignment: AlignmentDirectional.center,
                height: MediaQuery
                    .of(context)
                    .size
                    .height * 0.40,
                child: Container(
                  padding: EdgeInsets.fromLTRB(10, 15, 15, 5),
                  alignment: AlignmentDirectional.center,
                  child: Text(
                      'Para comenzar tus trabajos asignados ATA/ App solicitarà una fotografia al organismo asignado (fachada o puerta principal del inmueble). Esto con el fin de reportar tu asistencia en sitio.', style: TextStyle(fontSize: 17, fontWeight: FontWeight.w400, color:  Colors.black),

                      textAlign: TextAlign.justify,
                    textDirection: TextDirection.ltr),
              ),
              ),
              ),

              FlatButton(

                onPressed: () =>
                {
                  ImcRBloc.InicioSesion(context)

                },
                color: Color.fromRGBO(74, 191, 37, 1),
                padding: EdgeInsets.all(5.0),
                textColor: Colors.white,
                child: Column( // Replace with a Row for horizontal icon + text

                  children: <Widget>[
                    Icon(Icons.camera_alt),
                    Text("Tomar Foto",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),

//                      ],
//                    ),
//                  ),
//                ),
//              ),

            ],
          ),
        ),
      ),
    );
  }
}

Widget botonRegresar(BuildContext context) {
  return Container(
child:   FlatButton(
  child:
  Column( // Replace with a Row for horizontal icon + text
    children: <Widget>[
      Image(image: new AssetImage('assets/images/previous.png'),
          width: 25, height: 25, color: Colors.white),
      Text("Regresar",
        textAlign: TextAlign.center,
        style:TextStyle(fontWeight: FontWeight.bold,fontSize: 10,color: Colors.white),
      )
    ],
  ),
  onPressed: (){

    Navigator.pushAndRemoveUntil(
      context,
      PageRouteBuilder(

          pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
            return InventarioScreen();
          },

          transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
            return SlideTransition(
              position: Tween<Offset>(
                begin: Offset(1.0, 0.0),
                end: Offset.zero,
              ).animate(animation),
              child: child,
            );
          },

          transitionDuration: Duration(seconds: 1)
      ),

          (Route route) => false,
    );
  },
),
  );
}


