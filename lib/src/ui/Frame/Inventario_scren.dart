import 'dart:async';
import 'dart:io';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:ATA/src/blocs/ATA/ATA_bloc.dart';
import 'package:ATA/src/blocs/login_bloc.dart';
import 'package:ATA/src/complements/globalWidgets.dart';
import 'package:ATA/src/complements/globalVariables.dart';
import 'package:image_picker/image_picker.dart';
import 'package:geolocator/geolocator.dart';
import 'package:ATA/src/models/Inventario.dart';
import 'package:ATA/src/models/Proyects.dart';
import 'package:ATA/src/services/ApiDefinitions.dart';
import 'package:ATA/src/services/APiWebServices/Inventario_wsConsults.dart';
import 'package:ATA/src/ui/Frame/InventarioDetalle_scren.dart';
import 'package:flutter/services.dart';
class InventarioScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => InventarioScreenState();

}

class InventarioScreenState extends State<InventarioScreen> {
  File _pickedImage;
  LatLng _center ;
  Position currentLocation;
  final Map<String, Marker> _markers = {};

  @override
  void initState(){
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    DateTime now1 = DateTime.now();
    var currentTime = new DateTime(now1.year, now1.month, now1.day);
    GlobalVariables.dateNow = currentTime.toString();
  print('Cargando App...');
    return Scaffold(
      appBar:
        GlobalWidgets.topBar('EDITAR PROYECTO','Marco Antonio Moreno Silva', context, '$currentTime'.substring(0,10)),

      backgroundColor: Color.fromRGBO(207, 227, 233, 1),

      body: Material(
        child: Container(
          color: Color.fromRGBO(207, 227, 233, 1),
          child: Column(
            children: <Widget>[
              //------------Body------------
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    child: Column(
                      children: <Widget>[

                        GlobalWidgets.menuHome(context,'$currentTime'.substring(0,10),botonRegresar(context)),

                        Container(
                          height: MediaQuery.of(context).size.height * 0.02,

                        ),
                    Container(
                        height: MediaQuery.of(context).size.height * 0.02,
                        child:  Text(GlobalVariables.DescripProyect, maxLines: 2, style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Color.fromRGBO(9, 99, 141, 1))),

                    ),

                    Container(
                          color: Color.fromRGBO(12, 160, 219, 1), // cOLORE DE FONDO CAMBIAR
                          child: Column(
                            children: <Widget>[
                              //searchProyect(),
                              Container(
                                color: Color.fromRGBO(207, 227, 233, 1),
                                child: getProyects(context, GlobalVariables.listProyects),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),

    );


  }
  Widget botonRegresar(BuildContext context) {
      return Container(

        height: MediaQuery.of(context).size.height * 0.02,
      );
  }
  Widget searchProyect() => StreamBuilder<bool>(

    builder: (context, snap){
      return TextField(
        decoration: new InputDecoration(
          labelText: "Buscar",
          fillColor: Colors.blue,
          border: new OutlineInputBorder(
            borderRadius: new BorderRadius.circular(8.0),
            borderSide: new BorderSide(
            ),
          ),
          //fillColor: Colors.green
        ),
        onChanged: (text) {
          print("First text field: $text");
          ImcRBloc.loadIProyectBuscar(context, text);
        setState(() {
          Navigator.pushReplacement(
            context,
            PageRouteBuilder(pageBuilder: (context, anim1, anim2) => InventarioScreen(),
              transitionsBuilder: (context, anim1, anim2, child) => Container(child: child),//(context, anim1, anim2, child) => FadeTransition(opacity: anim1, child: child),
              transitionDuration: Duration(seconds: 1),
            )
        );
        });
        },
        textCapitalization: TextCapitalization.characters,
        autocorrect: false,
        textAlign: TextAlign.center,
        cursorColor: Colors.black,
        //maxLength: 8,
        //  decoration: GlobalStyles.decorationTFLogin('Escriba aqui...'),
      );
    },
  );

  void _pickImage() async {
    final imageSource = await showDialog<ImageSource>(
        context: context,
        builder: (context) =>
            AlertDialog(
              title: Text("Seleccione una imagen"),
              actions: <Widget>[
                MaterialButton(
                  child: Text("Camara"),
                  onPressed: () => Navigator.pop(context, ImageSource.camera),
                ),
                MaterialButton(
                  child: Text("Galeria"),
                  onPressed: () => Navigator.pop(context, ImageSource.gallery),
                )
              ],
            )
    );
    if(imageSource != null) {
      final file = await ImagePicker.pickImage(source: imageSource);
      if(file != null) {
        setState(() => _pickedImage = file);
      }
      //uploadFileASW(file);
      getUserLocation();
      getGoogleMaps(context, _center, _markers);
    }

  }
  Widget getBodyUploadFile(BuildContext context, LatLng _center, Map<String, Marker> _markers) {
    return   Container(
      height: MediaQuery.of(context).size.height * 1.0,
      child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  new Align(
                    alignment: Alignment.bottomCenter,
                    child: new SizedBox(
                      width: 50.0,
                      height: 45.0,
                      child: new RaisedButton(
                        padding: const EdgeInsets.all(10.0),
                        onPressed: (){
                          _pickImage();
                        },
                        color: Color.fromRGBO(170, 214, 225, 1),
                        child: new Icon(Icons.unarchive,color:  Color.fromRGBO(9, 99, 141, 1)),
                        shape: RoundedRectangleBorder(side: BorderSide(
                            color: Color.fromRGBO(0, 131, 186, 1),
                            width: 3,
                            style: BorderStyle.solid
                        ),
                          borderRadius: BorderRadius.circular(50),),
                      ),
                    ),

                  ),
                ]
            ),

          ]
      ),
    );
  }


  Future<Position> locateUser() async {
    return Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }

  getUserLocation() async {
    currentLocation = await locateUser();
    setState(() {
      _center = LatLng(currentLocation.latitude, currentLocation.longitude);
    });
    print('Ubicacion de la Foto:::: $_center');
  }


}
Widget getGoogleMaps(BuildContext context, LatLng _center, Map<String, Marker> _markers) {
  if(_center != null)
  return Container(
    height: MediaQuery.of(context).size.height * 2.0,
    child:GoogleMap(
      myLocationButtonEnabled: true,
      buildingsEnabled: true,
      myLocationEnabled: true,
      trafficEnabled: true,
      mapType: MapType.hybrid,
      initialCameraPosition: CameraPosition(
        target: _center,
        zoom: 17,
      ),
      markers: _markers.values.toSet(),
    ),
  );
}
 numeroPar(int index) async{
  if ((index % 2) == 0){
    print(index.toString() + ' es par');
  }else{
    print(index.toString() + ' es impar');
  }

}
Widget getProyects(BuildContext context , List<Proyects> listaProyects) {
  DateTime now1 = DateTime.parse("2020-05-20 20:18:00");
  var currentTime1 =
      new DateTime(now1.year, now1.month, now1.day, now1.hour, now1.minute);
  //print('$currentTime1');
  DateTime now2 = DateTime.parse("2020-05-21 16:14:00");
  var currentTime2 =
      new DateTime(now2.year, now2.month, now2.day, now2.hour, now2.minute);
  //print('$currentTime2');
  DateTime now3 = DateTime.parse("2020-05-15 19:42:00");
  var currentTime3 =
      new DateTime(now3.year, now3.month, now3.day, now3.hour, now3.minute);
  //print('$currentTime3');
    var color1 = Colors.amberAccent;
  var color2 = Colors.deepOrange;


  return Container(
    color: Color.fromRGBO(207, 227, 233, 1),

    padding: EdgeInsets.all(20),

    child: Column(children: <Widget>[

    for(var index = 0; index < listaProyects.length; index++)(


        InkWell(
          onTap: () {
            LoginBloc.loadingI(context,true);
            loadAllDatesIMC(context,listaProyects,index);
            Future.delayed(Duration(seconds: 3), () => {

              LoginBloc.loadingI(context,false),
              showDialog(context: context, builder: (context) => ShowInventarioForProyect(index))

            });

          }, // handle your onTap here
          child: Container(
            padding: EdgeInsets.all(15),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color:  (index % 2) == 0 ? Color.fromRGBO(9, 99, 141, 1): Color.fromRGBO(65, 190, 223, 1),
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0))),
            height: MediaQuery.of(context).size.height * 0.08,
            child: Row(children: <Widget>[
              new Icon(Icons.scatter_plot,color: Colors.white),
              Text(listaProyects[index].descripcion,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 14)),
              Expanded(
                child: Container(
                    //padding: EdgeInsets.fromLTRB(50, 0, 0, 10),
                  alignment: Alignment.bottomRight,
                  child: Text( listaProyects[index].fecha,  style: TextStyle(fontSize: 11, color: Colors.white)),


                ),
              ),
              Expanded(
                child: Container(
                    //padding: EdgeInsets.fromLTRB(100, 1, 11, 10),
                  alignment: Alignment.bottomRight,
                  child: new Icon(Icons.work,color: Colors.white),
                ),
              ),

        ]),
          ),
     // ),
  )
    ),

    ]),

  );
}


 void loadAllDatesIMC(BuildContext context,List<Proyects> listaProyects, int index) async {

   if(GlobalVariables.listIventario.isEmpty)
  print('Entrando a app cargando Ws -----> Inventario desde proyecto ///////' +  listaProyects[index].id.toString());

  getInvent(context, ApiDefinition.wsInventToProyect + listaProyects[index].id.toString()+ '/' + GlobalVariables.user.id.toString());
  //print(GlobalVariables.listIventario[0].toString());

}
Future<bool> _getFutureBool() {
  return Future.delayed(Duration(milliseconds: 5000))
      .then((onValue) => true);
}
Widget ShowInventarioForProyect(int index) =>
    StreamBuilder<bool>(builder: (context, snap) {


      return AlertDialog(

        backgroundColor: Color.fromRGBO(207, 227, 233, 1),
 scrollable: true,

        title: Column(

          children: <Widget>[

            Text('INVENTARIO',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color.fromRGBO(9, 99, 141, 1),
                    fontSize: 23)),
            Container(
              height: MediaQuery.of(context).size.height * 0.02,

            ),
            Row(
              children: <Widget>[
                Icon(Icons.work, color: Colors.blue),
                Text(GlobalVariables.listProyects[index].descripcion, textAlign: TextAlign.center, style: TextStyle( fontSize: 20, fontWeight: FontWeight.w900)),
              ],
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.02,

            ),
            searchFolio(index),

          ],
        ),

        content: Container(
          padding: EdgeInsets.all(20),
          color: Color.fromRGBO(178, 222, 235, 1),
          child: Column(
            children: <Widget>[
              for(var i = 0; i <  GlobalVariables.listIventario.length; i++)(
                 Column(
                children: <Widget>[
                     InkWell(
                      onTap: () {

                        print('PASAR A AGRUPADOR....' );
                        GlobalVariables.idProyect = GlobalVariables.listProyects[index].id;
                        GlobalVariables.idIventario =  GlobalVariables.listIventario[i].inventarioid;
                        GlobalVariables.nameProyectFile = GlobalVariables.listProyects[index].descripcion;
                        GlobalVariables.nameInvenFolioFile = GlobalVariables.listIventario[i].campoId;
                        GlobalVariables.nameAgrupador = GlobalVariables.listIventario[i].folio;
                        GlobalVariables.banderafirmaAdd ='0';
                        ImcRBloc.loadInventarioAgrupado(context, GlobalVariables.idProyect, 0);
                        print('id_proyect:: ' + GlobalVariables.listProyects[index].id.toString() + ' id_inventario::: ' + GlobalVariables.listIventario[i].inventarioid.toString());
                        ImcRBloc.loadInventarioAgrupado(context,GlobalVariables.listProyects[index].id, GlobalVariables.listIventario[i].inventarioid);
                       // GlobalVariables.idIventario = GlobalVariables.listIventario[i].nombrecompleto.toString();


                        LoginBloc.loadingI(context,true);
                        Future.delayed(Duration(seconds: 3), () => {
                          LoginBloc.loadingI(context,false),
//                          Navigator.pushReplacement(
//                              context,
//                              PageRouteBuilder(pageBuilder: (context, anim1, anim2) => InventarioDetalleScreen(),
//                                transitionsBuilder: (context, anim1, anim2, child) => Container(child: child),//(context, anim1, anim2, child) => FadeTransition(opacity: anim1, child: child),
//                                transitionDuration: Duration(seconds: 3),
//                              )
//                          ),
                        Navigator.pushAndRemoveUntil(
                        context,
                        PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                        return InventarioDetalleScreen();
                        }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                        return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                        ).animate(animation), child: child,);
                        },
                        transitionDuration: Duration(seconds: 1)
                        ), (Route route) => false,)

                        });

                      },

            child:
                  Container(
                    padding: EdgeInsets.all(10),
                    alignment: Alignment.topLeft,
                    decoration: BoxDecoration(
                        color: (i % 2) == 0 ? Color.fromRGBO(2, 142, 201, 1): Color.fromRGBO(16, 175, 205, 1),
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10.0),
                            bottomRight: Radius.circular(10.0))),
                    height: MediaQuery.of(context).size.height * 0.05,
                    child: Row(children: <Widget>[

                      Text(GlobalVariables.listIventario[i].campoId,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                              fontSize: 12)),

                      Expanded(
                        child: Container(
                          alignment: Alignment.topRight,
                          child: new Icon(Icons.library_books,color: Colors.white),
                        ),
                      ),
                    ]),
                  ),
                     ),
                ],
              )

                ),

          Container(
                          height: MediaQuery.of(context).size.height * 0.05,

                        ),
              Material(
                elevation: 5.0,
                //borderRadius: BorderRadius.circular(30.0),
                color:   Color.fromRGBO(9, 99, 141, 1),
                child: MaterialButton(
                  minWidth: MediaQuery.of(context).size.width *100,
                  onPressed: () async {

                    Navigator.pop(context);
                  },
                  textColor: Colors.white,
                  child: Text("Cancelar",
                    textAlign: TextAlign.center,

                    style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12,color: Colors.white),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    });

Widget searchFolio(int index) => StreamBuilder<bool>(

  builder: (context, snap){
    return TextField(
      decoration: new InputDecoration(
        labelText: "Buscar",
        fillColor: Colors.blue,
        border: new OutlineInputBorder(
          borderRadius: new BorderRadius.circular(8.0),
          borderSide: new BorderSide(
          ),
        ),
        //fillColor: Colors.green
      ),
      onChanged: (text) {
        print("First text field: $text");
        ImcRBloc.loadInventarioBuscar(context, GlobalVariables.listProyects[index].id.toString(), text);
//        setState(() {
//
//        });
      },
      textCapitalization: TextCapitalization.characters,
      autocorrect: false,
      textAlign: TextAlign.center,
      cursorColor: Colors.black,
      //maxLength: 8,
        //  decoration: GlobalStyles.decorationTFLogin('Escriba aqui...'),
    );
  },
);

getListInventarioKeyPreseed(BuildContext context, List<Inventario> listaInventario, String text, int index) async{
  //print('Lista a buscar' + listaInventario.length.toString()+ ' texttt '+ text );
  var  nameJ = listaInventario.where((inventario) => inventario.campoId.contains(text));
  // var list =  nameJ as List;
   var countList = nameJ.toList().length;

  GlobalVariables.listIventarioSearch = nameJ.toList();
  GlobalVariables.listIventario =  GlobalVariables.listIventarioSearch;

  print(GlobalVariables.listIventarioSearch.toString());


}


