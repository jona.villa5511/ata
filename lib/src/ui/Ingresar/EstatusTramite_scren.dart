import 'dart:io';


import 'package:ATA/src/ui/Ingresar/BienvenidaIngresar_scren.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:ATA/src/blocs/login_bloc.dart';
import 'package:ATA/src/complements/globalWidgets.dart';
import 'package:ATA/src/complements/globalVariables.dart';
import 'package:geolocator/geolocator.dart';
import 'package:ATA/src/ui/AgendaCitas_scren.dart';
class EstatusTramiteScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => EstatusTramiteScreenState();

}

class EstatusTramiteScreenState extends State<EstatusTramiteScreen> {
  File _pickedImage;
  LatLng _center;

  Position currentLocation;
  final Map<String, Marker> _markers = {};

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    DateTime now1 = DateTime.now();
    var currentTime = new DateTime(now1.year, now1.month, now1.day);
    GlobalVariables.dateNow = currentTime.toString();
    print('Cargando App...');
    return Scaffold(
      appBar:
      GlobalWidgets.topBar(
          'ESTATUS', ' Silva', context,
          '$currentTime'.substring(0, 10)),

      backgroundColor: Colors.black26,

      body: Material(
        child: Container(
          color: Colors.black12,
          child: Column(
            children: <Widget>[
              //------------Body------------
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        FlatButton(
                          child:
                          Column( // Replace with a Row for horizontal icon + text
                            children: <Widget>[
                              Image(image: new AssetImage('assets/images/previous.png'),
                                  width: 25, height: 25),
                              Text("Regresar",
                                textAlign: TextAlign.center,
                                style:TextStyle(fontWeight: FontWeight.bold,fontSize: 10,color: Color.fromRGBO(9, 99, 141, 1)),
                              )
                            ],
                          ),
                          onPressed: (){
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => BienvenidaIngresarScreen()),
                            );
                          },
                        ),

            Container(
              decoration: BoxDecoration(
                  color:  Color.fromRGBO(0, 132, 186, 1),

                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(0.0),
                      bottomLeft:Radius.circular(40.0),
                      topRight: Radius.circular(0.0),
                      bottomRight: Radius.circular(40.0))),

              child:  Row(

                children: <Widget>[

                ],
              ),
            ),

                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,

                        ),


                        Container(
                          child:  Text('¡ESTATUS DEL FOLIO!', maxLines: 1,  textAlign: TextAlign.center,
                            style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color: Color.fromRGBO(9, 99, 141, 1))
                          ), height: MediaQuery.of(context) .size.height * 0.02),
                        Container(height: MediaQuery.of(context) .size.height * 0.02),

                        Align (
                          alignment: Alignment.topCenter,
                          child: Text('Buscar Folio:', maxLines: 1,  textAlign: TextAlign.left,
                              style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,color: Color.fromRGBO(9, 99, 141, 1))
                          ),
                        ),
                        searchFolio(),
                        Container(height: MediaQuery.of(context) .size.height * 0.02),

                        GlobalVariables.controllerIngresoFolio.text == '147677' ? getEsattusAprobado(context) : Container(height: MediaQuery.of(context) .size.height * 0.00),
                        Container(height: MediaQuery.of(context) .size.height * 0.02),
                        GlobalVariables.controllerIngresoFolio.text == '667444' ? getEsattusEnproceso(context) : Container(height: MediaQuery.of(context) .size.height * 0.00),
                        Container(height: MediaQuery.of(context) .size.height * 0.02),
                        GlobalVariables.controllerIngresoFolio.text == '566933' ? getEsattusRechado(context): Container(height: MediaQuery.of(context) .size.height * 0.00)





                      ],
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),

    );
  }
  Widget searchFolio() => StreamBuilder<bool>(

    builder: (context, snap){
      return TextField(
        keyboardType: TextInputType.number,
        textCapitalization: TextCapitalization.characters,
        decoration: new InputDecoration(

          icon: new Icon(Icons.search,color: Color.fromRGBO(9, 99, 141, 1)),
          labelText: "Buscar",
          fillColor: Colors.blue,
          border: new OutlineInputBorder(
            borderRadius: new BorderRadius.circular(8.0),
            borderSide: new BorderSide(
            ),
          ),
          //fillColor: Colors.green
        ),
        onChanged: (text) {
          //print("First text field: $text");

        setState(() {
         // print("buscando $text");

          if(text=='147677'){
            print("aprobado");
            GlobalVariables.controllerIngresoFolio.text = text;
            getEsattusAprobado(context);
          }else if(text=='667444'){
            GlobalVariables.controllerIngresoFolio.text = text;
            getEsattusEnproceso(context);
            print("proceso");
          }else if(text=='566933'){
            GlobalVariables.controllerIngresoFolio.text = text;
            getEsattusRechado(context);
            print("rechado");
          }
        });
        },
        autocorrect: false,
        //maxLength: 8,
        //  decoration: GlobalStyles.decorationTFLogin('Escriba aqui...'),
      );
    },
  );
  Widget getEsattusAprobado(BuildContext context){
    return Container(
          padding: EdgeInsets.all(15),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color:  Colors.white70,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10.0),
                  bottomLeft:Radius.circular(40.0),
                  topRight: Radius.circular(10.0),
                  bottomRight: Radius.circular(40.0))),
          child: Row(children: <Widget>[
            getImage(context),
            Text(GlobalVariables.controllerIngresoFolio.text,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color.fromRGBO(9, 99, 141, 1),
                    fontSize: 14)),
            Expanded(
              child: Container(
                //padding: EdgeInsets.fromLTRB(50, 0, 0, 10),
                alignment: Alignment.bottomRight,
                child: Text( "APROBADA",  style: TextStyle(fontSize: 11, color: Color.fromRGBO(9, 99, 141, 1))),
              ),
            ),
            Expanded(
              child: Container(
                //padding: EdgeInsets.fromLTRB(100, 1, 11, 10),
                alignment: Alignment.bottomRight,
                child: new Icon(Icons.remove_red_eye,color: Color.fromRGBO(9, 99, 141, 1)),
              ),
            ),
          ]),
      );
  }

  Widget getEsattusEnproceso(BuildContext context){
    return Container(

          padding: EdgeInsets.all(15),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color:  Colors.white70,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10.0),
                  bottomLeft:Radius.circular(40.0),
                  topRight: Radius.circular(10.0),
                  bottomRight: Radius.circular(40.0))),
       child: Row(children: <Widget>[
            getImageProcesing(context),
            Text(GlobalVariables.controllerIngresoFolio.text,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color.fromRGBO(9, 99, 141, 1),
                    fontSize: 14)),
            Expanded(
              child: Container(
                //padding: EdgeInsets.fromLTRB(50, 0, 0, 10),
                alignment: Alignment.bottomRight,
                child: Text( "EN PROCESO",  style: TextStyle(fontSize: 11, color: Color.fromRGBO(9, 99, 141, 1))),
              ),
            ),
            Expanded(
              child: Container(
                //padding: EdgeInsets.fromLTRB(100, 1, 11, 10),
                alignment: Alignment.bottomRight,
                child: new Icon(Icons.remove_red_eye,color: Color.fromRGBO(9, 99, 141, 1)),
              ),
            ),
          ]),
    );
  }

  Widget getEsattusRechado(BuildContext context){
    return Container(
          padding: EdgeInsets.all(15),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color:  Colors.white70,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10.0),
                  bottomLeft:Radius.circular(40.0),
                  topRight: Radius.circular(10.0),
                  bottomRight: Radius.circular(40.0))),
          child: Row(children: <Widget>[
            getImageRechazada(context),
            Text(GlobalVariables.controllerIngresoFolio.text,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color.fromRGBO(9, 99, 141, 1),
                    fontSize: 14)),
            Expanded(
              child: Container(
                //padding: EdgeInsets.fromLTRB(50, 0, 0, 10),
                alignment: Alignment.bottomRight,
                child: Text( "RECHAZADO",  style: TextStyle(fontSize: 11, color: Color.fromRGBO(9, 99, 141, 1))),
              ),
            ),
            Expanded(
              child: Container(
                //padding: EdgeInsets.fromLTRB(100, 1, 11, 10),
                alignment: Alignment.bottomRight,
                child: new Icon(Icons.remove_red_eye,color: Color.fromRGBO(9, 99, 141, 1)),
              ),
            ),
          ]),
    );
  }
      Widget getImage(BuildContext context){
    return  Container(
      padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
      child:   new Column(
        children: <Widget>[
          FlatButton(
              child:   Image(image: new AssetImage('assets/images/aprobada.png'),
                  width: 35, height: 35),
              onPressed: () {


              }
          ),
        ],
      ),
    );
  }
  Widget getImageProcesing(BuildContext context){
    return  Container(
      padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
      child:   new Column(
        children: <Widget>[
          FlatButton(
              child:   Image(image: new AssetImage('assets/images/enproceso-png.png'),
                  width: 35, height: 35),
              onPressed: () {


              }
          ),
        ],
      ),
    );
  }
  Widget getImageRechazada(BuildContext context){
    return  Container(
      padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
      child:   new Column(
        children: <Widget>[
          FlatButton(
              child:   Image(image: new AssetImage('assets/images/rechazado.png'),
                  width: 45, height: 45),
              onPressed: () {


              }
          ),
        ],
      ),
    );
  }



}







