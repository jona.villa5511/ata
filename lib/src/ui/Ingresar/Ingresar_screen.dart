
import 'package:ATA/src/services/APiWebServices/Inventario_wsConsults.dart';
import 'package:ATA/src/ui/Ingresar/BienvenidaIngresar_scren.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:ATA/src/blocs/ATA/ATA_bloc.dart';
import 'package:ATA/src/blocs/login_bloc.dart';
import 'package:ATA/src/complements/globalStyles.dart';
import 'package:ATA/src/complements/globalVariables.dart';
import 'package:flutter/services.dart';
import 'package:ATA/src/complements/globalWidgets.dart';
import 'package:ATA/src/ui/Bienvenida_scren.dart';
import 'package:ATA/src/ui/login_screen.dart';
import 'package:local_auth/local_auth.dart';
import 'package:ATA/src/database/userData_database.dart';

class IngresarScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => IngresarScreenState();
}
class IngresarScreenState extends State<IngresarScreen> {


  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color:   Color.fromRGBO(207, 227, 233, 1),
      child: Column(
        //crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[


          Container(
          color:   Color.fromRGBO(207, 227, 233, 1),
              width: MediaQuery
                  .of(context)
                  .size
                  .width * 0.70,
              child: Column(

                children: <Widget>[
                  SizedBox(height: 50),
                  //titleLogin(),
                  //leyendCorporation(),
                ],
              ),
            ),

          FlatButton(
            child:
            Column( // Replace with a Row for horizontal icon + text
              children: <Widget>[
                Image(image: new AssetImage('assets/images/previous.png'),
                    width: 25, height: 25),
                Text("Regresar",
                  textAlign: TextAlign.center,
                  style:TextStyle(fontWeight: FontWeight.bold,fontSize: 10,color: Color.fromRGBO(9, 99, 141, 1)),
                )
              ],
            ),
            onPressed: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => LoginScreen()),
              );
            },
          ),

          getImageFolio(context),
          Container(
            height: MediaQuery
                .of(context)
                .size
                .height * 0.10,
          ),


          TextFormField(
            keyboardType: TextInputType.number,
            textCapitalization: TextCapitalization.characters,
            style: TextStyle(
              color: Colors.black,
            ),
            decoration: new InputDecoration(

              icon: new Icon(Icons.confirmation_number,color: Color.fromRGBO(9, 99, 141, 1)),
              labelText: "INGRESA EL FOLIO",
              fillColor: Colors.white,
              border: new OutlineInputBorder(
                borderRadius: new BorderRadius.circular(6.0),
                borderSide: new BorderSide(),
              ),
            ),
            controller:  GlobalVariables.controllerIngresoFolio,


          ),
          Flexible(
            flex: 1,
            child: Container(
              color:   Color.fromRGBO(207, 227, 233, 1),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  Material(
                    elevation: 9.0,
                    //borderRadius: BorderRadius.circular(30.0),
                    color: Color.fromRGBO(36, 90, 149, 1),
                    child: MaterialButton(
                      minWidth: MediaQuery
                          .of(context)
                          .size
                          .width / 2,
                      //padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      onPressed: () async {

                        if(GlobalVariables.controllerIngresoFolio.text =='147677'){
                          print("valido");
                          Navigator.pushAndRemoveUntil(
                            context,
                            PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                              return BienvenidaIngresarScreen();
                            }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                              return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                              ).animate(animation), child: child,);
                            },
                                transitionDuration: Duration(seconds: 1)
                            ), (Route route) => false,);
                        }else if(GlobalVariables.controllerIngresoFolio.text=='667444'){
                          print("valido");
                          Navigator.pushAndRemoveUntil(
                            context,
                            PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                              return BienvenidaIngresarScreen();
                            }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                              return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                              ).animate(animation), child: child,);
                            },
                                transitionDuration: Duration(seconds: 1)
                            ), (Route route) => false,);
                        }else if(GlobalVariables.controllerIngresoFolio.text=='566933'){
                          print("valido");
                          Navigator.pushAndRemoveUntil(
                            context,
                            PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                              return BienvenidaIngresarScreen();
                            }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                              return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                              ).animate(animation), child: child,);
                            },
                                transitionDuration: Duration(seconds: 1)
                            ), (Route route) => false,);
                        }else{
                          print("Folio no valido");
                          showDialog(context: context, builder: (context) => ShowMessageValidateAdd('Folio invalido','Vuelve a intentarlo.'));
                        }


                      },
                      textColor: Colors.white,
                      child:  Row( children: <Widget>[
                        new Icon(Icons.vignette,color: Colors.white),
                        Container(
                          width: MediaQuery.of(context).size.width *0.20,
                        ),
                      Align (
                          alignment: Alignment.topCenter,
                          child:Text("ENTRAR",
                            textAlign: TextAlign.center,
                            style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,color: Colors.white)),),
                        Container(
                          width: MediaQuery.of(context).size.width *0.15,
                        ),
                        new Icon(Icons.code,color: Colors.white),
                      ]),
                    ),
                  ),

                  //loginButton()
                ],
              ),
            ),
          ),

        ],
      ),
    );
  }
  Widget getImageFolio(BuildContext context){
    return  Container(
      padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
      child:   new Column(
        children: <Widget>[
          FlatButton(
              child:   Image(image: new AssetImage('assets/images/folio3.png'),
                  width: 150, height: 150),
              onPressed: () {


              }
          ),
        ],
      ),
    );
  }

  Widget titleLogin() =>
      StreamBuilder<bool>(
        builder: (context, snap) {
          return Row(
            children: <Widget>[
              Container(
                alignment: Alignment.bottomRight,
                child: ClipRRect(

                  borderRadius: BorderRadius.circular(80.0),
                  child: Image.asset(
                    'assets/images/Logo_ISAe.png',
                    width: 200.0,
                    height: 100.0,

                  ),
                ),

              ),
//        Container(
//
//          child: GlobalStyles.titleLogin('ISAE'),
////        GlobalStyles.titleLogin(' S'),
////        GlobalStyles.titleLogin(' A'),
////        GlobalStyles.titleLogin(' E'),
//
//        ),

            ],


          );
        },
      );
}
