import 'dart:io';


import 'package:ATA/src/ui/Ingresar/DescargaDocumentos_scren.dart';
import 'package:ATA/src/ui/Ingresar/Entrega_scren.dart';
import 'package:ATA/src/ui/Ingresar/EstatusTramite_scren.dart';
import 'package:ATA/src/ui/Ingresar/TramiteProrroga_scren.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:ATA/src/blocs/login_bloc.dart';
import 'package:ATA/src/complements/globalWidgets.dart';
import 'package:ATA/src/complements/globalVariables.dart';
import 'package:geolocator/geolocator.dart';
import 'package:ATA/src/ui/AgendaCitas_scren.dart';
class BienvenidaIngresarScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => BienvenidaIngresarScreenState();

}

class BienvenidaIngresarScreenState extends State<BienvenidaIngresarScreen> {
  File _pickedImage;
  LatLng _center;

  Position currentLocation;
  final Map<String, Marker> _markers = {};

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    DateTime now1 = DateTime.now();
    var currentTime = new DateTime(now1.year, now1.month, now1.day);
    GlobalVariables.dateNow = currentTime.toString();
    print('Cargando App...');
    return Scaffold(
      appBar:
      GlobalWidgets.topBar(
          'BIENVENIDO', ' Silva', context,
          '$currentTime'.substring(0, 10)),

      backgroundColor: Colors.black26,

      body: Material(
        child: Container(
          color: Colors.black12,
          child: Column(
            children: <Widget>[
              //------------Body------------
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    child: Column(
                      children: <Widget>[


            Container(
              decoration: BoxDecoration(
                  color:  Color.fromRGBO(0, 132, 186, 1),

                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(0.0),
                      bottomLeft:Radius.circular(40.0),
                      topRight: Radius.circular(0.0),
                      bottomRight: Radius.circular(40.0))),

              child:  Row(

                children: <Widget>[

                ],
              ),
            ),

                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,

                        ),


                        Container(
                          child:  Text('¡BIENVENIDO A LA AGENDA DE CITAS!', maxLines: 1,  textAlign: TextAlign.center,
                            style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color: Color.fromRGBO(9, 99, 141, 1))
                          ),
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,

                        ),
                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,

                        ),

                        Container(
                          width: MediaQuery.of(context).size.width * 0.9,
                          padding: EdgeInsets.fromLTRB(0, 10, 1, 1),
                          color:  Colors.white,
                          child: MaterialButton(
                            minWidth: MediaQuery.of(context).size.width / 2,
                            //padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            onPressed: () async {

                              Navigator.pushAndRemoveUntil(
                                context,
                                PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                                  return EstatusTramiteScreen();
                                }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                                  return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                                  ).animate(animation), child: child,);
                                },
                                    transitionDuration: Duration(seconds: 1)
                                ), (Route route) => false,);
                            },
                            textColor: Colors.blue,
                            child: Row( children: <Widget>[
                              new Icon(Icons.menu,color: Color.fromRGBO(62, 95, 138, 1)),
                              Container(
                                width: MediaQuery.of(context).size.width *0.02,
                              ),
                              Align (
                                alignment: Alignment.topCenter,
                                child:Text('Estatus del tramite',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontWeight: FontWeight.bold,   decoration:
                                  TextDecoration.underline)),),
                              Container(
                                width: MediaQuery.of(context).size.width *0.02,
                              ),
                              new Icon(Icons.insert_chart,color: Color.fromRGBO(62, 95, 138, 1)),
                            ]),
                          ),
                        ),
                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.01,

                        ),

                        Container(
                          width: MediaQuery.of(context).size.width * 0.9,
                          padding: EdgeInsets.fromLTRB(0, 10, 1, 1),
                          color:  Colors.white,
                          child: MaterialButton(
                            minWidth: MediaQuery.of(context).size.width / 2,
                            //padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            onPressed: () async {

                              Navigator.pushAndRemoveUntil(
                                context,
                                PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                                  return EntregaScreen();
                                }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                                  return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                                  ).animate(animation), child: child,);
                                },
                                    transitionDuration: Duration(seconds: 1)
                                ), (Route route) => false,);
                            },
                            textColor: Colors.blue,
                            child: Row( children: <Widget>[
                              new Icon(Icons.menu,color: Color.fromRGBO(62, 95, 138, 1)),
                              Container(
                                width: MediaQuery.of(context).size.width *0.02,
                              ),
                              Align (
                                alignment: Alignment.topCenter,
                                child:Text('Entrega',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontWeight: FontWeight.bold,   decoration:
                                    TextDecoration.underline)),),
                              Container(
                                width: MediaQuery.of(context).size.width *0.02,
                              ),
                              new Icon(Icons.check_box,color: Color.fromRGBO(62, 95, 138, 1)),
                            ]),
                          ),
                        ),
                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.01,

                        ),

                        Container(
                          width: MediaQuery.of(context).size.width * 0.9,
                          padding: EdgeInsets.fromLTRB(0, 10, 1, 1),
                          color:  Colors.white,
                          child: MaterialButton(
                            minWidth: MediaQuery.of(context).size.width / 2,
                            //padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            onPressed: () async {


                              Navigator.pushAndRemoveUntil(
                                context,
                                PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                                  return TramiteProrrogaScreen();
                                }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                                  return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                                  ).animate(animation), child: child,);
                                },
                                    transitionDuration: Duration(seconds: 1)
                                ), (Route route) => false,);
                            },
                            textColor: Colors.blue,
                            child: Row( children: <Widget>[
                              new Icon(Icons.menu,color: Color.fromRGBO(62, 95, 138, 1)),
                              Container(
                                width: MediaQuery.of(context).size.width *0.02,
                              ),
                              Align (
                                alignment: Alignment.topCenter,
                                child:Text('Tramite de prorroga',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontWeight: FontWeight.bold,   decoration:
                                    TextDecoration.underline)),),
                              Container(
                                width: MediaQuery.of(context).size.width *0.02,
                              ),
                              new Icon(Icons.sync_problem,color: Color.fromRGBO(62, 95, 138, 1)),
                            ]),
                          ),
                        ),
                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.01,

                        ),


                        Container(
                          width: MediaQuery.of(context).size.width * 0.9,
                          padding: EdgeInsets.fromLTRB(0, 10, 1, 1),
                          color:  Colors.white,
                          child: MaterialButton(
                            minWidth: MediaQuery.of(context).size.width / 2,
                            //padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            onPressed: () async {

                              Navigator.pushAndRemoveUntil(
                                context,
                                PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                                  return DescargarDocumentosScreen();
                                }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                                  return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                                  ).animate(animation), child: child,);
                                },
                                    transitionDuration: Duration(seconds: 1)
                                ), (Route route) => false,);

                            },
                            textColor: Colors.blue,
                            child: Row( children: <Widget>[
                              new Icon(Icons.menu,color: Color.fromRGBO(62, 95, 138, 1)),
                              Container(
                                width: MediaQuery.of(context).size.width *0.02,
                              ),
                              Align (
                                alignment: Alignment.topCenter,
                                child:Text('Descarga de documentos',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontWeight: FontWeight.bold,   decoration:
                                    TextDecoration.underline)),),
                              Container(
                                width: MediaQuery.of(context).size.width *0.02,
                              ),
                              new Icon(Icons.cloud_download,color: Color.fromRGBO(62, 95, 138, 1)),
                            ]),
                          ),
                        ),

                        Container(
                          alignment: Alignment.bottomCenter,
                          child:  Image.asset(
                            'assets/images/app_icon.png',
                            width: 200.0,
                            height: 200.0,
                          ),
                        ),

                      ],
                    ),
                  ),
                ),
              ),



            ],
          ),
        ),
      ),

    );
  }

  Widget botonRegresar(BuildContext context) {
    return Container(

      height: MediaQuery
          .of(context)
          .size
          .height * 0.02,
    );
  }



}







