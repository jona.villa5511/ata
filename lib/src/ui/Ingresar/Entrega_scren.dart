import 'dart:io';


import 'package:ATA/src/ui/Ingresar/BienvenidaIngresar_scren.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:ATA/src/blocs/login_bloc.dart';
import 'package:ATA/src/complements/globalWidgets.dart';
import 'package:ATA/src/complements/globalVariables.dart';
import 'package:geolocator/geolocator.dart';
import 'package:ATA/src/ui/AgendaCitas_scren.dart';
class EntregaScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => EntregaScreenState();

}

class EntregaScreenState extends State<EntregaScreen> {
  File _pickedImage;
  LatLng _center;

  Position currentLocation;
  final Map<String, Marker> _markers = {};

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    DateTime now1 = DateTime.now();
    var currentTime = new DateTime(now1.year, now1.month, now1.day);
    GlobalVariables.dateNow = currentTime.toString();
    print('Cargando App...');
    return Scaffold(
      appBar:
      GlobalWidgets.topBar(
          'ENTREGA', ' Silva', context,
          '$currentTime'.substring(0, 10)),

      backgroundColor: Colors.black26,

      body: Material(
        child: Container(
          color: Colors.black12,
          child: Column(
            children: <Widget>[
              //------------Body------------
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        FlatButton(
                          child:
                          Column( // Replace with a Row for horizontal icon + text
                            children: <Widget>[
                              Image(image: new AssetImage('assets/images/previous.png'),
                                  width: 25, height: 25),
                              Text("Regresar",
                                textAlign: TextAlign.center,
                                style:TextStyle(fontWeight: FontWeight.bold,fontSize: 10,color: Color.fromRGBO(9, 99, 141, 1)),
                              )
                            ],
                          ),
                          onPressed: (){
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => BienvenidaIngresarScreen()),
                            );
                          },
                        ),

            Container(
              decoration: BoxDecoration(
                  color:  Color.fromRGBO(0, 132, 186, 1),

                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(0.0),
                      bottomLeft:Radius.circular(40.0),
                      topRight: Radius.circular(0.0),
                      bottomRight: Radius.circular(40.0))),

              child:  Row(

                children: <Widget>[

                ],
              ),
            ),

                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,

                        ),


                        Container(
                          child:  Text('¡Datos de entrega Carnet ATA!', maxLines: 1,  textAlign: TextAlign.center,
                            style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color: Color.fromRGBO(9, 99, 141, 1))
                          ), height: MediaQuery.of(context) .size.height * 0.02),
                        Container(height: MediaQuery.of(context) .size.height * 0.02),
                        getImage(context),
                        getDatos()

                      ],
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),

    );
  }
  Widget getDatos(){
    return  Container(
      padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
      child: Form(
        child: SizedBox(

          width: double.infinity,
          child: Column(
              children: <Widget>[

                Container(
                  child: Column(children: <Widget>[

                    Table(
                        border: TableBorder.all(width: 1.8, color:  Color.fromRGBO(207, 227, 233, 1)),
                        children: [

                          TableRow(children: [
                            Column(children: [
                              SizedBox(
                                  width: double.infinity,
                                  child: Card(
                                    color: Color.fromRGBO(63, 168, 214, 1),
                                    child: Padding(
                                      padding: EdgeInsets.all(11.0),
                                      child: Text('DIA', style: TextStyle(fontWeight: FontWeight.bold,
                                          color: Colors.white,
                                          fontSize: 14)),
                                    ),
                                  )
                              ),
                            ]),
                          ]),

                          TableRow(children: [
                            Column(children: [
                              Container(
                                color: Color.fromRGBO(63, 168, 214, 1),
                                padding: EdgeInsets.fromLTRB(300, 1, 1, 10),
                                alignment: Alignment.bottomCenter,
                                height: MediaQuery.of(context).size.height * 0.0,
                                child: Row ( children: <Widget>[
                                  Icon(Icons.today, color: Colors.blue),
                                ]
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.fromLTRB(8, 1, 1, 10),
                                child :SizedBox(
                                  height: MediaQuery.of(context).size.height * 0.05,
                                  width: double.maxFinite,
                                  child:
                                  Align (
                                    alignment: Alignment.centerLeft,
                                    child:  Text('Viernes 27 de Noviembre del 2020', style: TextStyle(fontWeight: FontWeight.bold,
                                        color: Colors.black38,
                                        fontSize: 16)
                                    ),
                                  )
                              ),
                              ),
                            ]
                            )
                          ]),

                          TableRow(children: [
                            Column(children: [
                              SizedBox(
                                  width: double.infinity,
                                  child: Card(
                                    color: Color.fromRGBO(63, 168, 214, 1),
                                    child: Padding(
                                      padding: EdgeInsets.all(16.0),
                                      child: Text('Sucursal', style: TextStyle(fontWeight: FontWeight.bold,
                                          color: Colors.white,
                                          fontSize: 14)),
                                    ),
                                  )
                              ),
                            ]),
                          ]),

                          TableRow(children: [
                            Column(children: [
                              Container(
                                color: Color.fromRGBO(63, 168, 214, 1),
                                padding: EdgeInsets.fromLTRB(300, 1, 1, 10),
                                alignment: Alignment.topRight,
                                height: MediaQuery.of(context).size.height * 0.0,
                                child: Row ( children: <Widget>[
                                  Icon(Icons.account_balance, color: Colors.blue),
                                ]
                                ),
                              ),
                              Container(
                                  padding: EdgeInsets.fromLTRB(8, 1, 1, 10),
                                  child :SizedBox(
                                  height: MediaQuery.of(context).size.height * 0.05,
                                  width: double.infinity,
                                  child:
                                  Align (
                                    alignment: Alignment.centerLeft,
                                    child:  Text('0224', style: TextStyle(fontWeight: FontWeight.bold,
                                        color: Colors.black38,
                                        fontSize: 16)
                                    ),
                                  ),),
                              ),
                            ]
                            )
                          ]),


                          TableRow(children: [
                            Column(children: [
                              SizedBox(
                                  width: double.infinity,
                                  child: Card(
                                    color: Color.fromRGBO(63, 168, 214, 1),
                                    child: Padding(
                                      padding: EdgeInsets.all(16.0),
                                      child: Text('Direccion', style: TextStyle(fontWeight: FontWeight.bold,
                                          color: Colors.white,
                                          fontSize: 14)),
                                    ),
                                  )
                              ),
                            ]),
                          ]),

                          TableRow(children: [
                            Column(children: [
                              Container(
                                color: Color.fromRGBO(63, 168, 214, 1),
                                padding: EdgeInsets.fromLTRB(300, 1, 1, 10),
                                alignment: Alignment.topRight,
                                height: MediaQuery.of(context).size.height * 0.0,
                                child: Row ( children: <Widget>[
                                  Icon(Icons.directions_bike, color: Colors.blue),
                                ]
                                ),
                              ),
                              Container(
                                  padding: EdgeInsets.fromLTRB(8, 1, 1, 10),
                                  child : SizedBox(
                                  height: MediaQuery.of(context).size.height * 0.05,
                                  width: double.infinity,
                                  child:
                                  Align (
                                    alignment: Alignment.centerLeft,
                                    child:  Text('Calle Olivos Numero 3 Colonia centro Tepeji del Rio Hgo.',  maxLines: 2,style: TextStyle(fontWeight: FontWeight.bold,
                                        color: Colors.black38,
                                        fontSize: 12)
                                    ),
                                  ),),
                              ),
                            ]
                            )
                          ]),




                        ]
                    )
                  ]),
                ),



              ]),
          //),
        ),



      ),

    );
  }
  Widget getImage(BuildContext context){
    return  Container(
      padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
      child:   new Column(
        children: <Widget>[
          FlatButton(
              child:   Image(image: new AssetImage('assets/images/entrega1.png'),
                  width: 150, height: 150),
              onPressed: () {


              }
          ),
        ],
      ),
    );
  }
}







