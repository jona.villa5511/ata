import 'dart:convert';
import 'dart:io';


import 'package:ATA/src/services/APiWebServices/Inventario_wsConsults.dart';
import 'package:ATA/src/ui/Frame/PdfPreviewScreen.dart';
import 'package:ATA/src/ui/Frame/ViewImageFileScreen.dart';
import 'package:ATA/src/ui/Ingresar/BienvenidaIngresar_scren.dart';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:ATA/src/blocs/login_bloc.dart';
import 'package:ATA/src/complements/globalWidgets.dart';
import 'package:ATA/src/complements/globalVariables.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as path;
class MensajeDescargarProrrogaScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => MensajeDescargarProrrogaScreenState();

}

class MensajeDescargarProrrogaScreenState extends State<MensajeDescargarProrrogaScreen> {
  File _pickedImage;
  String URLS;
  String _pathUploadFile = '';
  String fileName ='';
  Map<String, String> _paths;
  String _extension;
  bool _loadingPath = false;
  bool _multiPick = false;
  FileType _pickingType = FileType.any;
  TextEditingController _controller = new TextEditingController();
  String _fileName;
  String _pathFile;
  List<StorageUploadTask> _tasks = <StorageUploadTask>[];
  StorageReference storageReference;
  final Dio _dio = Dio();
  String _progress = "-";
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  String _uploadedFileURL;

  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
  new GlobalKey<RefreshIndicatorState>();

  Position currentLocation;
  final Map<String, Marker> _markers = {};

  @override
  void dispose(){
    super.dispose();
  }
  Future<void> turnOffNotification(
      FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin) async {
    await flutterLocalNotificationsPlugin.cancelAll();
  }

  Future<void> turnOffNotificationById(
      FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin,
      num id) async {
    await flutterLocalNotificationsPlugin.cancel(id);
  }

  Future<void> initNotification() async {

    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    final android = AndroidInitializationSettings('@mipmap/app_icon');
    final iOS = IOSInitializationSettings();
    final initSettings = InitializationSettings(android, iOS);
    flutterLocalNotificationsPlugin.initialize(initSettings, onSelectNotification: _onSelectNotification);
  }

  @override
  void initState() {
    super.initState();
    disablePathProviderPlatformOverride = true;

    initNotification();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => _refreshIndicatorKey.currentState.show());

  }
  Future<void> _onSelectNotification(String json) async {
    final obj = jsonDecode(json);

    if (obj['isSuccess'] && GlobalVariables.banderaDownload) {
      OpenFile.open(obj['filePath']);
      GlobalVariables.banderaDownload = false;
    } else {

    }
  }

  Future<void> _showNotification(Map<String, dynamic> downloadStatus) async {
    final android = AndroidNotificationDetails(
        'channel id',
        'channel name',
        'channel description',
        priority: Priority.High,
        importance: Importance.Max
    );
    final iOS = IOSNotificationDetails();
    final platform = NotificationDetails(android, iOS);
    final json = jsonEncode(downloadStatus);
    final isSuccess = downloadStatus['isSuccess'];

    await flutterLocalNotificationsPlugin.show(
        0, // notification id
        isSuccess ? 'Descargado' : 'Fallo',
        isSuccess ? 'Archivo se ha descargado correctamente!' : 'Ocurrio un error al Descargar.',
        platform,
        payload: json
    );
  }

  Future<Directory> _getDownloadDirectory() async {

    return await getApplicationDocumentsDirectory();
  }

  Future<bool> _requestPermissions() async {

    return true;
  }

  void _onReceiveProgress(int received, int total) {
    if (total != -1) {
      setState(() {
        _progress = (received / total * 100).toStringAsFixed(0) + "%";
      });
    }
  }

  Future<void> _startDownload(String savePath,String _fileURL) async {
    Map<String, dynamic> result = {
      'isSuccess': false,
      'filePath': null,
      'error': null,
    };

    try {
      final response = await _dio.download(
          _fileURL,
          savePath,
          onReceiveProgress: _onReceiveProgress
      );
      result['isSuccess'] = response.statusCode == 200;
      result['filePath'] = savePath;
      LoginBloc.Downloading(context,false);
    } catch (ex) {
      result['error'] = ex.toString();
    } finally {
      await _showNotification(result);
    }
  }

  Future<void> _download(String _fileURL, String _nombreArchivo) async {
    final dir = await _getDownloadDirectory();

    final isPermissionStatusGranted = await _requestPermissions();

    if (isPermissionStatusGranted) {
      final savePath = path.join(dir.path, _nombreArchivo);
      await _startDownload(savePath,_fileURL);
    } else {
      // handle the scenario when user declines the permissions
    }
  }
  void pickImage(BuildContext context) async {
    var picture = await ImagePicker.pickImage(source: ImageSource.gallery);
    this.setState(() {
      _pickedImage = picture;
    });


    String fileName = _pickedImage.path
        .split('/')
        .last;

    print(fileName);
  }
  void _openFileExplorer() async {
    setState(() => _loadingPath = true);
    try {
      if (_multiPick) {
        _pathFile = null;
        _paths = await FilePicker.getMultiFilePath(
            type: _pickingType,
            allowedExtensions: (_extension?.isNotEmpty ?? false)
                ? _extension?.replaceAll(' ', '')?.split(',')
                : null);
      } else {
        _paths = null;
        _pathFile = await FilePicker.getFilePath(
            type: _pickingType,
            allowedExtensions: (_extension?.isNotEmpty ?? false)
                ? _extension?.replaceAll(' ', '')?.split(',')
                : null);
        print('path ' + _pathFile);
        File file = new File(_pathFile);
        fileName = file.path.split('/').last;

        print(fileName);
        if(_pathFile.contains('pdf') || _pathFile.contains('xls')){
          print('es correecto el formato ');

          LoginBloc.loadingI(context,true);

          Directory documentDirectory =
          await getApplicationDocumentsDirectory();
          String documentPath = documentDirectory.path;
          _pathUploadFile = "$documentPath/$fileName";
          File temp = File(_pathFile);
          File newFile = await temp.copy(_pathUploadFile);
          print('path:: ' + _pathUploadFile);

          print('fileFinal:: ' + newFile.path);


          StorageReference storageReference = FirebaseStorage.instance
              .ref()
              .child('ATA/$fileName');
          StorageUploadTask uploadTask = storageReference.putFile(temp);
          await uploadTask.onComplete;
          print('File Uploaded');

          var dowurl = await (await uploadTask.onComplete).ref.getDownloadURL();
          _uploadedFileURL = dowurl.toString();

          print('urllllllllll  ' +_uploadedFileURL);



            LoginBloc.loadingI(context, false);





        }else{
          print('formato incorrecto');
          showDialog(context: context, builder: (context) => ShowMessage('Formato incorrecto solo admite PDF ò XLSX.'));
        }



      }
    } catch (e) {
      print("Error al cargar::: " + e.toString());
    }
    if (!mounted) return;
    setState(() {
      _loadingPath = false;
      _fileName = _pathFile != null
          ? _pathFile.split('/').last
          : _paths != null ? _paths.keys.toString() : '...';
    });
  }


  @override
  Widget build(BuildContext context) {
    DateTime now1 = DateTime.now();
    var currentTime = new DateTime(now1.year, now1.month, now1.day);
    GlobalVariables.dateNow = currentTime.toString();
    print('Cargando App...');
    return Scaffold(
      appBar:
      GlobalWidgets.topBar(
          'PRORROGA', ' Silva', context,
          '$currentTime'.substring(0, 10)),

      backgroundColor: Colors.black26,

      body: Material(
        child: Container(
          color: Colors.black12,
          child: Column(
            children: <Widget>[
              //------------Body------------
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    child: Column(
                      children: <Widget>[

                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,
                        ),
                        FlatButton(
                          child:
                          Column( // Replace with a Row for horizontal icon + text
                            children: <Widget>[
                              Image(image: new AssetImage('assets/images/previous.png'),
                                  width: 25, height: 25),
                              Text("Salir",
                                textAlign: TextAlign.center,
                                style:TextStyle(fontWeight: FontWeight.bold,fontSize: 10,color: Color.fromRGBO(9, 99, 141, 1)),
                              )
                            ],
                          ),
                          onPressed: (){
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => BienvenidaIngresarScreen()),
                            );
                          },
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(0, 132, 186, 1),

                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(0.0),
                                  bottomLeft: Radius.circular(40.0),
                                  topRight: Radius.circular(0.0),
                                  bottomRight: Radius.circular(40.0))),

                          child: Row(

                            children: <Widget>[
                            ],
                          ),
                        ),

                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,

                        ),


                        Container(
                            child: Text('¡CITA CON PRORROGA!', maxLines: 1,
                                textAlign: TextAlign.center,
                                style: TextStyle(fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                    color: Color.fromRGBO(9, 99, 141, 1))
                            ), height: MediaQuery
                            .of(context)
                            .size
                            .height * 0.04),

                        getImage(context),

                        getInputDate(context),
                        Container(
                          padding: EdgeInsets.all(20),
                          color: Color.fromRGBO(178, 222, 235, 1),
                          child: Column(
                            children: <Widget>[
                              Text('Tu cita fue agendanda correctamente, Descargar acuse de confirmación de cita.', textAlign: TextAlign.center, style: TextStyle( fontSize: 15, fontWeight: FontWeight.w500)),
                              Container(
                                height: MediaQuery.of(context).size.height * 0.02,

                              ),
                              Material(
                                elevation: 5.0,
                                borderRadius: BorderRadius.circular(30.0),
                                color: Color.fromRGBO(74, 191, 37, 1),
                                child: MaterialButton(

                                  padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                  onPressed: () async {

                                    print('OK');
                                    GlobalVariables.banderaDownload = true;
                                    _download('https://firebasestorage.googleapis.com/v0/b/isae-v1.appspot.com/o/ATA%2Fprueba.pdf?alt=media&token=661b58ab-c0be-4532-9a78-eb5b312ba2b5',
                                        'prueba.pdf');


                                  },
                                  textColor: Colors.white,
                                  child: Row( children: <Widget>[

                                    new Icon(Icons.file_download,color: Colors.white),
                                    Text("Descargar",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12,color: Colors.white)),
                                    new Icon(Icons.arrow_forward_ios,color: Colors.white),
                                  ]),
                                ),
                              ),

                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),

    );
  }

  Widget getInputDate(BuildContext context){
    return  Container(
      padding: EdgeInsets.fromLTRB(8, 1, 1, 1),
      child: SizedBox(
        height: MediaQuery.of(context).size.height * 0.08,
        width: double.infinity,
        child:  new Column(
          children: <Widget>[
            Row ( children: <Widget>[ Icon(Icons.calendar_today,color: Colors.blue),
            Text('Fecha agendada ' + GlobalVariables.fechaCita,textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12,color: Color.fromRGBO(9, 99, 141, 1))),
            ],),
            Container(
              height: MediaQuery
                  .of(context)
                  .size
                  .height * 0.02,

            ),
            Row ( children: <Widget>[ Icon(Icons.timelapse,color: Colors.blue),
              Text('Acudir en el horario de ' + GlobalVariables.value+' Hrs.',textAlign: TextAlign.center,
                  style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12,color: Color.fromRGBO(9, 99, 141, 1))),
            ],),
          ],
        ),

      ),
    );
  }
  Widget getImage(BuildContext context){
    return  Container(
      padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
      child:   new Column(
        children: <Widget>[
          FlatButton(
              child:   Image(image: new AssetImage('assets/images/check2.png'),
                  width: 100, height: 100),
              onPressed: () {


              }
          ),
        ],
      ),
    );
  }
}







