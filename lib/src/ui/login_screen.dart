
import 'package:ATA/src/ui/Frame/InventarioAdd_scren.dart';
import 'package:ATA/src/ui/DocumentoAnexo_scren.dart';
import 'package:ATA/src/ui/Ingresar/Ingresar_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:ATA/src/blocs/ATA/ATA_bloc.dart';
import 'package:ATA/src/blocs/login_bloc.dart';
import 'package:ATA/src/complements/globalStyles.dart';
import 'package:ATA/src/complements/globalVariables.dart';
import 'package:flutter/services.dart';
import 'package:ATA/src/complements/globalWidgets.dart';
import 'package:ATA/src/ui/registro_screen.dart';
import 'package:local_auth/local_auth.dart';
import 'package:ATA/src/database/userData_database.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => LoginScreenState();
}
class LoginScreenState extends State<LoginScreen> {

  //-----------------------Authentication Biometrics----------------------------
  final LocalAuthentication _localAuthentication = LocalAuthentication();

  Future<bool> _isBiometricAvailable() async {
    bool isAvailable = false;
    try {
      isAvailable = await _localAuthentication.canCheckBiometrics;
    } on PlatformException catch (e) {
      print(e);
    }

    if (!mounted) return isAvailable;

    isAvailable
        ? print('Biometric is available!')
        : print('Biometric is unavailable.');

    return isAvailable;
  }

  Future<void> _getListOfBiometricTypes() async {
    List<BiometricType> listOfBiometrics;
    try {
      listOfBiometrics = await _localAuthentication.getAvailableBiometrics();
    } on PlatformException catch (e) {
      print(e);
    }

    if (!mounted) return;

    print(listOfBiometrics);
  }

  Future<void> _authenticateUser() async {
    bool isAuthenticated = false;
    try {
      isAuthenticated = await _localAuthentication.authenticateWithBiometrics(

        localizedReason:
        "Por favor auntentíquese para continuar",
        useErrorDialogs: true,
        stickyAuth: true,
      );
    } on PlatformException catch (e) {
      print(e);
    }

    if (!mounted) return;

    isAuthenticated
        ? print('User is authenticated!')
        : print('User is not authenticated.');

    if (isAuthenticated) {
      if(GlobalVariables.textNEmp.text.length == 0  || GlobalVariables.textPass.text.length == 0){
        print('Sin login');
      } else {
        print('login correcto');
        //show IMC:
        LoginBloc.login(context,1,1);
      }
    }
  }

  Future _idTouch() async {
    //Prueba:
    final dbHelper = UserDataDatabase.instance;
    final countInfo = await dbHelper.queryRowCount();

    if(countInfo != 0) {
      if(await _isBiometricAvailable()){
        await _getListOfBiometricTypes();
        await _authenticateUser();
      }
    } else {
      print('sin datos - no idTouch');
    }

  }

  //----------------------------------------------------------------------------

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Material(
      color: Colors.white,
      child: Column(
        //crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          //------------Tittle------------
          Flexible(
            flex: 1,
            child: Container(
              color:  Colors.white,
              width: MediaQuery.of(context).size.width * 0.90,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 50),
                  titleLogin(),
                  //leyendCorporation(),
                //Container(child: GlobalStyles.titleLogin('Carnet')),
                ],
              ),
            ),
          ),

          Flexible(
          flex: 1,
            child: Container(
              color:  Colors.white,
              child: Column(

                children: <Widget>[
                  Material(
                    elevation: 9.0,
                    //borderRadius: BorderRadius.circular(30.0),
                    color: Color.fromRGBO(207, 227, 233, 1),
                    child: MaterialButton(
                      minWidth: MediaQuery.of(context).size.width / 2,
                      //padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      onPressed: () async {
                        GlobalWidgets.limpiar();
                        GlobalVariables.controllerIngresoFolio.clear();
                        Navigator.pushAndRemoveUntil(
                          context,
                          PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                            return IngresarScreen();
                          }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                            return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                            ).animate(animation), child: child,);
                          },
                              transitionDuration: Duration(seconds: 1)
                          ), (Route route) => false,);


                      },
                      textColor: Color.fromRGBO(62, 95, 138, 1),
                      child:  Row( children: <Widget>[
                        new Icon(Icons.toys,color: Color.fromRGBO(62, 95, 138, 1)),
                        Container(
                          width: MediaQuery.of(context).size.width *0.20,
                        ),
                        Align (
                          alignment: Alignment.topCenter,
                          child:Text("INGRESAR",
                              textAlign: TextAlign.center,
                              style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,color: Color.fromRGBO(62, 95, 138, 1))),),
                        Container(
                          width: MediaQuery.of(context).size.width *0.02,
                        ),

                      ]),
                    ),
                  ),
                  Container(height: MediaQuery.of(context) .size.height * 0.02),
                  Material(
                    elevation: 9.0,
                    //borderRadius: BorderRadius.circular(30.0),
                    color: Color.fromRGBO(207, 227, 233, 1),
                    child: MaterialButton(
                      minWidth: MediaQuery.of(context).size.width / 2,
                      //padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      onPressed: () async {

                        GlobalWidgets.limpiar();
                        Navigator.pushAndRemoveUntil(
                          context,
                          PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                            return DocumentoAnexoScreen();
                          }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                            return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                            ).animate(animation), child: child,);
                          },
                              transitionDuration: Duration(seconds: 1)
                          ), (Route route) => false,);


                      },
                      textColor: Color.fromRGBO(62, 95, 138, 1),
                      child: Row( children: <Widget>[
                        new Icon(Icons.account_circle,color: Color.fromRGBO(62, 95, 138, 1)),
                        Container(
                          width: MediaQuery.of(context).size.width *0.20,
                        ),
                        Align (
                          alignment: Alignment.topCenter,
                          child:Text("REGISTRARSE",
                              textAlign: TextAlign.center,
                              style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,color: Color.fromRGBO(62, 95, 138, 1))),),
                        Container(
                          width: MediaQuery.of(context).size.width *0.02,
                        ),

                      ]),
                    ),
                  ),
                  //loginButton()
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
  Future<Position> locateUser() async {
    return Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }

  getUserLocation() async {
    GlobalVariables.currentLocation = await locateUser();
    setState(() {
      GlobalVariables.center = LatLng(GlobalVariables.currentLocation.latitude, GlobalVariables.currentLocation.longitude);
    });

    print('Ubicacion de la Foto:::: '+ GlobalVariables.center.toString());
  }
}

Widget titleLogin() => StreamBuilder<bool>(
  builder: (context, snap){

    return
        Container(
          alignment: Alignment.bottomCenter,
          child:  ClipRRect(

            //borderRadius: BorderRadius.circular(80.0),
            child: Image.asset(
              'assets/images/app_icon.png',
              width: 100.0,
              height: 100.0,

            ),
          ),


    );
  },
);

Widget leyendCorporation() => StreamBuilder<bool>(
  builder: (context, snap){
        return Row(
          children: <Widget>[

            Container(
          padding: EdgeInsets.fromLTRB(50, 0, 0, 10),
            child: Text('CARNET ATA', maxLines: 2, textAlign: TextAlign.left, style: TextStyle(fontSize: 15,   color: Color.fromRGBO(36, 90, 149, 1))),
        ),
          ],
        );
      },
    );

Widget imageProfile() => StreamBuilder<bool>(
    builder: (context, snap){
      return Image(
        fit: BoxFit.cover,
        image: new AssetImage('assets/images/foto.png'),
        height: 80,
        width: 60,
      );
    },
);

Widget noEmpF() => StreamBuilder<bool>(
    builder: (context, snap){
      return TextField(
        controller: GlobalVariables.textNEmp,
//        keyboardType: TextInputType.number,
        autocorrect: false,
        textAlign: TextAlign.center,
        cursorColor: Colors.blue,
        //maxLength: 8,
        decoration: GlobalStyles.decorationTFLogin('Usuario'),
      );
    },
);

Widget passwordF() => StreamBuilder<bool>(
  builder: (context, snap){
    return TextField(
      controller: GlobalVariables.textPass,
      autocorrect: false,
      textAlign: TextAlign.center,
      obscureText: true,
      cursorColor:  Colors.blue,
      decoration: GlobalStyles.decorationTFLogin('Contraseña'),
    );
  },
);

Widget loginButton() => StreamBuilder<bool>(
  builder: (context, snap){
    return Material(
      elevation: 5.0,
      //borderRadius: BorderRadius.circular(30.0),
      color:  Colors.blue,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width / 2,
        //padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () async {
          //_addUser();



          LoginBloc.login(context,1,1);
        },
        textColor: Colors.blue,
        child: Text("INGRESAR",
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
    );
  },
);
