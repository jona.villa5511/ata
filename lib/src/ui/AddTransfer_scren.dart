

import 'package:ATA/src/ui/ConfirmCardTransfer_scren.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:ATA/src/complements/globalWidgets.dart';
import 'package:ATA/src/complements/globalVariables.dart';
import 'package:ATA/src/services/APiWebServices/Inventario_wsConsults.dart';
import 'package:ATA/src/ui/AgendaCitas_scren.dart';

import 'AgendaCitas_scren.dart';
import 'ConfirmTransfer_scren.dart';
class AddTransferScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => AddTransferScreenState();

}

class AddTransferScreenState extends State<AddTransferScreen> {
  String _value;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:
      GlobalWidgets.topBar('TRANSFERENCIA ', 'Marco', context,''),
      backgroundColor: Color.fromRGBO(207, 227, 233, 1),
      body: Material(
        child: Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    child: Column(
                      children: <Widget>[

                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,
                        ),
                        FlatButton(
                          child:
                          Column( // Replace with a Row for horizontal icon + text
                            children: <Widget>[
                              Image(image: new AssetImage('assets/images/previous.png'),
                                  width: 25, height: 25),
                              Text("Regresar",
                                textAlign: TextAlign.center,
                                style:TextStyle(fontWeight: FontWeight.bold,fontSize: 10,color: Color.fromRGBO(9, 99, 141, 1)),
                              )
                            ],
                          ),
                          onPressed: (){
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => AgendaCitasScreen()),
                            );
                          },
                        ),
                        Container(
                          child:  Text('Transferencia electronica', maxLines: 1,  textAlign: TextAlign.center,
                            style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,color: Color.fromRGBO(9, 99, 141, 1))
                          ),
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.02,

                        ),
                        FlatButton(
                          child: Image(image: new AssetImage('assets/images/tranfer2.jpeg'),
                              width: 250, height: 250),
                          onPressed: (){


                          },
                        ),


                        getInputNumeric(),
                        Container(
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.05,
                        ),
                        Material(
                          elevation: 3.0,
                          borderRadius: BorderRadius.circular(30.0),
                          color: Color.fromRGBO(36, 90, 149, 1),
                          child: MaterialButton(

                            //padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            onPressed: () async {

                              print('OK');
                              if(_formKey.currentState.validate()){

                                print('correcto:');

                                Navigator.pushAndRemoveUntil(
                                  context,
                                  PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                                    return ConfirmTransferScreen();
                                  }, transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                                    return SlideTransition(position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero,
                                    ).animate(animation), child: child,);
                                  },
                                      transitionDuration: Duration(seconds: 1)
                                  ), (Route route) => false,);


                              }else {

                                showDialog(context: context, builder: (context) => ShowMessageValidateAdd('Existen campo vacios ó el formato es incorrecto!','Favor de validar.'));
                              }
                            },
                            textColor: Colors.white,
                            child: Row( children: <Widget>[

                              new Icon(Icons.credit_card,color: Colors.white),
                              Text("Guardar",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12,color: Colors.white)),
                              new Icon(Icons.arrow_forward_ios,color: Colors.white),
                            ]),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );

  }
   Widget getInputNumeric(){
    return  Container(
      padding: EdgeInsets.fromLTRB(8, 1, 1, 1),
        child: Form(
        key: _formKey,
      child: SizedBox(

        width: double.infinity,
        child: Column(
          children: <Widget>[

            Align (
              alignment: Alignment.topLeft,
              child: Text('Banco: ', maxLines: 1,  textAlign: TextAlign.left,
                  style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,color: Color.fromRGBO(9, 99, 141, 1))
              ),
            ),
            selectBank(),

            Align (
              alignment: Alignment.topLeft,
              child: Text('Nombre del beneficiario: ', maxLines: 1,  textAlign: TextAlign.left,
                  style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,color: Color.fromRGBO(9, 99, 141, 1))
              ),
            ),
            TextFormField(
              style: TextStyle(
                color: Colors.black,
              ),
              decoration: new InputDecoration(
                fillColor: Colors.white,
                border: new OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(6.0),
                  borderSide: new BorderSide(),
                ),
              ),
              controller:  GlobalVariables.controllerNombreBeneficiario,
              autovalidate: true,
              validator: (value){

                RegExp regex = new RegExp(r'^[a-zA-Z ]*$');
                if (!regex.hasMatch(value))
                  return 'Ingrese solo texto';
                else
                  return null;

              },
            ),


            Align (
              alignment: Alignment.topLeft,
              child: Text('No. de sucursal: ', maxLines: 1,  textAlign: TextAlign.left,
                  style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,color: Color.fromRGBO(9, 99, 141, 1))
              ),
            ),
             Align (
            alignment: Alignment.topCenter,
            child: Text('0224', maxLines: 1,  textAlign: TextAlign.left,
                style: TextStyle(fontSize: 18,color: Colors.black)
            ),
            ),

            Align (
              alignment: Alignment.topLeft,
              child: Text('Numero de cuenta:', maxLines: 1,  textAlign: TextAlign.left,
                  style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,color: Color.fromRGBO(9, 99, 141, 1))
              ),
            ),
            TextFormField(
              keyboardType: TextInputType.number,
              textCapitalization: TextCapitalization.characters,
              style: TextStyle(
                color: Colors.black,
              ),
              decoration: new InputDecoration(
                fillColor: Colors.white,
                border: new OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(6.0),
                  borderSide: new BorderSide(),
                ),
              ),
              controller:  GlobalVariables.controllerNumerodeCuenta,
              autovalidate: true,
              maxLength:20,
              validator: (value){

                RegExp regex = new RegExp(r'^\d{20}');
                if (!regex.hasMatch(value))
                  return 'Ingrese los 20 numeros de su cuenta';
                else
                  return null;

              },
            ),

            Align (
              alignment: Alignment.topLeft,
              child: Text('CLABE interbancaria:', maxLines: 1,  textAlign: TextAlign.left,
                  style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,color: Color.fromRGBO(9, 99, 141, 1))
              ),
            ),
            TextFormField(
              keyboardType: TextInputType.number,
              textCapitalization: TextCapitalization.characters,
              style: TextStyle(
                color: Colors.black,
              ),
              decoration: new InputDecoration(
                fillColor: Colors.white,
                border: new OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(6.0),
                  borderSide: new BorderSide(),
                ),
              ),
              controller:  GlobalVariables.controllerClabeInterbanc,
              autovalidate: true,
              maxLength:18,
              validator: (value){

                RegExp regex = new RegExp(r'^\d{18}');
                if (!regex.hasMatch(value))
                  return 'Ingrese los 18 numeros de la CLABE';
                else
                  return null;

              },
            ),
        ]),
        //),
      ),



        ),

    );
  }
  Widget selectBank() {
    return Container(

      child: Row(
        children: <Widget>[
          Container(
              color: Colors.white24,
              child: Column(
                  children: <Widget>[
                    DropdownButton<String>(
                      items: [
                        DropdownMenuItem<String>(
                          child: Text('BBVA BANCOMER'),
                          value: 'BBVA BANCOMER',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('BANAMEX'),
                          value: 'BANAMEX',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('SANTANDER'),
                          value: 'SANTANDER',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('BAJIO '),
                          value: 'BAJIO',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('INBURSA'),
                          value: 'INBURSA',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('BANORTE'),
                          value: 'BANORTE',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('AZTECA'),
                          value: 'AZTECA',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('BANCOPPEL'),
                          value: 'BANCOPPEL',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('BANSEFI'),
                          value: 'BANSEFI',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('SCOTIABANK'),
                          value: 'SCOTIABANK',
                        ),
                      ],
                      onChanged: (String value) {
                        setState(() {
                          print('Value '+ value);


                          GlobalVariables.valueBank  = value;
                          _value = value;
                        });
                      },
                      hint: Text('Selecciona un banco',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12,color: Colors.black)),
                      value: _value,
                    ),

                  ])
          ),
        ],
      ),
    );

  }
}