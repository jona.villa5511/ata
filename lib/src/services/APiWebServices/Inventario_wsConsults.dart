import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';
import 'package:http/http.dart' as http;
import 'package:ATA/src/blocs/ATA/ATA_bloc.dart';
import 'package:ATA/src/blocs/login_bloc.dart';
import 'package:ATA/src/complements/globalVariables.dart';
import 'package:ATA/src/models/Adress.dart';
import 'package:ATA/src/models/Auxiliar.dart';
import 'package:ATA/src/models/Files.dart';
import 'package:ATA/src/models/Inventario.dart';
import 'package:ATA/src/models/InventarioAgrupado.dart';
import 'package:ATA/src/models/InventarioDetalleAgrupado.dart';
import 'package:ATA/src/models/Proyects.dart';
import 'package:ATA/src/models/user.dart';
import 'package:ATA/src/ui/Frame/FirmarDocumentoAdd_scren.dart';
import 'package:ATA/src/ui/Frame/PdfFinalScreen.dart';
import 'package:path_provider/path_provider.dart';

import '../ApiDefinitions.dart';


Future<http.Response> loginUser (BuildContext context, String api, String user, String pass) async {
  print(api +' usuario ' + user + ' pass  ' +pass);
  GlobalVariables.valideUserLogin = false;
  Map data = {
    'usuario': user,
    'password': pass
  };
  //encode Map to JSON
  var body = json.encode(data);

  var response = await http.post(api,
      headers: {"Content-Type": "application/json"},
      body: body
  );

  print("${response.statusCode}");

  if (response.statusCode < 200 || response.statusCode > 400 || json == null) {
    print('Error WS');
    GlobalVariables.valideUserLogin = false;
    final error = response.statusCode;
    print('Error $error: conexión - service');
    ImcRBloc.laoding(context,false);
    showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $error: conexión - service'));


    throw new Exception("Error while fetching data");
  } else {
    var jsonList = json.decode(response.body) as List;
    var countR = jsonList.toList().length;


    User user = null;
    print(countR);
    for(var index = 0; index < countR; index++){
      user = new  User(
          jsonList[index]['usuarioId'],
          jsonList[index]['nombreCompleto'].toString(),
        jsonList[index]['correo'].toString(),
        jsonList[index]['telefono'].toString(),
        jsonList[index]['cp'].toString(),
        jsonList[index]['ubicacion'].toString(),
        jsonList[index]['perfilId'].toString(),
        jsonList[index]['jefeId'].toString(),
        jsonList[index]['usuario'].toString(),
        jsonList[index]['password'].toString(),
        jsonList[index]['perfilId'],
        jsonList[index]['perfilNombre'].toString()


      );

      //print(index)
    }


    GlobalVariables.user = user;

    LoginBloc.login(context,2,2);

      if (user != null) {

        if (GlobalVariables.user.userInfo == GlobalVariables.textNEmp.text) {
          print("${response.body}");
          print('user base::: ' + user.userInfo);
          print("USUARIO CORRECTO::");
          GlobalVariables.valideUserLogin = true;
          if (GlobalVariables.valideUserLogin) {

              LoginBloc.addUserData();

            ImcRBloc.getProyects(context, 1,'1','PROYECTOS MIGRACIÒN');
          }
        }
      } else {
        ImcRBloc.laoding(context, false);
        Future.delayed(Duration(seconds: 1), () => {
        if(GlobalVariables.valideUserLoginContrasenaNull){

        }else{
          print('usuario incorrecto'),

  showDialog(context: context, builder:
  (context) => ShowMessageValidateAdd('Usuario o contraseña Incorrecta!','Nota: Los clientes no cuentan con acceso'))
        }
  });
      }

    return response;
  }
}
Future<http.Response> loginValidateUser (BuildContext context, String api, String user, String pass, int olvido) async {
  print(api +' usuario updaye ' + user);
  GlobalVariables.valideUpdateUserLogin = false;
  GlobalVariables.valideUserLoginContrasenaNull = false;

  Map data = {
    'usuario': user,
    'password': pass
  };
  //encode Map to JSON
  var body = json.encode(data);

  var response = await http.post(api,
      headers: {"Content-Type": "application/json"},
      body: body
  );

  print("${response.statusCode}");

  if (response.statusCode < 200 || response.statusCode > 400 || json == null) {
    print('Error WS');
    GlobalVariables.valideUserLogin = false;
    final error = response.statusCode;
    print('Error $error: conexión - service');
    ImcRBloc.laoding(context,false);
    showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $error: conexión - service'));


    throw new Exception("Error while fetching data");
  } else {
    var jsonList = json.decode(response.body) as List;
    var countR = jsonList.toList().length;


    User user = null;
    print(countR);
    for(var index = 0; index < countR; index++){
      user = new  User(
          jsonList[index]['usuarioId'],
          jsonList[index]['nombreCompleto'].toString(),
          jsonList[index]['correo'].toString(),
          jsonList[index]['telefono'].toString(),
          jsonList[index]['cp'].toString(),
          jsonList[index]['ubicacion'].toString(),
          jsonList[index]['perfilId'].toString(),
          jsonList[index]['jefeId'].toString(),
          jsonList[index]['usuario'].toString(),
          jsonList[index]['password'].toString(),
          jsonList[index]['perfilId'],
          jsonList[index]['perfilNombre'].toString()



      );

      //print(index)
    }


    GlobalVariables.user = user;
    GlobalVariables.userValide = user;

    if (user != null) {
        if (GlobalVariables.user.userInfo == GlobalVariables.textNEmp.text) {
          print("${response.body}");
          print('user para actualizar::: ' + user.userInfo);

          GlobalVariables.valideUpdateUserLogin = true;
          if(olvido == 1){
            if (GlobalVariables.valideUpdateUserLogin) {
              ImcRBloc.UpdatePassLoginn(context);
            }
          }else{
           if(user.passInfo.contains('null')){
             GlobalVariables.valideUserLoginContrasenaNull = true;

             print('contraseña nula ' );
             ImcRBloc.newUserLoginn(context);
//            showDialog(context: context,
//                builder: (context) =>
//                    ShowMessage('Contraseña nula!'));

            }else{
             GlobalVariables.valideUserLoginContrasenaNull = false;

           }
          }

        }
      } else {
      if(olvido == 1) {
        ImcRBloc.laoding(context, false);
        showDialog(context: context, builder: (context) => ShowMessageValidateAdd('Usuario incorrecto!','Favor de Validar el usuario, debe ingresar el usuario para restaurar la contraseña.'));

      }else{

      }
      }

    return response;
  }
}
Future<http.Response> loginValidateUserReset (BuildContext context, String api, String user, String pass) async {
  print(api +' usuario updaye ' + user);

  Map data = {
    'usuario': user,
    'password': pass
  };
  //encode Map to JSON
  var body = json.encode(data);

  var response = await http.post(api,
      headers: {"Content-Type": "application/json"},
      body: body
  );

  print("${response.statusCode}");

  if (response.statusCode < 200 || response.statusCode > 400 || json == null) {
    print('Error WS');
    GlobalVariables.valideUserLogin = false;
    final error = response.statusCode;
    print('Error $error: conexión - service');
    ImcRBloc.laoding(context,false);
    showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $error: conexión - service'));


    throw new Exception("Error while fetching data");
  } else {
    var jsonList = json.decode(response.body) as List;
    var countR = jsonList.toList().length;


    User user = null;
    print(countR);
    for(var index = 0; index < countR; index++){
      user = new  User(
          jsonList[index]['usuarioId'],
          jsonList[index]['nombreCompleto'].toString(),
          jsonList[index]['correo'].toString(),
          jsonList[index]['telefono'].toString(),
          jsonList[index]['cp'].toString(),
          jsonList[index]['ubicacion'].toString(),
          jsonList[index]['perfilId'].toString(),
          jsonList[index]['jefeId'].toString(),
          jsonList[index]['usuario'].toString(),
          jsonList[index]['password'].toString(),
          jsonList[index]['perfilId'],
          jsonList[index]['perfilNombre'].toString()



      );

      //print(index)
    }



    GlobalVariables.userValideReset = user;

    if (user != null) {

        print("${response.body}");
        print('user para actualizar::: ' + user.nombreCompleto);


        print('Entrando a app cargando Ws -----> Login ' + GlobalVariables.userValideReset.id.toString());
        updateUserReset(context, ApiDefinition.wsLoginUserUpdate + GlobalVariables.userValideReset.id.toString(), GlobalVariables.textPassNewReset.text);





    } else {

        ImcRBloc.laoding(context, false);
        showDialog(context: context, builder: (context) => ShowMessageValidateAdd('Usuario incorrecto!','Favor de Validar el usuario, debe ingresar el usuario para restaurar la contraseña.'));


    }

    return response;
  }
}
Future<http.Response> getProyect (BuildContext context, String api, int opcion) async {
print(api);
  var response = await http.get(api);

  if(response.statusCode < 200 || response.statusCode > 400 || json == null){

    print('Error WS');
    final error = response.statusCode;
    showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $error: conexión - service'));

  } else {
    GlobalVariables.listProyects.clear();
    var jsonList = json.decode(response.body) as List;
    var countR = jsonList.toList().length;

   List<Proyects> listProyects = new List<Proyects>();
    Proyects proyects = null;

 for(var index = 0; index < countR; index++){
    proyects = new  Proyects(
        jsonList[index]['id'],
        jsonList[index]['descripcion'].toString(),
        jsonList[index]['fecha'].toString());
     listProyects.add(proyects);
        //print(index)
    }
    print('Lista WS Proyectos::: '+ listProyects.length.toString());
    GlobalVariables.listProyects = listProyects;
    print('Lista Variable Lista nueva:::: '+  GlobalVariables.listProyects.length.toString());
    print('Carga completa WEB SERVICES PROYECTOS');

    if(opcion==1){
      showDialog(context: context, builder: (context) => ShowMessageValidateAdd('iniciio!',''));
      ImcRBloc.loadFoto(context,1);
    }else if(opcion==2){
      ImcRBloc.loadInventarioAdd(context);
    }else{
      ImcRBloc.loadProyects(context);
    }

    return response;

  }
  
}

Future<http.Response> getInvent (BuildContext context, String api) async {

  var response = await http.get(api);
print(api);
  if(response.statusCode < 200 || response.statusCode > 400 || json == null){

    print('Error WS');
    final error = response.statusCode;
    showDialog(context: context, builder: (context) => AlertDialog(title: Text('Error'), content: Text('Error $error: conexión - service')));

  } else {

    var jsonList = json.decode(response.body) as List;
    var countR = jsonList.toList().length;

    List<Inventario> listInventario = new List<Inventario>();
    Inventario inventario = null;
    print(countR);
    for(var index = 0; index < countR; index++){
      inventario = new  Inventario(
          jsonList[index]['inventarioid'],
          jsonList[index]['folio'].toString(),
          jsonList[index]['campoId'].toString());
      listInventario.add(inventario);
      //print(index)
    }
    GlobalVariables.listIventario.clear();
    //print('Lista WS Iventario::: '+ listInventario.toString());
    GlobalVariables.listIventario = listInventario;
    print('Lista Variable Lista nueva:::: '+  GlobalVariables.listIventario.length.toString());
    print('Carga completa WEB SERVICES INVENTARIOS');


    return response;

  }

}
Future<http.Response> getInventarioBuscar (BuildContext context, String api, String folio) async {

  print(api);
  Map data = {
    'folio': folio
  };
  //encode Map to JSON
  var body = json.encode(data);

  var response = await http.post(api,
      headers: {"Content-Type": "application/json"},
      body: body
  );
  print("${response.statusCode}");

  if (response.statusCode < 200 || response.statusCode >= 400 || json == null) {
    print('Error WS');
    final error = response.statusCode;
    showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $error: conexión - service'));

    throw new Exception("Error while fetching data");
  } else {

    var jsonList = json.decode(response.body) as List;
    var countR = jsonList.toList().length;

    List<Inventario> listInventario = new List<Inventario>();
    Inventario inventario = null;
    print(countR);
    for(var index = 0; index < countR; index++){
      inventario = new  Inventario(
          jsonList[index]['inventarioid'],
          jsonList[index]['folio'].toString(),
          jsonList[index]['campoId'].toString());
      listInventario.add(inventario);
      //print(index)
    }
    GlobalVariables.listIventario.clear();
    //print('Lista WS Iventario::: '+ listInventario.toString());
    GlobalVariables.listIventario = listInventario;




    return response;
  }

}
Future<String> getMaxInvent (BuildContext context, String api) async {
print('Maxinvent ' +api);
  var response = await http.get(api);
  print(api);
  if(response.statusCode < 200 || response.statusCode > 400 || json == null){

    print('Error WS');
    final error = response.statusCode;
    showDialog(context: context, builder: (context) => AlertDialog(title: Text('Error'), content: Text('Error $error: conexión - service')));

  } else {

    print("respuesta Max inventario ${response.body}");
    GlobalVariables.idMaxInventarioByProyect = response.body;
    print('Max variable:::: '+  GlobalVariables.idMaxInventarioByProyect);
    print('Carga completa WEB SERVICES INVENTARIOS');


    return response.body;

  }

}
Future<http.Response> getProyectoBuscar (BuildContext context, String api, String descripcion) async {

  print(api);
  Map data = {
    'descripcion': descripcion
  };
  //encode Map to JSON
  var body = json.encode(data);

  var response = await http.post(api,
      headers: {"Content-Type": "application/json"},
      body: body
  );
  print("${response.statusCode}");

  if (response.statusCode < 200 || response.statusCode >= 400 || json == null) {
    print('Error WS');
    final error = response.statusCode;
    showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $error: conexión - service'));

    throw new Exception("Error while fetching data");
  } else {

    var jsonList = json.decode(response.body) as List;
    var countR = jsonList.toList().length;

    List<Proyects> listProyects = new List<Proyects>();
    Proyects proyect = null;
    print(countR);
    for(var index = 0; index < countR; index++){
      proyect = new  Proyects(
          jsonList[index]['id'],
          jsonList[index]['descripcion'].toString(),
          jsonList[index]['fecha']);
      listProyects.add(proyect);
      //print(index)
    }

    GlobalVariables.listProyects.clear();
    //print('Lista WS Iventario::: '+ listInventario.toString());
    GlobalVariables.listProyects = listProyects;




    return response;
  }

}

Future<http.Response> getInventarioAgrupado (BuildContext context, String api, String date, String nEmploye) async {

  var response = await http.get(api);
  print(api);
  if(response.statusCode < 200 || response.statusCode > 400 || json == null){

    print('Error WS');
    final error = response.statusCode;
    showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $error: conexión - service'));

  } else {

    var jsonList = json.decode(response.body) as List;
    var countR = jsonList.toList().length;

    List<InventarioAgrupado> listAgrupado = new List<InventarioAgrupado>();
    InventarioAgrupado agrupados = null;

    for(var index = 0; index < countR; index++){
      agrupados = new  InventarioAgrupado(
          jsonList[index]['idAgrup'],
          jsonList[index]['campo'].toString());
      listAgrupado.add(agrupados);
      //print(index)
    }
    print('Lista WS Proyectos::: '+ listAgrupado.length.toString());
    GlobalVariables.listAgrupado = listAgrupado;
    print('Lista Variable Lista nueva:::: '+  GlobalVariables.listAgrupado.length.toString());
    print('Carga completa WEB SERVICES PROYECTOS');

   // ImcRBloc.loadInfoTableR(context,1);
    return response;

  }

}

Future<http.Response> getInventarioDetalleAgrupado (BuildContext context, String api) async {

  var response = await http.get(api);
  print(api);
  if(response.statusCode < 200 || response.statusCode > 400 || json == null){

    print('Error WS');
    final error = response.statusCode;
    showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $error: conexión - service'));

  } else {

    var jsonList = json.decode(response.body) as List;
    var countR = jsonList.toList().length;

    List<InventarioDetalleAgrupado> listDetalleAgrupado = new List<InventarioDetalleAgrupado>();
    InventarioDetalleAgrupado detalleAgrupado = null;
    print(countR);
    for(var index = 0; index < countR; index++){
      detalleAgrupado = new  InventarioDetalleAgrupado(
          jsonList[index]['enApp'],
          jsonList[index]['campoNombre'].toString(),
          jsonList[index]['campoValor'].toString(),
          jsonList[index]['tipoCampo'].toString(),
          jsonList[index]['campo'].toString(),
          jsonList[index]['patter'].toString(),
          jsonList[index]['mensaje'].toString()

  );
      listDetalleAgrupado.add(detalleAgrupado);
      //print(index)
    }
    GlobalVariables.listDetalleAgrupado.clear();
    //print('Lista WS Iventario::: '+ listDetalleAgrupado.toString());
    GlobalVariables.listDetalleAgrupado = listDetalleAgrupado;
    print('Lista Variable Lista nueva:::: '+  GlobalVariables.listDetalleAgrupado.length.toString());
    print('Carga completa WEB SERVICES INVENTARIOS');


    return response;

  }

}

Future<http.Response> updateInvent (BuildContext context, String api, String campo, String valor) async {
  print(api);

  Map data = {
    'campoNombre': campo,
    'campoValor': valor
  };
  //encode Map to JSON
  var body = json.encode(data);

  var response = await http.put(api,
      headers: {"Content-Type": "application/json"},
      body: body
  );

  print("${response.statusCode}");

  if (response.statusCode < 200 || response.statusCode > 400 || json == null) {
    print('Error WS');
    final error = response.statusCode;
    showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $error: conexión - service'));


    throw new Exception("Error while fetching data");
  } else {
    print("${response.body}");

    return response;
  }
}

Future<http.Response> updateUser (BuildContext context, String api, String newPass) async {
  print(api);
if(GlobalVariables.textPassNew.text == GlobalVariables.textPassConfir.text) {
  Map data = {
    'password': newPass
  };
  //encode Map to JSON
  var body = json.encode(data);

  var response = await http.put(api,
      headers: {"Content-Type": "application/json"},
      body: body
  );

  print("${response.statusCode}");

  if (response.statusCode < 200 || response.statusCode > 400 || json == null) {
    print('Error WS');
    final error = response.statusCode;
    showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $error: conexión - service'));


    throw new Exception("Error while fetching data");
  } else {
    ImcRBloc.laoding(context, false);
    print("${response.body}");
    ImcRBloc.closeSesion(context);
    showDialog(context: context,
        builder: (context) =>
            ShowMessageSave('Contraseñe se actualizo correctamente!'));
    GlobalVariables.textPassNew.text = '';
    GlobalVariables.textPassConfir.text = '';
    GlobalVariables.userReset.text = '';
    GlobalVariables.codResetPass.text = '';
    GlobalVariables.codResetPassConfir ='';


    return response;
  }
}else{
  ImcRBloc.laoding(context, false);
  ImcRBloc.UpdatePassLoginn(context);
  showDialog(context: context,
      builder: (context) =>
          ShowMessage('La contraseña no coincide!'));
  ImcRBloc.laoding(context, false);
}
}
Future<http.Response> updateUserReset (BuildContext context, String api, String newPass) async {
  print(api);
  if(GlobalVariables.textPassNewReset.text == GlobalVariables.textPassConfirReset.text) {
    Map data = {
      'password': newPass
    };
    //encode Map to JSON
    var body = json.encode(data);

    var response = await http.put(api,
        headers: {"Content-Type": "application/json"},
        body: body
    );

    print("${response.statusCode}");

    if (response.statusCode < 200 || response.statusCode > 400 || json == null) {
      print('Error WS');
      final error = response.statusCode;
      showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $error: conexión - service'));


      throw new Exception("Error while fetching data");
    } else {
      ImcRBloc.laoding(context, false);
      print("${response.body}");
      ImcRBloc.closeSesion(context);
      showDialog(context: context,
          builder: (context) =>
              ShowMessageSave('Contraseñe se actualizo correctamente!'));
      GlobalVariables.textPassNew.text = '';
      GlobalVariables.textPassConfir.text = '';
      GlobalVariables.userReset.text = '';
      GlobalVariables.codResetPass.text = '';
      GlobalVariables.codResetPassConfir ='';
      GlobalVariables.textPassNewReset.text = '';
      GlobalVariables.textPassConfirReset.text = '';

      return response;
    }
  }else{
    ImcRBloc.laoding(context, false);
    ImcRBloc.UpdatePassResetLoginn(context);
    showDialog(context: context,
        builder: (context) =>
            ShowMessage('La contraseña no coincide!'));
    ImcRBloc.laoding(context, false);
  }
}
Future<http.Response> resetPassword(BuildContext context, String api) async {
  print(api);
  print('enviar correo a : ' +  GlobalVariables.userReset.text);

  Map data = {
    'email': GlobalVariables.userReset.text,
    'content': 'Tu codigo de confirmacion es : ' +  GlobalVariables.codResetPassConfir  ,
    'subject': 'Confirmacion de usuario'
  };
  //encode Map to JSON
  var body = json.encode(data);

  var response = await http.post(api,
      headers: {"Content-Type": "application/json"},
      body: body
  );

  print("${response.statusCode}");

  if (response.statusCode < 200 || response.statusCode > 400 || json == null) {
    print('Error WS');

    final error = response.statusCode;
    print('Error $error: conexión - service');
    ImcRBloc.laoding(context,false);
    showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $error: conexión - service'));


    throw new Exception("Error while fetching data");
  } else {

    print(response.body);

    GlobalVariables.codResetPass.text ='';

    return response;
  }
}

Future<http.Response> getCatalogosAdd (BuildContext context, String api, int id) async {

  var response = await http.get(api);
  print(api);
  if(response.statusCode < 200 || response.statusCode > 400 || json == null){

    print('Error WS');
    final error = response.statusCode;
    showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $error: conexión - service'));

  } else {

    var jsonList = json.decode(response.body) as List;
    var countR = jsonList.toList().length;

    List<Auxiliar> listCatalogos = new List<Auxiliar>();
    Auxiliar catalogos = null;

    for(var index = 0; index < countR; index++){
      catalogos = new  Auxiliar(
          jsonList[index]['id'],
          jsonList[index]['descripcion'].toString());
      listCatalogos.add(catalogos);
      //print(index)
    }

    switch(id) {
      case 1: {
        GlobalVariables.listCatalogos1 = listCatalogos;
      }
      break;
      case 2: {
        GlobalVariables.listCatalogos2 = listCatalogos;
      }
      break;
      case 3: {
        GlobalVariables.listCatalogos3 = listCatalogos;
      }
      break;
      case 4: {

      }
      break;
      case 5: {
        GlobalVariables.listCatalogos5 = listCatalogos;
      }
      break;
      case 6: {
        GlobalVariables.listCatalogos6 = listCatalogos;
      }
      break;
      case 7: {
        GlobalVariables.listCatalogos7 = listCatalogos;
      }
      break;
      case 8: {
        print('Lista WS Proyectos::: '+ GlobalVariables.listCatalogos8.length.toString());

        GlobalVariables.listCatalogos8 = listCatalogos;
      }
      break;
      case 9: {
        GlobalVariables.listCatalogos9 = listCatalogos;
      }
      break;
      case 10: {
        GlobalVariables.listCatalogos10 = listCatalogos;
      }
      break;
      case 11: {
        GlobalVariables.listCatalogos11 = listCatalogos;
      }
      break;
      case 12: {
        GlobalVariables.listCatalogos12 = listCatalogos;
      }
      break;
      case 13: {
        GlobalVariables.listCatalogos13 = listCatalogos;
      }
      break;
      case 14: {
        GlobalVariables.listCatalogos14 = listCatalogos;
      }
      break;
      case 15: {
        GlobalVariables.listCatalogos15 = listCatalogos;
      }
      break;
      case 16: {
        GlobalVariables.listCatalogos16 = listCatalogos;
      }
      break;
      case 17: {
        GlobalVariables.listCatalogos17 = listCatalogos;
      }
      break;
      case 18: {
        GlobalVariables.listCatalogos18 = listCatalogos;
      }
      break;
      case 19: {
        GlobalVariables.listCatalogos19 = listCatalogos;
      }
      break;
      case 20: {
        GlobalVariables.listCatalogos20 = listCatalogos;
      }
      break;
      case 21: {
        GlobalVariables.listCatalogos21 = listCatalogos;
      }
      break;
      case 22: {
        GlobalVariables.listCatalogos22 = listCatalogos;
      }
      break;
      case 23: {
        GlobalVariables.listCatalogos23 = listCatalogos;
      }
      break;
      case 24: {
        GlobalVariables.listCatalogos24 = listCatalogos;
      }
      break;
      case 25: {
        GlobalVariables.listCatalogos25 = listCatalogos;
      }
      break;
      case 26: {
        GlobalVariables.listCatalogos26 = listCatalogos;
      }
      break;
      case 27: {
        GlobalVariables.listCatalogos27 = listCatalogos;
      }
      break;
      case 28: {
        GlobalVariables.listCatalogos28 = listCatalogos;
      }
      break;
      case 29: {
        GlobalVariables.listCatalogos29 = listCatalogos;
      }
      break;

      default: {
        //statements;
      }
      break;
    }






    print('Carga completa CATLOGOS');

    // ImcRBloc.loadInfoTableR(context,1);
    return response;

  }

}

Future<http.Response> insertInvent (BuildContext context, String api) async {
try{
  bool bandera = false;
  print(api);
  print('nuevo Folio: ' + GlobalVariables.nameProyectFile == '' ? null : 'Folio - ');
  String folio = await getMaxInvent(context,ApiDefinition.wsMaxInventByProyect+ GlobalVariables.idProyect.toString());

  Map data = {
    'inventarioid':0,
    'proyectoid':  GlobalVariables.idProyect == null ? 0 : GlobalVariables.idProyect,
    'proyecto':   GlobalVariables.nameProyectFile == '' ? null : GlobalVariables.nameProyectFile,
    'proyectodescripcion':   GlobalVariables.nameProyectFile == '' ? null : GlobalVariables.nameProyectFile,
    'fcreacon': GlobalVariables.controllerFCREACON.text == '' ? null : GlobalVariables.controllerFCREACON.text,
    'folio': GlobalVariables.controllerFOLIO.text == '' ? null : GlobalVariables.controllerFOLIO.text,
    'id':GlobalVariables.controllerID.text== '' ? 0 : GlobalVariables.controllerID.text,
    'aPaterno': GlobalVariables.controllerAPELLIDOS.text == '' ? null : GlobalVariables.controllerAPELLIDOS.text,
    'aMaterno': GlobalVariables.controllerAPELLIDOS2.text == '' ? null : GlobalVariables.controllerAPELLIDOS2.text,
    'nombres':  GlobalVariables.controllerNOMBRES.text == '' ? null : GlobalVariables.controllerNOMBRES.text,
    'nombrecompleto': GlobalVariables.controllerNOMBRES.text == '' ? null : GlobalVariables.controllerNOMBRES.text + ' ' + GlobalVariables.controllerAPELLIDOS.text + ' ' + GlobalVariables.controllerAPELLIDOS2.text,
    'numempleado': GlobalVariables.controllerNUMEMPLEADO.text== '' ? 0 : GlobalVariables.controllerNUMEMPLEADO.text,
    'vip': GlobalVariables.selectedItemCatVIP == null ? null : GlobalVariables.selectedItemCatVIP.descripcion,
    'puesto': GlobalVariables.controllerPUESTO.text == '' ? null : GlobalVariables.controllerPUESTO.text,
    'direccion': GlobalVariables.controllerDIRECCION.text == '' ? null : GlobalVariables.controllerDIRECCION.text,
    'subdireccion': GlobalVariables.controllerSUBDIRECCION.text == '' ? null : GlobalVariables.controllerSUBDIRECCION.text,
    'clavesubdireccion': GlobalVariables.controllerCLAVESUBDIRECCION.text== '' ? 0 :  GlobalVariables.controllerCLAVESUBDIRECCION.text,
    'gerencia': GlobalVariables.controllerGERENCIA.text == '' ? null : GlobalVariables.controllerGERENCIA.text,
    'clavegerencia': GlobalVariables.controllerCLAVEGERENCIA.text== '' ? 0 : GlobalVariables.controllerCLAVEGERENCIA.text,
    'depto': GlobalVariables.controllerDEPTO.text == '' ? null : GlobalVariables.controllerDEPTO.text,
    'clavecentrotrabajo': GlobalVariables.controllerCLAVECENTROTRABAJO.text== '' ? 0 : GlobalVariables.controllerCLAVECENTROTRABAJO.text,
    'correo': GlobalVariables.controllerCORREO.text == '' ? null : GlobalVariables.controllerCORREO.text,
    'telefono': GlobalVariables.controllerTELEFONO.text== '' ? 0 : GlobalVariables.controllerTELEFONO.text,
    'ext': GlobalVariables.controllerEXT.text== '' ? 0 : GlobalVariables.controllerEXT.text,
    'ubicacion': GlobalVariables.controllerUBICACION.text == '' ? null :GlobalVariables.controllerUBICACION.text,
    'cp': GlobalVariables.controllerCP.text== '' ? 0 : GlobalVariables.controllerCP.text,
    'estado':GlobalVariables.controllerESTADO.text == '' ? '' : GlobalVariables.controllerESTADO.text,
    'colonia': GlobalVariables.controllerCOLONIA.text == '' ? '' : GlobalVariables.controllerCOLONIA.text,
    'ubicacioncompleta':GlobalVariables.controllerESTADO.text == '' ? null : GlobalVariables.controllerCOLONIA.text + ' ' + GlobalVariables.controllerESTADO.text +' ' +GlobalVariables.controllerCP.text,
    'zona': GlobalVariables.controllerZONA.text == '' ? null : GlobalVariables.controllerZONA.text,
    'localidad': GlobalVariables.controllerLOCALIDAD.text == '' ? '' : GlobalVariables.controllerLOCALIDAD.text,
    'edificio': GlobalVariables.controllerEDIFICIO.text == '' ? null : GlobalVariables.controllerEDIFICIO.text,
    'piso': GlobalVariables.selectedItemCatPISO == null ? null : GlobalVariables.selectedItemCatPISO.descripcion,
    'area': GlobalVariables.controllerAREA.text == '' ? null : GlobalVariables.controllerAREA.text,
    'adscripcion':  GlobalVariables.controllerADSCRIPCION.text == '' ? null : GlobalVariables.controllerADSCRIPCION.text,
    'apellidosjefe':  GlobalVariables.controllerAPELLIDOSJEFE.text == '' ? null : GlobalVariables.controllerAPELLIDOSJEFE.text,
    'apellidos2jefe':  GlobalVariables.controllerAPELLIDOS2JEFE.text == '' ? null : GlobalVariables.controllerAPELLIDOS2JEFE.text,
    'nombresjefe':  GlobalVariables.controllerNOMBRESJEFE.text == '' ? null : GlobalVariables.controllerNOMBRESJEFE.text ,
    'nombrecompletojefe':  GlobalVariables.controllerNOMBRESJEFE.text == '' ? null : GlobalVariables.controllerAPELLIDOSJEFE.text + ' ' + GlobalVariables.controllerAPELLIDOS2JEFE.text + ' ' + GlobalVariables.controllerNOMBRESJEFE.text,
    'fichajefe':  GlobalVariables.controllerFICHAJEFE.text== '' ? 0 : GlobalVariables.controllerFICHAJEFE.text,
    'extjefe': GlobalVariables.controllerEXTJEFE.text== '' ? 0 : GlobalVariables.controllerEXTJEFE.text,
    'ubicacionjefe': GlobalVariables.controllerUBICACIONJEFE.text == '' ? null : GlobalVariables.controllerUBICACIONJEFE.text,
    'nombrejefeinmediato': GlobalVariables.controllerNOMBREJEFEINMEDIATO.text == '' ? null :GlobalVariables.controllerNOMBREJEFEINMEDIATO.text,
    'apellidosresguardo': GlobalVariables.controllerAPELLIDOSRESGUARDO.text == '' ? null : GlobalVariables.controllerAPELLIDOSRESGUARDO.text,
    'apellidos2resguardo': GlobalVariables.controllerAPELLIDOS2RESGUARDO.text == '' ? null : GlobalVariables.controllerAPELLIDOS2RESGUARDO.text,
    'nombresresguardo': GlobalVariables.controllerNOMBRESRESGUARDO.text == '' ? null : GlobalVariables.controllerNOMBRESRESGUARDO.text,
    'nombrecompletoresguardo': GlobalVariables.controllerNOMBRESRESGUARDO.text == '' ? null : GlobalVariables.controllerAPELLIDOSRESGUARDO.text + ' ' + GlobalVariables.controllerAPELLIDOS2RESGUARDO.text + ' ' + GlobalVariables.controllerNOMBRESRESGUARDO.text,
    'adscripcionresguardo': GlobalVariables.controllerADSCRIPCIONRESGUARDO.text == '' ? null : GlobalVariables.controllerADSCRIPCIONRESGUARDO.text,
    'extresguardo': GlobalVariables.controllerEXTRESGUARDO.text== '' ? 0 : GlobalVariables.controllerEXTRESGUARDO.text,
    'apellidosresponsable': GlobalVariables.controllerAPELLIDOSRESPONSABLE.text == '' ? null : GlobalVariables.controllerAPELLIDOSRESPONSABLE.text,
    'apellidos2responsable': GlobalVariables.controllerAPELLIDOS2RESPONSABLE.text == '' ? null : GlobalVariables.controllerAPELLIDOS2RESPONSABLE.text,
    'nombresresponsable': GlobalVariables.controllerNOMBRESRESPONSABLE.text == '' ? null : GlobalVariables.controllerNOMBRESRESPONSABLE.text,
    'nombrecompletoresponsable': GlobalVariables.controllerNOMBRESRESPONSABLE.text == '' ? null : GlobalVariables.controllerAPELLIDOSRESPONSABLE.text + ' ' + GlobalVariables.controllerAPELLIDOS2RESPONSABLE.text + ' ' +GlobalVariables.controllerNOMBRESRESPONSABLE.text,
    'apellidospemex': GlobalVariables.controllerAPELLIDOSPEMEX.text == '' ? null : GlobalVariables.controllerAPELLIDOSPEMEX.text,
    'apellidos2pemex': GlobalVariables.controllerAPELLIDOS2PEMEX.text == '' ? null : GlobalVariables.controllerAPELLIDOS2PEMEX.text,
    'nombrespemex': GlobalVariables.controllerNOMBRESPEMEX.text == '' ? null : GlobalVariables.controllerNOMBRESPEMEX.text,
    'nombrecompletopemex': GlobalVariables.controllerNOMBRESPEMEX.text == '' ? null : GlobalVariables.controllerAPELLIDOSPEMEX.text + ' ' + GlobalVariables.controllerAPELLIDOS2PEMEX.text + ' ' + GlobalVariables.controllerNOMBRESPEMEX.text,
    'tipoequipo': GlobalVariables.selectedItemCatTIPOEQUIPO == null ? null :GlobalVariables.selectedItemCatTIPOEQUIPO.descripcion,
    'equipo': 'EQUIPO',
    'marcaequipo': GlobalVariables.selectedItemCatMARCAEQUIPO == null ? null : GlobalVariables.selectedItemCatMARCAEQUIPO.descripcion,
    'modeloequipo': GlobalVariables.selectedItemCatMODELOEQUIPO == null ? null : GlobalVariables.selectedItemCatMODELOEQUIPO.descripcion ,
    'numserieequipo': GlobalVariables.controllerNUMSERIEEQUIPO.text == '' ? null : GlobalVariables.controllerNUMSERIEEQUIPO.text,
    'equipocompleto': GlobalVariables.controllerNUMSERIEEQUIPO.text == null ? null : GlobalVariables.controllerNUMSERIEEQUIPO.text.length.toString(),
    'monitor': 'MONITOR',
    'marcamonitor': GlobalVariables.selectedItemCatMARCAMONITOR == null ? null : GlobalVariables.selectedItemCatMARCAMONITOR.descripcion,
    'modelomonitor': GlobalVariables.selectedItemCatMODELOMONITOR == null ? null : GlobalVariables.selectedItemCatMODELOMONITOR.descripcion,
    'numseriemonitor': GlobalVariables.controllerNUMSERIEMONITOR.text == '' ? null : GlobalVariables.controllerNUMSERIEMONITOR.text,
    'monitorcompleto': GlobalVariables.controllerNUMSERIEMONITOR.text == null ? null : GlobalVariables.controllerNUMSERIEMONITOR.text.length.toString(),
    'teclado':  'TECLADO',
    'marcateclado': GlobalVariables.selectedItemCatMARCATECLADO == null ? null : GlobalVariables.selectedItemCatMARCATECLADO.descripcion,
    'modeloteclado': GlobalVariables.selectedItemCatMODELOTECLADO == null ? null :GlobalVariables.selectedItemCatMODELOTECLADO.descripcion,
    'numserieteclado': GlobalVariables.controllerNUMSERIETECLADO.text == '' ? null : GlobalVariables.controllerNUMSERIETECLADO.text,
    'tecladocompleto': GlobalVariables.controllerNUMSERIETECLADO.text == '' ? null : GlobalVariables.controllerNUMSERIETECLADO.text.length.toString(),
    'mouse': 'MOUSE',
    'marcamouse':GlobalVariables.selectedItemCatMARCAMOUSE == null ? null : GlobalVariables.selectedItemCatMARCAMOUSE.descripcion,
    'modelomause':GlobalVariables.selectedItemCatMODELOMAUSE == null ? null : GlobalVariables.selectedItemCatMODELOMAUSE.descripcion,
    'numseriemouse': GlobalVariables.controllerNUMSERIEMOUSE.text == '' ? null : GlobalVariables.controllerNUMSERIEMOUSE.text,
    'mousecompleto': GlobalVariables.controllerNUMSERIEMOUSE.text == '' ? null : GlobalVariables.controllerNUMSERIEMOUSE.text.length.toString(),
    'ups': 'UPS',
    'marcaups': GlobalVariables.selectedItemCatMARCAUPS == null ? null : GlobalVariables.selectedItemCatMARCAUPS.descripcion,
    'modeloups': GlobalVariables.selectedItemCatMODELOUPS == null ? null : GlobalVariables.selectedItemCatMODELOUPS.descripcion,
    'numserieups': GlobalVariables.controllerNUMSERIEUPS.text == '' ? null : GlobalVariables.controllerNUMSERIEUPS.text,
    'upscompleto': GlobalVariables.controllerNUMSERIEUPS.text == '' ? null : GlobalVariables.controllerNUMSERIEUPS.text.length.toString(),
    'maletin': 'MALETIN',
    'marcamaletin': GlobalVariables.selectedItemCatMARCAMALETIN == null ? null : GlobalVariables.selectedItemCatMARCAMALETIN.descripcion,
    'modelomaletin': GlobalVariables.selectedItemCatMODELOMALETIN == null ? null : GlobalVariables.selectedItemCatMODELOMALETIN.descripcion,
    'numseriemaletin': GlobalVariables.controllerNUMSERIEMALETIN.text == '' ? null : GlobalVariables.controllerNUMSERIEMALETIN.text,
    'maletincomleto': GlobalVariables.controllerNUMSERIEMALETIN.text == '' ? null : GlobalVariables.controllerNUMSERIEMALETIN.text.length.toString(),
    'candado': 'CANDADO',
    'marcacandado': GlobalVariables.selectedItemCatMARCACANDADO == null ? null : GlobalVariables.selectedItemCatMARCACANDADO.descripcion,
    'modelocandado':GlobalVariables.selectedItemCatMODELOCANDADO == null ? null : GlobalVariables.selectedItemCatMODELOCANDADO.descripcion,
    'numseriecandado':GlobalVariables.controllerNUMSERIECANDADO.text == '' ? null : GlobalVariables.controllerNUMSERIECANDADO.text,
    'candadocompleto': GlobalVariables.controllerNUMSERIECANDADO.text == '' ? null : GlobalVariables.controllerNUMSERIECANDADO.text.length.toString(),
    'bocinas':'BOCINAS',
    'marcabocinas':GlobalVariables.selectedItemCatMARCABOCINAS == null ? null : GlobalVariables.selectedItemCatMARCABOCINAS.descripcion,
    'modelobocinas':GlobalVariables.selectedItemCatMODELOBOCINAS == null ? null : GlobalVariables.selectedItemCatMODELOBOCINAS.descripcion,
    'numseriebocinas':GlobalVariables.controllerNUMSERIEBOCINAS.text == '' ? null :GlobalVariables.controllerNUMSERIEBOCINAS.text,
    'bocinascompleto':GlobalVariables.controllerNUMSERIEBOCINAS.text == '' ? null :GlobalVariables.controllerNUMSERIEBOCINAS.text.length.toString(),
    'camara': 'CAMARA',
    'marcacamara': GlobalVariables.selectedItemCatMARCACAMARA == null ? null : GlobalVariables.selectedItemCatMARCACAMARA.descripcion,
    'modelocamara': GlobalVariables.selectedItemCatMODELOCAMARA == null ? null : GlobalVariables.selectedItemCatMODELOCAMARA.descripcion,
    'numseriecmara':GlobalVariables.controllerNUMSERIECMARA.text == '' ? null : GlobalVariables.controllerNUMSERIECMARA.text,
    'camaracompleto': GlobalVariables.controllerNUMSERIECMARA.text == '' ? null : GlobalVariables.controllerNUMSERIECMARA.text.length.toString(),
    'monitor2': 'MONITOR ADICIONAL',
    'marcamonitor2': GlobalVariables.selectedItemCatMARCAMONITOR2 == null ? null : GlobalVariables.selectedItemCatMARCAMONITOR2.descripcion,
    'modelomonitor2':GlobalVariables.selectedItemCatMODELOMONITOR2 == null ? null : GlobalVariables.selectedItemCatMODELOMONITOR2.descripcion,
    'numseriemonitor2': GlobalVariables.controllerNUMSERIEMONITOR2.text == '' ? null : GlobalVariables.controllerNUMSERIEMONITOR2.text,
    'monitor2completo': GlobalVariables.controllerNUMSERIEMONITOR2.text == '' ? null : GlobalVariables.controllerNUMSERIEMONITOR2.text.length.toString(),
    'accesorio': 'ACCESORIO',
    'marcaaccesorio': GlobalVariables.selectedItemCatMARCAACCESORIO == null ? null : GlobalVariables.selectedItemCatMARCAACCESORIO.descripcion,
    'modeloaccesorio': GlobalVariables.selectedItemCatMODELOACCESORIO == null ? null : GlobalVariables.selectedItemCatMODELOACCESORIO.descripcion,
    'numserieaccesorio': GlobalVariables.controllerNUMSERIEACCESORIO.text == '' ? null : GlobalVariables.controllerNUMSERIEACCESORIO.text,
    'accesoriocompleto': GlobalVariables.controllerNUMSERIEACCESORIO.text == '' ? null : GlobalVariables.controllerNUMSERIEACCESORIO.text.length.toString(),
    'ram': GlobalVariables.selectedItemCatRAM == null ? null : GlobalVariables.selectedItemCatRAM.descripcion,
    'discoduro': GlobalVariables.selectedItemCatDISCODURO == null ? null : GlobalVariables.selectedItemCatDISCODURO.descripcion,
    'procesador': GlobalVariables.controllerPROCESADOR.text == '' ? null : GlobalVariables.controllerPROCESADOR.text,
    'tipoequipocomp1': GlobalVariables.selectedItemCatTIPOEQUIPOCOMP1 == null ? null :  GlobalVariables.selectedItemCatTIPOEQUIPOCOMP1.descripcion,
    'modelocomp1': GlobalVariables.selectedItemCatMODELOCOMP1 == null ? null : GlobalVariables.selectedItemCatMODELOCOMP1.descripcion,
    'numseriecomp1': GlobalVariables.controllerNUMSERIECOMP1.text == '' ? null : GlobalVariables.controllerNUMSERIECOMP1.text,
    'cruceclientecomp1': GlobalVariables.controllerCRUCECLIENTECOMP1.text == '' ? null : GlobalVariables.controllerCRUCECLIENTECOMP1.text,
    'tipoequipocomp2': GlobalVariables.selectedItemCatTIPOEQUIPOCOMP2 == null ? null : GlobalVariables.selectedItemCatTIPOEQUIPOCOMP2.descripcion,
    'modelocomp2': GlobalVariables.selectedItemCatMODELOCOMP2 == null ? null : GlobalVariables.selectedItemCatMODELOCOMP2.descripcion,
    'numseriecomp2':GlobalVariables.controllerNUMSERIECOMP2.text == '' ? null : GlobalVariables.controllerNUMSERIECOMP2.text,
    'cruceclientecomp2': GlobalVariables.controllerCRUCECLIENTECOMP2 == null ? null : GlobalVariables.controllerCRUCECLIENTECOMP2.text,
    'tipoequipocomp3':GlobalVariables.selectedItemCatTIPOEQUIPOCOMP3 == null ? null : GlobalVariables.selectedItemCatTIPOEQUIPOCOMP3.descripcion,
    'modelocomp3':GlobalVariables.selectedItemCatMODELOCOMP3 == null ? null : GlobalVariables.selectedItemCatMODELOCOMP3.descripcion,
    'numseriecomp3':GlobalVariables.controllerNUMSERIECOMP3.text == '' ? null : GlobalVariables.controllerNUMSERIECOMP3.text,
    'cruceclientecomp3': GlobalVariables.controllerCRUCECLIENTECOMP3.text == '' ? null : GlobalVariables.controllerCRUCECLIENTECOMP3.text,
    'tipoequipocomp4': GlobalVariables.selectedItemCatTIPOEQUIPOCOMP4 == null ? null :GlobalVariables.selectedItemCatTIPOEQUIPOCOMP4.descripcion,
    'modelocomp4': GlobalVariables.selectedItemCatMODELOCOMP4 == null ? null : GlobalVariables.selectedItemCatMODELOCOMP4.descripcion,
    'numseriecomp4': GlobalVariables.controllerNUMSERIECOMP4.text == '' ? null : GlobalVariables.controllerNUMSERIECOMP4.text,
    'cruceclientecomp4': GlobalVariables.controllerCRUCECLIENTECOMP4.text == '' ? null : GlobalVariables.controllerCRUCECLIENTECOMP4.text,
    'tipoequipocomp5': GlobalVariables.selectedItemCatTIPOEQUIPOCOMP5 == null ? null : GlobalVariables.selectedItemCatTIPOEQUIPOCOMP5.descripcion,
    'modelocomp5': GlobalVariables.selectedItemCatMODELOCOMP5 == null ? null : GlobalVariables.selectedItemCatMODELOCOMP5.descripcion,
    'numseriecomp5': GlobalVariables.controllerNUMSERIECOMP5.text == '' ? null : GlobalVariables.controllerNUMSERIECOMP5.text,
    'cruceclientecomp5': GlobalVariables.controllerCRUCECLIENTECOMP5.text == '' ? null : GlobalVariables.controllerCRUCECLIENTECOMP5.text,
    'tipoequipocomp6': GlobalVariables.selectedItemCatTIPOEQUIPOCOMP6 == null ? null : GlobalVariables.selectedItemCatTIPOEQUIPOCOMP6.descripcion,
    'modelocomp6': GlobalVariables.selectedItemCatMODELOCOMP6 == null ? null : GlobalVariables.selectedItemCatMODELOCOMP6.descripcion,
    'numseriecomp6': GlobalVariables.controllerNUMSERIECOMP6.text == '' ? null : GlobalVariables.controllerNUMSERIECOMP6.text,
    'cruceclientecomp6': GlobalVariables.controllerCRUCECLIENTECOMP6.text == '' ? null : GlobalVariables.controllerCRUCECLIENTECOMP6.text,
    'tipoequipocomp7': GlobalVariables.selectedItemCatTIPOEQUIPOCOMP7== null ? null : GlobalVariables.selectedItemCatTIPOEQUIPOCOMP7.descripcion,
    'modelocomp7': GlobalVariables.selectedItemCatMODELOCOMP7 == null ? null : GlobalVariables.selectedItemCatMODELOCOMP7.descripcion,
    'numseriecomp7': GlobalVariables.controllerNUMSERIECOMP7.text == '' ? null : GlobalVariables.controllerNUMSERIECOMP7.text,
    'cruceclientecomp7': GlobalVariables.controllerCRUCECLIENTECOMP7.text == '' ? null : GlobalVariables.controllerCRUCECLIENTECOMP7.text,
    'validacioncomp1': GlobalVariables.controllerNUMSERIECOMP1.text == GlobalVariables.controllerCRUCECLIENTECOMP1.text ? 'VERDADERO' : 'FALSO',
    'validacioncomp2':GlobalVariables.controllerNUMSERIECOMP2.text == GlobalVariables.controllerCRUCECLIENTECOMP2.text ? 'VERDADERO' : 'FALSO',
    'validacioncomp3':GlobalVariables.controllerNUMSERIECOMP3.text == GlobalVariables.controllerCRUCECLIENTECOMP3.text ? 'VERDADERO' : 'FALSO',
    'validacioncomp4':GlobalVariables.controllerNUMSERIECOMP4.text == GlobalVariables.controllerCRUCECLIENTECOMP4.text ? 'VERDADERO' : 'FALSO',
    'validacioncomp5':GlobalVariables.controllerNUMSERIECOMP5.text == GlobalVariables.controllerCRUCECLIENTECOMP5.text ? 'VERDADERO' : 'FALSO',
    'validacioncomp6':GlobalVariables.controllerNUMSERIECOMP6.text == GlobalVariables.controllerCRUCECLIENTECOMP6.text ? 'VERDADERO' : 'FALSO',
    'validacioncomp7':GlobalVariables.controllerNUMSERIECOMP7.text == GlobalVariables.controllerCRUCECLIENTECOMP7.text ? 'VERDADERO' : 'FALSO',
    'validadoscomp': GlobalVariables.controllerVALIDADOSCOMP.text == '' ? null : GlobalVariables.controllerVALIDADOSCOMP.text,
    'tecniconombre':GlobalVariables.controllerTECNICONOMBRE.text == '' ? null : GlobalVariables.controllerTECNICONOMBRE.text,
    'dia': GlobalVariables.selectedItemCatDIA == null ? null : GlobalVariables.selectedItemCatDIA.descripcion,
    'mes': GlobalVariables.selectedItemCatMES == null ? null : GlobalVariables.selectedItemCatMES.descripcion,
    'anio': GlobalVariables.selectedItemCatANIO == null ? null : GlobalVariables.selectedItemCatANIO.descripcion,
    'reqespecial1': GlobalVariables.controllerREQESPECIAL1.text == '' ? null : GlobalVariables.controllerREQESPECIAL1.text,
    'reqespecial2': GlobalVariables.controllerREQESPECIAL2.text == '' ? null : GlobalVariables.controllerREQESPECIAL2.text,
    'obsinv': GlobalVariables.controllerOBSINV.text == '' ? null : GlobalVariables.controllerOBSINV.text,
    'obsresguardo':GlobalVariables.controllerOBSINV.text == '' ? null : GlobalVariables.controllerOBSINV.text,
    'obsextras1': GlobalVariables.controllerOBSEXTRAS1.text == '' ? null : GlobalVariables.controllerOBSEXTRAS1.text,
    'obsextras2':GlobalVariables.controllerOBSEXTRAS2.text == '' ? null : GlobalVariables.controllerOBSEXTRAS2.text,
    'estatus':'NUEVO',
    'fescalacion':GlobalVariables.controllerFESCALACION.text == '' ? null: GlobalVariables.controllerFESCALACION.text,
    'comentariosescalacion':GlobalVariables.controllerCOMENTARIOSESCALACION.text == '' ? null : GlobalVariables.controllerCOMENTARIOSESCALACION.text,
    'campolibre1':GlobalVariables.controllerCAMPOLIBRE1.text == '' ? null : GlobalVariables.controllerCAMPOLIBRE1.text ,
    'campolibre2':GlobalVariables.controllerCAMPOLIBRE2.text == '' ? null : GlobalVariables.controllerCAMPOLIBRE2.text ,
    'campolibre3':GlobalVariables.controllerCAMPOLIBRE3.text == '' ? null : GlobalVariables.controllerCAMPOLIBRE3.text ,
    'campolibre4':GlobalVariables.controllerCAMPOLIBRE4.text == '' ? null : GlobalVariables.controllerCAMPOLIBRE4.text ,
    'campolibre5':GlobalVariables.controllerCAMPOLIBRE5.text == '' ? null : GlobalVariables.controllerCAMPOLIBRE5.text ,
    'campolibre6':GlobalVariables.controllerCAMPOLIBRE6.text == '' ? null : GlobalVariables.controllerCAMPOLIBRE6.text ,
    'campolibre7':GlobalVariables.controllerCAMPOLIBRE7.text == '' ? null : GlobalVariables.controllerCAMPOLIBRE7.text ,
    'campolibre8':GlobalVariables.controllerCAMPOLIBRE8.text == '' ? null : GlobalVariables.controllerCAMPOLIBRE8.text ,
    'campolibre9':GlobalVariables.controllerCAMPOLIBRE9.text == '' ? null : GlobalVariables.controllerCAMPOLIBRE9.text ,
    'campolibre10':GlobalVariables.controllerCAMPOLIBRE10.text == '' ? null : GlobalVariables.controllerCAMPOLIBRE10.text ,
    'campolibre11':GlobalVariables.controllerCAMPOLIBRE11.text == '' ? null : GlobalVariables.controllerCAMPOLIBRE11.text ,
    'campolibre12':GlobalVariables.controllerCAMPOLIBRE12.text == '' ? null : GlobalVariables.controllerCAMPOLIBRE12.text ,
    'campolibre13':GlobalVariables.controllerCAMPOLIBRE13.text == '' ? null : GlobalVariables.controllerCAMPOLIBRE13.text ,
    'campolibre14':GlobalVariables.controllerCAMPOLIBRE14.text == '' ? null : GlobalVariables.controllerCAMPOLIBRE14.text ,
    'campolibre15':GlobalVariables.controllerCAMPOLIBRE15.text == '' ? null : GlobalVariables.controllerCAMPOLIBRE15.text ,
    'campolibre16':GlobalVariables.controllerCAMPOLIBRE16.text == '' ? null : GlobalVariables.controllerCAMPOLIBRE16.text ,
    'campolibre17':GlobalVariables.controllerCAMPOLIBRE17.text == '' ? null : GlobalVariables.controllerCAMPOLIBRE17.text ,
    'campolibre18':GlobalVariables.controllerCAMPOLIBRE18.text == '' ? null : GlobalVariables.controllerCAMPOLIBRE18.text ,
    'campolibre19':GlobalVariables.controllerCAMPOLIBRE19.text == '' ? null : GlobalVariables.controllerCAMPOLIBRE19.text ,
    'campolibre20':GlobalVariables.controllerCAMPOLIBRE20.text == '' ? null : GlobalVariables.controllerCAMPOLIBRE20.text,
    'campoId':  GlobalVariables.nameProyectFile == '' ? null :  GlobalVariables.nameProyectFile.substring(0,5) +'-'+ GlobalVariables.idProyect.toString(),
    'usuario':  GlobalVariables.user.id.toString()
  };

  var body = json.encode(data);
  var response = await http.post(api,
      headers: {"Content-Type": "application/json"},
      body: body
  );
  bandera = true;
  print("${response.statusCode}");

  if (response.statusCode < 200 || response.statusCode >= 400 || json == null) {
    print('Error WS');
    final error = response.statusCode;
  print(response.headers);
  print(response.request);
    showDialog(context: context, builder: (context) => ShowMessageValidateAdd('Ocurrio un Error al agregar, el formato de es incorrecto Error $error:',''));
  bandera =false;

    throw new Exception("Error while fetching data");
  } else {
    bandera = true;
    print("respuesta  ${response.body}");

    GlobalVariables.idInventarioInsertado = int.parse(response.body);
    print('Id inventario insertado ' + GlobalVariables.idInventarioInsertado.toString());

    ImcRBloc.updateDetalleInvet(
        context, GlobalVariables.idProyect,
        GlobalVariables.idInventarioInsertado,
       'campoId',
        GlobalVariables.nameProyectFile == '' ? null :  GlobalVariables.nameProyectFile.substring(0,5) +'-001-'+ folio);
    print('Se actualizo el Folio -- campoID del inventario ' + GlobalVariables.idInventarioInsertado.toString());
    print('IdMaxInventario por proyecto para Folio '+ folio);



    if(bandera){
      print('Insertado correctamente....');


      if(GlobalVariables.fileNoSerieFotoAddEQUIPO != null){
       String fileName = GlobalVariables.fileNameFotoNumSerieAddEQUIPO + '-' + GlobalVariables.controllerNUMSERIEEQUIPO.text + '.png';
        insertNoSerieAdd(context, fileName, GlobalVariables.fileNoSerieFotoAddEQUIPO);
      }
      if(GlobalVariables.fileNoSerieFotoAddMONITOR != null){
        String fileName = GlobalVariables.fileNameFotoNumSerieAddMONITOR + '-' + GlobalVariables.controllerNUMSERIEMONITOR.text + '.png';
        insertNoSerieAdd(context, fileName, GlobalVariables.fileNoSerieFotoAddMONITOR);
      }
      if(GlobalVariables.fileNoSerieFotoAddTECLADO != null){
        String fileName = GlobalVariables.fileNameFotoNumSerieAddTECLADO+ '-' + GlobalVariables.controllerNUMSERIETECLADO.text + '.png';
        insertNoSerieAdd(context, fileName, GlobalVariables.fileNoSerieFotoAddTECLADO);
      }
      if(GlobalVariables.fileNoSerieFotoAddMOUSE != null){
        String fileName = GlobalVariables.fileNameFotoNumSerieAddMOUSE + '-' + GlobalVariables.controllerNUMSERIEMOUSE.text + '.png';
        insertNoSerieAdd(context, fileName, GlobalVariables.fileNoSerieFotoAddMOUSE);
      }
      if(GlobalVariables.fileNoSerieFotoAddUPS != null){
        String fileName = GlobalVariables.fileNameFotoNumSerieAddUPS + '-' + GlobalVariables.controllerNUMSERIEUPS.text + '.png';
        insertNoSerieAdd(context, fileName, GlobalVariables.fileNoSerieFotoAddUPS);
      }
      if(GlobalVariables.fileNoSerieFotoAddMALETIN != null){
        String fileName = GlobalVariables.fileNameFotoNumSerieAddMALETIN + '-' + GlobalVariables.controllerNUMSERIEMALETIN.text + '.png';
        insertNoSerieAdd(context, fileName, GlobalVariables.fileNoSerieFotoAddMALETIN);
      }
      if(GlobalVariables.fileNoSerieFotoAddCANDADO != null){
        String fileName = GlobalVariables.fileNameFotoNumSerieAddCANDADO + '-' + GlobalVariables.controllerNUMSERIECANDADO.text + '.png';
        insertNoSerieAdd(context, fileName, GlobalVariables.fileNoSerieFotoAddCANDADO);
      }
      if(GlobalVariables.fileNoSerieFotoAddBOCINAS != null){
        String fileName = GlobalVariables.fileNameFotoNumSerieAddBOCINAS + '-' + GlobalVariables.controllerNUMSERIEBOCINAS.text + '.png';
        insertNoSerieAdd(context, fileName, GlobalVariables.fileNoSerieFotoAddBOCINAS);
      }
      if(GlobalVariables.fileNoSerieFotoAddCAMARA != null){
        String fileName = GlobalVariables.fileNameFotoNumSerieAddCAMARA + '-' + GlobalVariables.controllerNUMSERIECMARA.text + '.png';
        insertNoSerieAdd(context, fileName, GlobalVariables.fileNoSerieFotoAddCAMARA);
      }
      if(GlobalVariables.fileNoSerieFotoAddMONITOR2 != null){
        String fileName = GlobalVariables.fileNameFotoNumSerieAddMONITOR2 + '-' + GlobalVariables.controllerNUMSERIEMONITOR2.text + '.png';
        insertNoSerieAdd(context, fileName, GlobalVariables.fileNoSerieFotoAddMONITOR2);
      }
      if(GlobalVariables.fileNoSerieFotoAddACCESORIO != null){
        String fileName = GlobalVariables.fileNameFotoNumSerieAddACCESORIO + '-' + GlobalVariables.controllerNUMSERIEACCESORIO.text + '.png';
        insertNoSerieAdd(context, fileName, GlobalVariables.fileNoSerieFotoAddACCESORIO);
      }
      if(GlobalVariables.fileNoSerieFotoAddCOMP1 != null){
        String fileName = GlobalVariables.fileNameFotoNumSerieAddCOMP1 + '-' + GlobalVariables.controllerNUMSERIECOMP1.text + '.png';
        insertNoSerieAdd(context, fileName, GlobalVariables.fileNoSerieFotoAddCOMP1);
      }
      if(GlobalVariables.fileNoSerieFotoAddCOMP2 != null){
        String fileName = GlobalVariables.fileNameFotoNumSerieAddCOMP2 + '-' + GlobalVariables.controllerNUMSERIECOMP2.text + '.png';
        insertNoSerieAdd(context, fileName, GlobalVariables.fileNoSerieFotoAddCOMP2);
      }
      if(GlobalVariables.fileNoSerieFotoAddCOMP3 != null){
        String fileName = GlobalVariables.fileNameFotoNumSerieAddCOMP3 + '-' + GlobalVariables.controllerNUMSERIECOMP2.text + '.png';
        insertNoSerieAdd(context, fileName, GlobalVariables.fileNoSerieFotoAddCOMP3);
      }
      if(GlobalVariables.fileNoSerieFotoAddCOMP4 != null){
        String fileName = GlobalVariables.fileNameFotoNumSerieAddCOMP4 + '-' + GlobalVariables.controllerNUMSERIECOMP3.text + '.png';
        insertNoSerieAdd(context, fileName, GlobalVariables.fileNoSerieFotoAddCOMP4);
      }
      if(GlobalVariables.fileNoSerieFotoAddCOMP5 != null){
        String fileName = GlobalVariables.fileNameFotoNumSerieAddCOMP5 + '-' + GlobalVariables.controllerNUMSERIECOMP4.text + '.png';
        insertNoSerieAdd(context, fileName, GlobalVariables.fileNoSerieFotoAddCOMP5);
      }
      if(GlobalVariables.fileNoSerieFotoAddCOMP6 != null){
        String fileName = GlobalVariables.fileNameFotoNumSerieAddCOMP6 + '-' + GlobalVariables.controllerNUMSERIECOMP5.text + '.png';
        insertNoSerieAdd(context, fileName, GlobalVariables.fileNoSerieFotoAddCOMP6);
      }
      if(GlobalVariables.fileNoSerieFotoAddCOMP7 != null){
        String fileName = GlobalVariables.fileNameFotoNumSerieAddCOMP7 + '-' + GlobalVariables.controllerNUMSERIECOMP6.text + '.png';
        insertNoSerieAdd(context, fileName, GlobalVariables.fileNoSerieFotoAddCOMP7);
      }
      //            LoginBloc.loadingI(context,true);
      Future.delayed(Duration(seconds: 3), () => {
        //      LoginBloc.loadingI(context,false),
        Navigator.pushReplacement(
            context,
            PageRouteBuilder(pageBuilder: (context, anim1, anim2) => FirmarDocumentoAddScreen(),
              transitionsBuilder: (context, anim1, anim2, child) => Container(child: child),//(context, anim1, anim2, child) => FadeTransition(opacity: anim1, child: child),
              transitionDuration: Duration(seconds: 3),
            )
        ),

      });
      showDialog(context: context, builder: (context) => ShowMessageSave('Inventario guardado correctamente!'));
    }else{
      showDialog(context: context, builder: (context) => ShowMessage('Ocurrio un error al agregar.'));
    }

    return response;
  }
}on Exception catch (exception) {
  //showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $exception: conexión - service'));
  print("throwing new error");
  throw Exception('Servicio no disponible ' + ' Error $exception: conexión - service');
}
}

void insertNoSerieAdd(BuildContext context, String fileName, File temp ) async{
  print("Guardar::::...... ");
  String _uploadedFileURL;


  print(fileName);

  // String nameFile = GlobalVariables.nameProyectFile + '_'+ GlobalVariables.nameInvenFolioFile.toString() + '_Evidencia_' + numeroconsecu.toString();
  print('cargo foto ' + fileName);

  LoginBloc.loadingI(context,true);



  StorageReference storageReference = FirebaseStorage.instance
      .ref()
      .child('Proyectos/${GlobalVariables.idProyect}-${GlobalVariables.nameProyectFile}/${GlobalVariables.idInventarioInsertado}-Inventario/Evidencias/$fileName');
  StorageUploadTask uploadTask = storageReference.putFile(temp);
  await uploadTask.onComplete;
  print('File Uploaded');

  var dowurl = await (await uploadTask.onComplete).ref.getDownloadURL();
  _uploadedFileURL = dowurl.toString();

  print('urllllllllll  ' +_uploadedFileURL);

  Files files = new Files(
      0,
      fileName,
      _uploadedFileURL,
      GlobalVariables.center.toString(),
      GlobalVariables.user.id,
      null,
      GlobalVariables.idInventarioInsertado,
      GlobalVariables.idProyect,
      'jpg'
  );
  print('Files ' + files.toString());

  ImcRBloc.InsertFiles(context, files);
//  Future.delayed(Duration(seconds: 2), () => {
//    ImcRBloc.getFilesIvent(context)
//  });
//  Future.delayed(Duration(seconds: 4), () => {
//    LoginBloc.loadingI(context, false),
//    Navigator.pushReplacement(
//        context,
//        PageRouteBuilder(pageBuilder: (context, anim1, anim2) => InventarioAddScreen(),
//          transitionsBuilder: (context, anim1, anim2, child) => Container(child: child),//(context, anim1, anim2, child) => FadeTransition(opacity: anim1, child: child),
//          transitionDuration: Duration(seconds: 3),
//        )
//    )
//  });
 // showDialog(context: context, builder: (context) => ShowMessageSave('No Serie guardado correctamente!'));
}

Future<http.Response> getWsAdressCP (BuildContext context, String api, String cp) async {
  print(api +' CP ' + cp );

  Map data = {
    'cp': cp
  };
  //encode Map to JSON
  var body = json.encode(data);

  var response = await http.post(api,
      headers: {"Content-Type": "application/json"},
      body: body
  );

  print("${response.statusCode}");

  if (response.statusCode < 200 || response.statusCode > 400 || json == null) {
    print('Error WS');
    GlobalVariables.valideUserLogin = false;
    final error = response.statusCode;
    print('Error $error: conexión - service');
    ImcRBloc.laoding(context,false);
    showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $error: conexión - service'));


    throw new Exception("Error while fetching data");
  } else {
    var jsonList = json.decode(response.body) as List;
    var countR = jsonList.toList().length;


    Adress aux = null;
    print(countR);
    for(var index = 0; index < countR; index++){
      aux = new   Adress(
          jsonList[index]['id'],
          jsonList[index]['descripcion'],
        jsonList[index]['colonia'],
        jsonList[index]['localidad'],
        jsonList[index]['estado'],
        jsonList[index]['cp']

      );


    }

    print(aux);
    GlobalVariables.adressCP = aux;


    return response;
  }
}






Future<http.Response> getWsAdressCatCP (BuildContext context, String api, String parametro, int numCat) async {
  print(api +' parametro ' + parametro );

  Map data = {
    'descripcion': parametro
  };
  //encode Map to JSON
  var body = json.encode(data);

  var response = await http.post(api,
      headers: {"Content-Type": "application/json"},
      body: body
  );

  print("${response.statusCode}");

  if (response.statusCode < 200 || response.statusCode > 400 || json == null) {
    print('Error WS');
    GlobalVariables.valideUserLogin = false;
    final error = response.statusCode;
    print('Error $error: conexión - service');
    ImcRBloc.laoding(context,false);
    showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $error: conexión - service'));


    throw new Exception("Error while fetching data");
  } else {

    var jsonList = json.decode(response.body) as List;
    var countR = jsonList.toList().length;

    List<Auxiliar> listAdress = new List<Auxiliar>();
    Auxiliar aux = null;
    print(countR);
    for(var index = 0; index < countR; index++){
         aux = new   Auxiliar(
          jsonList[index]['id'],
          jsonList[index]['descripcion']
      );
      listAdress.add(aux);
      //print(index)
    }



    switch(numCat) {
      case 4: {

        GlobalVariables.listCatalogos4 = listAdress;
        print('Carga completa WEB Catalogos de direccion');
        print(listAdress.length);
      }
      break;
      case 6: {

        GlobalVariables.listCatalogos6 = listAdress;
        print('Carga completa WEB Catalogos de direccion');
        print(listAdress.length);
      }
      break;
      case 7: {

        GlobalVariables.listCatalogos7 = listAdress;
        print('Carga completa WEB Catalogos de direccion');
        print(listAdress.length);
      }
      break;
      default: {
        //statements;
      }
      break;
    }



    return response;
  }
}

Future<http.Response> getwsMarcaModelo (BuildContext context, String api, String parametro, int numCat, String campo) async {
  print(api +' marca ' + parametro + ' num de catalogo ' + numCat.toString() + ' CAMPO ' + campo);

  Map data = {
    "id": numCat,
    "descripcion": parametro
  };
  //encode Map to JSON
  var body = json.encode(data);

  var response = await http.post(api,
      headers: {"Content-Type": "application/json"},
      body: body
  );

  print("${response.statusCode}");

  if (response.statusCode < 200 || response.statusCode > 400 || json == null) {
    print('Error WS');
    GlobalVariables.valideUserLogin = false;
    final error = response.statusCode;
    print('Error $error: conexión - service');
    ImcRBloc.laoding(context,false);
    showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $error: conexión - service'));


    throw new Exception("Error while fetching data");
  } else {

    var jsonList = json.decode(response.body) as List;
    var countR = jsonList.toList().length;

    List<Auxiliar> listAdress = new List<Auxiliar>();
    Auxiliar aux = null;
    print(countR);
    for(var index = 0; index < countR; index++){
      aux = new   Auxiliar(
          jsonList[index]['id'],
          jsonList[index]['descripcion']
      );
      listAdress.add(aux);
      //print(index)
    }



    switch(campo) {
      case 'EQUIPO': {

        GlobalVariables.listCatMarcaModeloEQUIPO = listAdress;
        print('Carga completa WEB CatMarcaModelo EQUI de direccion');
        print(listAdress.length);
      }
      break;
      case 'MONITOR': {

        GlobalVariables.listCatMarcaModeloMONITOR = listAdress;
        print('Carga completa WEB CatMarcaModelo de direccion');
        print(listAdress.length);
      }
      break;
      case 'TECLADO': {

        GlobalVariables.listCatMarcaModeloTECLADO = listAdress;
        print('Carga completa WEB CatMarcaModelo de direccion');
        print(listAdress.length);
      }
      break;
      case 'MOUSE': {

        GlobalVariables.listCatMarcaModeloMOUSE = listAdress;
        print('Carga completa WEB CatMarcaModelo de direccion');
        print(listAdress.length);
      }
      break;
      case 'UPS': {

        GlobalVariables.listCatMarcaModeloUPS= listAdress;
        print('Carga completa WEB CatMarcaModelo de direccion');
        print(listAdress.length);
      }
      break;
      case 'MALETIN': {

        GlobalVariables.listCatMarcaModeloMALETIN = listAdress;
        print('Carga completa WEB CatMarcaModelo de direccion');
        print(listAdress.length);
      }
      break;
      case 'CANDADO': {

        GlobalVariables.listCatMarcaModeloCANDADO = listAdress;
        print('Carga completa WEB CatMarcaModelo de direccion');
        print(listAdress.length);
      }
      break;
      case 'BOCINAS': {

        GlobalVariables.listCatMarcaModeloBOCINAS = listAdress;
        print('Carga completa WEB CatMarcaModelo de direccion');
        print(listAdress.length);
      }
      break;
      case 'CAMARA': {

        GlobalVariables.listCatMarcaModeloCAMARA = listAdress;
        print('Carga completa WEB CatMarcaModelo de direccion');
        print(listAdress.length);
      }
      break;
      case 'ADICIONAL': {

        GlobalVariables.listCatMarcaModeloMONITORADICIONAL = listAdress;
        print('Carga completa WEB CatMarcaModelo de direccion');
        print(listAdress.length);
      }
      break;
      case 'ACCESORIO': {

        GlobalVariables.listCatMarcaModeloACCERIO = listAdress;
        print('Carga completa WEB CatMarcaModelo de direccion');
        print(listAdress.length);
      }
      break;
      default: {
        //statements;
      }
      break;
    }



    return response;
  }
}



Future<http.Response> getFilesInv (BuildContext context, String api) async {
  print(api +' Idinven Files ' + GlobalVariables.idIventario.toString() + '  '+  GlobalVariables.idProyect.toString() );

  Map data = {
    'idInventario': GlobalVariables.idIventario,
    'idProyecto': GlobalVariables.idProyect
  };
  //encode Map to JSON
  var body = json.encode(data);

  var response = await http.post(api,
      headers: {"Content-Type": "application/json"},
      body: body
  );

  print("${response.statusCode}");

  if (response.statusCode < 200 || response.statusCode > 400 || json == null) {
    print('Error WS');
    GlobalVariables.valideUserLogin = false;
    final error = response.statusCode;
    print('Error $error: conexión - service');
    ImcRBloc.laoding(context,false);
    showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $error: conexión - service'));


    throw new Exception("Error while fetching data");
  } else {
    var jsonList = json.decode(response.body) as List;
    var countR = jsonList.toList().length;

  List<Files> listFiles = new List<Files>();
    Files aux = null;
    print(countR);
    for(var index = 0; index < countR; index++){
      aux = new   Files(
          jsonList[index]['id'],
          jsonList[index]['fileName'],
          jsonList[index]['fileUrl'],
          jsonList[index]['fileCoordenadas'],
          jsonList[index]['idUsuario'],
          jsonList[index]['fecha'],
          jsonList[index]['idInventario'],
           jsonList[index]['idProyecto'],
        jsonList[index]['tipo']


      );
      listFiles.add(aux);

    }

    GlobalVariables.listaFilesInven = listFiles;
    print(GlobalVariables.listaFilesInven.length.toString());


    return response;
  }
}

Future<http.Response> getFilesInvByNoSerie (BuildContext context, String api, String fileName) async {
  print(api +' Idinven Files ' + GlobalVariables.idIventario.toString() + '  '+  GlobalVariables.idProyect.toString() );

  Map data = {
    'idInventario': GlobalVariables.idIventario,
    'idProyecto': GlobalVariables.idProyect,
    'fileName':fileName
  };
  //encode Map to JSON
  var body = json.encode(data);

  var response = await http.post(api,
      headers: {"Content-Type": "application/json"},
      body: body
  );

  print("${response.statusCode}");

  if (response.statusCode < 200 || response.statusCode > 400 || json == null) {
    print('Error WS');
    final error = response.statusCode;
    print('Error $error: conexión - service');
    ImcRBloc.laoding(context,false);
    showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $error: conexión - service'));


    throw new Exception("Error while fetching data");
  } else {
    var jsonList = json.decode(response.body) as List;
    var countR = jsonList.toList().length;

    List<Files> listFiles = new List<Files>();
    Files aux = null;
    print(countR);
    for(var index = 0; index < countR; index++){
      aux = new   Files(
          jsonList[index]['id'],
          jsonList[index]['fileName'],
          jsonList[index]['fileUrl'],
          jsonList[index]['fileCoordenadas'],
          jsonList[index]['idUsuario'],
          jsonList[index]['fecha'],
          jsonList[index]['idInventario'],
          jsonList[index]['idProyecto'],
          jsonList[index]['tipo']


      );
      listFiles.add(aux);

    }

    GlobalVariables.listaFilesInvenByNoSerie = listFiles;
    print(GlobalVariables.listaFilesInvenByNoSerie.length.toString());


    return response;
  }
}
Future<http.Response> getFilesInvValidate(BuildContext context, String api, String fileName) async {
  print(api +' Idinven Files ' + GlobalVariables.idIventario.toString() + '  '+  GlobalVariables.idProyect.toString() );

  Map data = {
    'idInventario': GlobalVariables.idIventario,
    'idProyecto': GlobalVariables.idProyect,
    'fileName':fileName
  };
  //encode Map to JSON
  var body = json.encode(data);

  var response = await http.post(api,
      headers: {"Content-Type": "application/json"},
      body: body
  );

  print("${response.statusCode}");

  if (response.statusCode < 200 || response.statusCode > 400 || json == null) {
    print('Error WS');

    final error = response.statusCode;
    print('Error $error: conexión - service');
    ImcRBloc.laoding(context,false);
    showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $error: conexión - service'));


    throw new Exception("Error while fetching data");
  } else {
    var jsonList = json.decode(response.body) as List;
    var countR = jsonList.toList().length;

    int count = null;
    print(countR);
    count = jsonList[0]['validate'];
    GlobalVariables.countValidateFiles = count;
    print(GlobalVariables.countValidateFiles.toString());


    return response;
  }
}

Future<http.Response> insertFilesInv (BuildContext context, String api, Files files) async {
  print(api);
GlobalVariables.bandera = false;
  Map data = {
    'fileName': files.fileName,
    'fileUrl': files.fileUrl,
    'fileCoordenadas': files.fileCoordenadas,
    'idUsuario': files.idUsuario,
    'fecha': files.fecha,
    'idInventario': files.idInventario,
    'idProyecto': files.idProyecto,
    'tipo': files.tipo
  };
  //encode Map to JSON
  var body = json.encode(data);

  var response = await http.put(api,
      headers: {"Content-Type": "application/json"},
      body: body
  );
  GlobalVariables.bandera = true;
  print("${response.statusCode}");

  if (response.statusCode < 200 || response.statusCode >= 400 || json == null) {
    print('Error WS');
    final error = response.statusCode;
    showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $error: conexión - service'));

    GlobalVariables.bandera = false;
    throw new Exception("Error while fetching data");
          } else {
            print("${response.body}");
            print('Inserto  a la base correctamente');

    return response;
  }
}

Future<http.Response> insertFilesInvOtros(BuildContext context, String api, Files files) async {
  print(api);
  GlobalVariables.bandera = false;
  Map data = {
    'fileName': files.fileName,
    'fileUrl': files.fileUrl,
    'fileCoordenadas': files.fileCoordenadas,
    'idUsuario': files.idUsuario,
    'fecha': files.fecha,
    'idInventario': files.idInventario,
    'idProyecto': files.idProyecto,
    'tipo': files.tipo
  };
  //encode Map to JSON
  var body = json.encode(data);

  var response = await http.put(api,
      headers: {"Content-Type": "application/json"},
      body: body
  );
  GlobalVariables.bandera = true;
  print("${response.statusCode}");

  if (response.statusCode < 200 || response.statusCode >= 400 || json == null) {
    print('Error WS');
    final error = response.statusCode;
    showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $error: conexión - service'));

    GlobalVariables.bandera = false;
    throw new Exception("Error while fetching data");
  } else {
    print("${response.body}");
    print('Inserto  a la base correctamente');

    return response;
  }
}


Future<http.Response> deleteFilesInv (BuildContext context, String api, Files files) async {
  print(api);
  GlobalVariables.banderadelete = false;
  Map data = {
    'id': files.id
  };
  //encode Map to JSON
  var body = json.encode(data);

  var response = await http.put(api,
      headers: {"Content-Type": "application/json"},
      body: body
  );
  GlobalVariables.banderadelete = true;
  print("${response.statusCode}");

  if (response.statusCode < 200 || response.statusCode >= 400 || json == null) {
    print('Error WS');
    final error = response.statusCode;
    showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $error: conexión - service'));

    GlobalVariables.banderadelete = false;
    throw new Exception("Error while fetching data");
  } else {
    print("${response.body}");
    print('eleimino  a la base correctamente');

    return response;
  }
}





//Future<http.Response> getFileFormat (BuildContext context, String api, File file) async {
//  print(api);
//  //final bytes = await Io.File(file.path).readAsBytes();
//  //final bytes = Io.File(file.path).readAsBytesSync();
//  String base64Encode(List<int> bytes) => base64.encode(bytes);
//  Uint8List base64Decode(String source) => base64.decode(source);
//
////  var fil = Io.File(file.path);
////  await fil.writeAsBytes(bytes);
////  fil.writeAsBytesSync(bytes);
//
//  var bytes = GlobalVariables.firma.buffer.asByteData().buffer.asUint8List();
//  //final bytes = Io.File(file.path).readAsBytesSync();
//  //String img64 = base64Encode(bytes);
//
// print(bytes);
// Map data = {
//    'file':  await File(file.path).readAsBytes(),
//    'idinventario': 1
//  };
//
//  //encode Map to JSON
//  var body = json.encode(data);
//
//  var response = await http.post(api,
//      headers: {"Content-Type": "application/json"},
//      body: body
//
//  ).then((response) => response);
//  print(response.headers);
//  print(response.body);
//
//  print('estatus  '+response.statusCode.toString());
////  if (response.statusCode < 200 || response.statusCode > 400 || json == null) {
////    print('Error WS');
////    GlobalVariables.valideUserLogin = false;
////    final error = response.statusCode;
////    print('Error $error: conexión - service');
////    ImcRBloc.laoding(context,false);
////    showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $error: conexión - service'));
////
////
////    throw new Exception("Error while fetching data");
////  } else {
////    response.blob();
//
//    return response;
//  //}
//}

//Future<String> getFileFormat (BuildContext context, String api, File file) async {
//
//  var request = http.MultipartRequest('POST', Uri.parse(api));
//  request.files.add(await http.MultipartFile.fromPath('picture', file.path));
//  var res = await request.send();
//  print(res.statusCode);
//  return res.reasonPhrase;
//
//}
getFileFormat (BuildContext context, String api, File firma1, File firma2, File firma3, File firma4, String nameFile) async {

  StorageReference storageReference;
print('generando pdf Reporte');
try{
    var request = http.MultipartRequest('POST', Uri.parse(api));
    request.files.add(await http.MultipartFile.fromPath('file1', firma1.path));
    request.files.add(await http.MultipartFile.fromPath('file2', firma2.path));
    request.files.add(await http.MultipartFile.fromPath('file3', firma3.path));
    request.files.add(await http.MultipartFile.fromPath('file4', firma4.path));
    if(  GlobalVariables.banderafirmaAdd =='1'){
      print(api + 'id inventario add:::: ' + GlobalVariables.idInventarioInsertado.toString());
      request.fields['idinventario'] = GlobalVariables.idInventarioInsertado.toString();
    }else{
      print(api + 'id inventario editar :::: ' + GlobalVariables.idIventario.toString());
      request.fields['idinventario'] = GlobalVariables.idIventario.toString();
    }

  var response = await request.send().then((response) => response);

print(response.statusCode);
    print("Result: ${response.hashCode}");
  if (response.statusCode == 200) {
    print("!!!!!!!!!!!!Uploaded!!!!!!!!!!!!!");
    print("Result: ${response.headers}");
    final finalResp = await http.Response.fromStream(response);
    //print(finalResp.bodyBytes);

    var dir = await getApplicationDocumentsDirectory();
    File file = File("${dir.path}/$nameFile");
    await file.writeAsBytes(finalResp.bodyBytes);
    print(file.path);

    if(  GlobalVariables.banderafirmaAdd =='1'){
      storageReference = FirebaseStorage.instance
          .ref()
          .child('Proyectos/${GlobalVariables.idProyect}-${GlobalVariables.nameProyectFile}/${GlobalVariables.idInventarioInsertado}-Inventario/Evidencias/$nameFile');

    }else{
      storageReference = FirebaseStorage.instance
          .ref()
          .child('Proyectos/${GlobalVariables.idProyect}-${GlobalVariables.nameProyectFile}/${GlobalVariables.idIventario}-Inventario/Evidencias/$nameFile');

    }
     StorageUploadTask uploadTask = storageReference.putFile(file);
    await uploadTask.onComplete;
    print('***********File Uploaded***********');
    var dowurl = await (await uploadTask.onComplete).ref.getDownloadURL();
    String _uploadedFileURL = dowurl.toString();
    print('urllllllllll  ' + _uploadedFileURL);
    Files files=null;
    if(  GlobalVariables.banderafirmaAdd =='1'){
       files = new Files(
          0,
          nameFile,
          _uploadedFileURL,
          GlobalVariables.center.toString(),
          GlobalVariables.user.id,
          null,
          GlobalVariables.idInventarioInsertado,
          GlobalVariables.idProyect,
          'pdf'
      );
    }else{
       files = new Files(
          0,
          nameFile,
          _uploadedFileURL,
          GlobalVariables.center.toString(),
          GlobalVariables.user.id,
          null,
          GlobalVariables.idIventario,
          GlobalVariables.idProyect,
          'pdf'
      );
    }


    ImcRBloc.InsertFiles(context, files);

    GlobalVariables.urlPathPDF = _uploadedFileURL;
    GlobalVariables.nombrePDF = nameFile;
    getPDF();
    print('ID PROEYECTO '+ GlobalVariables.idProyect.toString());
    print('ID INVENTARIO INSERTADO PARA ESTATUS FRIMADO '+ GlobalVariables.idInventarioInsertado.toString());
    ImcRBloc.updateDetalleInvet(
        context, GlobalVariables.idProyect,
        GlobalVariables.idInventarioInsertado,
        'ESTATUS',
        'VALIDACION QA');
    Future.delayed(Duration(seconds: 3), () =>
    {
      LoginBloc.loadingI(context, false),
      Navigator.pushReplacement(
          context,
          PageRouteBuilder(
            pageBuilder: (context, anim1, anim2) => PdfFinalScreen(),
            transitionsBuilder: (context, anim1, anim2, child) =>
                Container(child: child),
            //(context, anim1, anim2, child) => FadeTransition(opacity: anim1, child: child),
            transitionDuration: Duration(seconds: 3),
          )
      )
    });
  }else{
    LoginBloc.loadingI(context, false);
    Navigator.pop(context);
    showDialog(context: context, builder: (context) => ShowMessage('Error al generar el Reporte.'));
  }


}on Exception catch (exception) {
  showDialog(context: context, builder: (context) => ShowMessage('Servicio no disponible' + 'Error $exception: conexión - service'));
  print("throwing new error");
  throw Exception('Servicio no disponible' + 'Error $exception: conexión - service');
}
}




getPDF() async{
  print('entro getPFD ' +GlobalVariables.urlPathPDF);
  GlobalVariables.doc = await PDFDocument.fromURL(GlobalVariables.urlPathPDF);

}




Widget ShowMessage(String message) =>
    StreamBuilder<bool>(builder: (context, snap) {


      return AlertDialog(
        backgroundColor: Color.fromRGBO(207, 227, 233, 1),
        scrollable: true,
        title: Column(

          children: <Widget>[

          ],
        ),

        content: Container(

          color: Color.fromRGBO(178, 222, 235, 1),
          child: Column(

            children: <Widget>[

              Row(

                children: <Widget>[

                  Text(message, textAlign: TextAlign.center, style: TextStyle( fontSize: 12, fontWeight: FontWeight.bold)),
                  Icon(Icons.report_problem, color: Colors.blue),
                ],
              ),



            ],
          ),
        ),
      );
    });
Widget ShowMessageSave(String message) =>
    StreamBuilder<bool>(builder: (context, snap) {


      return AlertDialog(
        backgroundColor: Color.fromRGBO(207, 227, 233, 1),
        scrollable: true,
        title: Column(

          children: <Widget>[

          ],
        ),

        content: Container(

          padding: EdgeInsets.all(20),
          color: Color.fromRGBO(178, 222, 235, 1),
          child: Column(

            children: <Widget>[

              Row(

                children: <Widget>[

                  Text(message, textAlign: TextAlign.center, style: TextStyle( fontSize: 12, fontWeight: FontWeight.bold)),
                  Icon(Icons.check_circle_outline, color: Colors.blue),
                ],
              ),



            ],
          ),
        ),
      );
    });

Widget ShowMessageValidateAdd(String message, String campo) =>
    StreamBuilder<bool>(builder: (context, snap) {


      return AlertDialog(
        backgroundColor: Color.fromRGBO(207, 227, 233, 1),
        scrollable: true,
        title: new Text('Alerta'),

        content: Container(


          color: Color.fromRGBO(178, 222, 235, 1),
          child: Column(
            children: <Widget>[
                Text(message, style: TextStyle( fontSize: 12, fontWeight: FontWeight.bold)),
              Text(campo, style: TextStyle( fontSize: 12, fontWeight: FontWeight.bold)),
              Icon(Icons.report_problem, color: Colors.blue)
            ],
          ),
        ),
      );
    });
