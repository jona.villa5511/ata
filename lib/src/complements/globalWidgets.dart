
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ATA/src/blocs/login_bloc.dart';
import 'package:ATA/src/models/Auxiliar.dart';
import 'package:ATA/src/services/ApiDefinitions.dart';
import 'package:ATA/src/ui/Bienvenida_scren.dart';
import 'package:ATA/src/ui/login_screen.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:ATA/src/blocs/ATA/ATA_bloc.dart';
import 'package:ATA/src/complements/globalVariables.dart';



class GlobalWidgets{

  static topBar(String title, String nameEmp, BuildContext context, String dateNow) {
    return AppBar(

      backgroundColor: Color.fromRGBO(62, 95, 138, 1),

      title: Row(
        children: <Widget>[
          Container(
            child: Row(
              children: <Widget>[
            Row(
            children: <Widget>[
              new Icon(Icons.arrow_right,color: Colors.white),
            Container(

            alignment: Alignment.center,
            child: Text(title, maxLines: 2, textAlign: TextAlign.center, style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.white)),
          ) ,

              Container(
                width: MediaQuery.of(context).size.width * 0.100,
              ),

                SizedBox(
                width: 40.0,
                height: 40.0,
                child: new RaisedButton(
                  padding: const EdgeInsets.all(2.0),

                  color: Color.fromRGBO(0, 132, 186, 1),
                  child: ClipRRect(

                    borderRadius: BorderRadius.circular(50.0),
                    child:
                    //new Icon(Icons.account_circle,color: Colors.white),
                    Image.asset(
                      'assets/images/app_icon.png',
                      width: 50.0,
                      height: 50.0,
                    ),
                  ),
                  shape: RoundedRectangleBorder(side: BorderSide(
                      color: Color.fromRGBO(0, 132, 186, 1),
                      width: 1,
                      style: BorderStyle.solid
                  ),
                    borderRadius: BorderRadius.circular(60),),
                ),

              ),



            ]),


              ],
            ),
          ),

        ],
      ),
      leading: GlobalWidgets.menuLateral(context)
    );
  }
  static menuHome(BuildContext context, String dateNow,Widget botonRegresar) {
    return Container(
      decoration: BoxDecoration(
          color:  Color.fromRGBO(0, 132, 186, 1),

          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(0.0),
              bottomLeft:Radius.circular(40.0),
              topRight: Radius.circular(0.0),
              bottomRight: Radius.circular(40.0))),

      child:  Row(

        children: <Widget>[
          FlatButton(
            onPressed: () => {

              LoginBloc.loadingI(context,true),

              LoginBloc.loadingI(context,false),
            },
            color: Color.fromRGBO(207, 227, 233, 1),
            padding: EdgeInsets.all(20.0),
            textColor: Colors.white,
            shape: RoundedRectangleBorder(side: BorderSide(
                color: Color.fromRGBO(0, 131, 186, 1),
                width: MediaQuery.of(context).size.width *0.02,
                style: BorderStyle.solid
            ),
              borderRadius: BorderRadius.circular(20),),
            child: Column( // Replace with a Row for horizontal icon + text
              children: <Widget>[
                new Icon(Icons.date_range,color: Color.fromRGBO(9, 99, 141, 1)),
                Text('AGENDAR CITA',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,color: Color.fromRGBO(9, 99, 141, 1)),
                )
              ],
            ),
          ),



          Expanded(
            child: Container(
              padding: EdgeInsets.all(10.0),
              alignment: Alignment.centerRight,
              child:Column( // Replace with a Row for horizontal icon + text
                  children: <Widget>[
                    Row( children: <Widget>[
                      Icon(Icons.dashboard,color: Colors.white),
                      Container(
                        width: MediaQuery.of(context).size.width *0.02,
                      ),
                      Text(dateNow, maxLines: 2, textAlign: TextAlign.left, style: TextStyle(fontSize: 13, color: Colors.white)),

                      botonRegresar

                    ]),


                    Row( children: <Widget>[
              Icon(Icons.account_circle,color: Colors.white),
              Text( 'Alejandro Pineda', maxLines: 3, style: TextStyle(fontSize: 13, color: Colors.white)),

               ]),



                ])
            ),
          ),
        ],
      ),
    );
  }


  static menuLateral(BuildContext context) {
    return PopupMenuButton(
      color: Color.fromRGBO(9, 99, 141, 1),
      padding: EdgeInsets.all(16.0),
      offset: Offset(0, 100),
      /*child: Container(
        width: MediaQuery.of(context).size.width * 0.20,
        height: MediaQuery.of(context).size.height,
        color: Colors.amberAccent,
      ),*/
      itemBuilder: (context) => [
        PopupMenuItem(
          //height: MediaQuery.of(context).size.height,
          child: Column(
            children: <Widget>[

              Container(
                width: MediaQuery.of(context).size.width * 0.42,
                height: MediaQuery.of(context).size.height * 0.50,
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[


                      ListTile(
                        leading: new Icon(Icons.supervisor_account,color: Colors.white),
                        title: Text('Cerrar Sesion', maxLines: 1, style: TextStyle(fontSize: 11, fontWeight: FontWeight.bold, color: Colors.white)),
                        onTap: () {
//                          ImcRBloc.closeSesion(context);


                          Navigator.pushAndRemoveUntil(
                              context,
                              PageRouteBuilder(

                                  pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
                                    return LoginScreen();
                                  },

                                  transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                                    return SlideTransition(
                                      position: Tween<Offset>(
                                        begin: Offset(1.0, 0.0),
                                        end: Offset.zero,
                                      ).animate(animation),
                                      child: child,
                                    );
                                  },

                                  transitionDuration: Duration(seconds: 1)
                              ),

                                  (Route route) => false

                          );
                        },
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.40,
                        height: MediaQuery.of(context).size.height * 0.003,
                        color: Colors.white,
                      ),
                    ],
                  ),
                ),
              ),


            ],
          ),


        ),
      ],
      icon: Icon(Icons.menu, color: Colors.white, size: 40.0),
    );
  }
  static limpiar() async {

  GlobalVariables.value ='';
  GlobalVariables.valueBank ='';
  GlobalVariables.fechaCita ='';
  GlobalVariables.controllerTarjeta.text ='';
  GlobalVariables.controllerTarjetaNombre.text ='';
  GlobalVariables.controllerTarjetaFechaCaducidad.text ='';
  GlobalVariables.controllerTarjetaCVV.text ='';
  GlobalVariables.controllerNombreBeneficiario.text ='';
  GlobalVariables.controllerNumerodeCuenta.text ='';
  GlobalVariables.controllerClabeInterbanc.text ='';
  }

  static loadingIndicator(BuildContext context, bool status) async {

    ProgressDialog pr;
    pr = new ProgressDialog(context,type: ProgressDialogType.Normal, showLogs: false);
    pr.style(
        message: 'Cargando...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );
    if(status == true) {
      await pr.show();
    } else {
      await pr.hide();
    }
  }





static setCampo(String campo, Auxiliar newValue)async{
  campo == 'PROYECTO' ?    GlobalVariables.selectedItemCatPROYECTOadd = newValue :
  campo == 'PROYECTODESCRIPCION' ?    GlobalVariables.selectedItemCatPROYECTODESCRIPCIONadd = newValue :
  campo == 'FCREACON' ?    GlobalVariables.selectedItemCatFCREACONadd = newValue :
  campo == 'FOLIO' ?    GlobalVariables.selectedItemCatFOLIOadd = newValue :
  campo == 'ID' ?    GlobalVariables.selectedItemCatIDadd = newValue :
  campo == 'APATERNO' ?    GlobalVariables.selectedItemCatAPELLIDOSadd = newValue :
  campo == 'AMATERNO' ?    GlobalVariables.selectedItemCatAPELLIDOS2add = newValue :
  campo == 'NOMBRES' ?    GlobalVariables.selectedItemCatNOMBRESadd = newValue :
  campo == 'NOMBRECOMPLETO' ?    GlobalVariables.selectedItemCatNOMBRECOMPLETOadd = newValue :
  campo == 'NUMEMPLEADO' ?    GlobalVariables.selectedItemCatNUMEMPLEADOadd = newValue :
  campo == 'VIP' ?    GlobalVariables.selectedItemCatVIPadd = newValue :
  campo == 'PUESTO' ?    GlobalVariables.selectedItemCatPUESTOadd = newValue :
  campo == 'DIRECCION' ?    GlobalVariables.selectedItemCatDIRECCIONadd = newValue :
  campo == 'SUBDIRECCION' ?    GlobalVariables.selectedItemCatSUBDIRECCIONadd = newValue :
  campo == 'CLAVESUBDIRECCION' ?    GlobalVariables.selectedItemCatCLAVESUBDIRECCIONadd = newValue :
  campo == 'GERENCIA' ?    GlobalVariables.selectedItemCatGERENCIAadd = newValue :
  campo == 'CLAVEGERENCIA' ?    GlobalVariables.selectedItemCatCLAVEGERENCIAadd = newValue :
  campo == 'DEPTO' ?    GlobalVariables.selectedItemCatDEPTOadd = newValue :
  campo == 'CLAVECENTROTRABAJO' ?    GlobalVariables.selectedItemCatCLAVECENTROTRABAJOadd = newValue :
  campo == 'CORREO' ?    GlobalVariables.selectedItemCatCORREOadd = newValue :
  campo == 'TELEFONO' ?    GlobalVariables.selectedItemCatTELEFONOadd = newValue :
  campo == 'EXT' ?    GlobalVariables.selectedItemCatEXTadd = newValue :
  campo == 'UBICACION' ?    GlobalVariables.selectedItemCatUBICACIONadd = newValue :
  campo == 'ESTADO' ?    GlobalVariables.selectedItemCatESTADOadd = newValue :
  campo == 'CP' ?    GlobalVariables.selectedItemCatCPadd = newValue :
  campo == 'COLONIA' ?    GlobalVariables.selectedItemCatCOLONIAadd = newValue :
  campo == 'UBICACIONCOMPLETA' ?    GlobalVariables.selectedItemCatUBICACIONCOMPLETAadd = newValue :
  campo == 'ZONA' ?    GlobalVariables.selectedItemCatZONAadd = newValue :
  campo == 'LOCALIDAD' ?    GlobalVariables.selectedItemCatLOCALIDADadd = newValue :
  campo == 'EDIFICIO' ?    GlobalVariables.selectedItemCatEDIFICIOadd = newValue :
  campo == 'PISO' ?    GlobalVariables.selectedItemCatPISOadd = newValue :
  campo == 'AREA' ?    GlobalVariables.selectedItemCatAREAadd = newValue :
  campo == 'ADSCRIPCION' ?    GlobalVariables.selectedItemCatADSCRIPCIONadd = newValue :
  campo == 'APELLIDOSJEFE' ?    GlobalVariables.selectedItemCatAPELLIDOSJEFEadd = newValue :
  campo == 'NOMBRESJEFE' ?    GlobalVariables.selectedItemCatNOMBRESJEFEadd = newValue :
  campo == 'NOMBRECOMPLETOJEFE' ?    GlobalVariables.selectedItemCatNOMBRECOMPLETOJEFEadd = newValue :
  campo == 'FICHAJEFE' ?    GlobalVariables.selectedItemCatFICHAJEFEadd = newValue :
  campo == 'EXTJEFE' ?    GlobalVariables.selectedItemCatEXTJEFEadd = newValue :
  campo == 'UBICACIONJEFE' ?    GlobalVariables.selectedItemCatUBICACIONJEFEadd = newValue :
  campo == 'NOMBREJEFEINMEDIATO' ?    GlobalVariables.selectedItemCatNOMBREJEFEINMEDIATOadd = newValue :
  campo == 'APELLIDOSRESGUARDO' ?    GlobalVariables.selectedItemCatAPELLIDOSRESGUARDOadd = newValue :
  campo == 'NOMBRESRESGUARDO' ?    GlobalVariables.selectedItemCatNOMBRESRESGUARDOadd = newValue :
  campo == 'NOMBRECOMPLETORESGUARDO' ?    GlobalVariables.selectedItemCatNOMBRECOMPLETORESGUARDOadd = newValue :
  campo == 'ADSCRIPCIONRESGUARDO' ?    GlobalVariables.selectedItemCatADSCRIPCIONRESGUARDOadd = newValue :
  campo == 'EXTRESGUARDO' ?    GlobalVariables.selectedItemCatEXTRESGUARDOadd = newValue :
  campo == 'APELLIDOSRESPONSABLE' ?    GlobalVariables.selectedItemCatAPELLIDOSRESPONSABLEadd = newValue :
  campo == 'NOMBRESRESPONSABLE' ?    GlobalVariables.selectedItemCatNOMBRESRESPONSABLEadd = newValue :
  campo == 'NOMBRECOMPLETORESPONSABLE' ?    GlobalVariables.selectedItemCatNOMBRECOMPLETORESPONSABLEadd = newValue :
  campo == 'APELLIDOSPEMEX' ?    GlobalVariables.selectedItemCatAPELLIDOSPEMEXadd = newValue :
  campo == 'NOMBRESPEMEX' ?    GlobalVariables.selectedItemCatNOMBRESPEMEXadd = newValue :
  campo == 'NOMBRECOMPLETOPEMEX' ?    GlobalVariables.selectedItemCatNOMBRECOMPLETOPEMEXadd = newValue :
  campo == 'TIPOEQUIPO' ?    GlobalVariables.selectedItemCatTIPOEQUIPOadd = newValue :
  campo == 'EQUIPO' ?    GlobalVariables.selectedItemCatEQUIPOadd = newValue :
  campo == 'MARCAEQUIPO' ?    GlobalVariables.selectedItemCatMARCAEQUIPOadd = newValue :
  campo == 'MODELOEQUIPO' ?    GlobalVariables.selectedItemCatMODELOEQUIPOadd = newValue :
  campo == 'NUMSERIEEQUIPO' ?    GlobalVariables.selectedItemCatNUMSERIEEQUIPOadd = newValue :
  campo == 'EQUIPOCOMPLETO' ?    GlobalVariables.selectedItemCatEQUIPOCOMPLETOadd = newValue :
  campo == 'MONITOR' ?    GlobalVariables.selectedItemCatMONITORadd = newValue :
  campo == 'MARCAMONITOR' ?    GlobalVariables.selectedItemCatMARCAMONITORadd = newValue :
  campo == 'MODELOMONITOR' ?    GlobalVariables.selectedItemCatMODELOMONITORadd = newValue :
  campo == 'NUMSERIEMONITOR' ?    GlobalVariables.selectedItemCatNUMSERIEMONITORadd = newValue :
  campo == 'MONITORCOMPLETO' ?    GlobalVariables.selectedItemCatMONITORCOMPLETOadd = newValue :
  campo == 'TECLADO' ?    GlobalVariables.selectedItemCatTECLADOadd = newValue :
  campo == 'MARCATECLADO' ?    GlobalVariables.selectedItemCatMARCATECLADOadd = newValue :
  campo == 'MODELOTECLADO' ?    GlobalVariables.selectedItemCatMODELOTECLADOadd = newValue :
  campo == 'NUMSERIETECLADO' ?    GlobalVariables.selectedItemCatNUMSERIETECLADOadd = newValue :
  campo == 'TECLADOCOMPLETO' ?    GlobalVariables.selectedItemCatTECLADOCOMPLETOadd = newValue :
  campo == 'MOUSE' ?    GlobalVariables.selectedItemCatMOUSEadd = newValue :
  campo == 'MARCAMOUSE' ?    GlobalVariables.selectedItemCatMARCAMOUSEadd = newValue :
  campo == 'MODELOMAUSE' ?    GlobalVariables.selectedItemCatMODELOMAUSEadd = newValue :
  campo == 'NUMSERIEMOUSE' ?    GlobalVariables.selectedItemCatNUMSERIEMOUSEadd = newValue :
  campo == 'MOUSECOMPLETO' ?    GlobalVariables.selectedItemCatMOUSECOMPLETOadd = newValue :
  campo == 'UPS' ?    GlobalVariables.selectedItemCatUPSadd = newValue :
  campo == 'MARCAUPS' ?    GlobalVariables.selectedItemCatMARCAUPSadd = newValue :
  campo == 'MODELOUPS' ?    GlobalVariables.selectedItemCatMODELOUPSadd = newValue :
  campo == 'NUMSERIEUPS' ?    GlobalVariables.selectedItemCatNUMSERIEUPSadd = newValue :
  campo == 'UPSCOMPLETO' ?    GlobalVariables.selectedItemCatUPSCOMPLETOadd = newValue :
  campo == 'MALETIN' ?    GlobalVariables.selectedItemCatMALETINadd = newValue :
  campo == 'MARCAMALETIN' ?    GlobalVariables.selectedItemCatMARCAMALETINadd = newValue :
  campo == 'MODELOMALETIN' ?    GlobalVariables.selectedItemCatMODELOMALETINadd = newValue :
  campo == 'NUMSERIEMALETIN' ?    GlobalVariables.selectedItemCatNUMSERIEMALETINadd = newValue :
  campo == 'MALETINCOMLETO' ?    GlobalVariables.selectedItemCatMALETINCOMLETOadd = newValue :
  campo == 'CANDADO' ?    GlobalVariables.selectedItemCatCANDADOadd = newValue :
  campo == 'MARCACANDADO' ?    GlobalVariables.selectedItemCatMARCACANDADOadd = newValue :
  campo == 'MODELOCANDADO' ?    GlobalVariables.selectedItemCatMODELOCANDADOadd = newValue :
  campo == 'NUMSERIECANDADO' ?    GlobalVariables.selectedItemCatNUMSERIECANDADOadd = newValue :
  campo == 'CANDADOCOMPLETO' ?    GlobalVariables.selectedItemCatCANDADOCOMPLETOadd = newValue :
  campo == 'BOCINAS' ?    GlobalVariables.selectedItemCatBOCINASadd = newValue :
  campo == 'MARCABOCINAS' ?    GlobalVariables.selectedItemCatMARCABOCINASadd = newValue :
  campo == 'MODELOBOCINAS' ?    GlobalVariables.selectedItemCatMODELOBOCINASadd = newValue :
  campo == 'NUMSERIEBOCINAS' ?    GlobalVariables.selectedItemCatNUMSERIEBOCINASadd = newValue :
  campo == 'BOCINASCOMPLETO' ?    GlobalVariables.selectedItemCatBOCINASCOMPLETOadd = newValue :
  campo == 'CAMARA' ?    GlobalVariables.selectedItemCatCAMARAadd = newValue :
  campo == 'MARCACAMARA' ?    GlobalVariables.selectedItemCatMARCACAMARAadd = newValue :
  campo == 'MODELOCAMARA' ?    GlobalVariables.selectedItemCatMODELOCAMARAadd = newValue :
  campo == 'NUMSERIECMARA' ?    GlobalVariables.selectedItemCatNUMSERIECMARAadd = newValue :
  campo == 'CAMARACOMPLETO' ?    GlobalVariables.selectedItemCatCAMARACOMPLETOadd = newValue :
  campo == 'MONITOR2' ?    GlobalVariables.selectedItemCatMONITOR2add = newValue :
  campo == 'MARCAMONITOR2' ?    GlobalVariables.selectedItemCatMARCAMONITOR2add = newValue :
  campo == 'MODELOMONITOR2' ?    GlobalVariables.selectedItemCatMODELOMONITOR2add = newValue :
  campo == 'NUMSERIEMONITOR2' ?    GlobalVariables.selectedItemCatNUMSERIEMONITOR2add = newValue :
  campo == 'MONITOR2COMPLETO' ?    GlobalVariables.selectedItemCatMONITOR2COMPLETOadd = newValue :
  campo == 'ACCESORIO' ?    GlobalVariables.selectedItemCatACCESORIOadd = newValue :
  campo == 'MARCAACCESORIO' ?    GlobalVariables.selectedItemCatMARCAACCESORIOadd = newValue :
  campo == 'MODELOACCESORIO' ?    GlobalVariables.selectedItemCatMODELOACCESORIOadd = newValue :
  campo == 'NUMSERIEACCESORIO' ?    GlobalVariables.selectedItemCatNUMSERIEACCESORIOadd = newValue :
  campo == 'ACCESORIOCOMPLETO' ?    GlobalVariables.selectedItemCatACCESORIOCOMPLETOadd = newValue :
  campo == 'RAM' ?    GlobalVariables.selectedItemCatRAMadd = newValue :
  campo == 'DISCODURO' ?    GlobalVariables.selectedItemCatDISCODUROadd = newValue :
  campo == 'PROCESADOR' ?    GlobalVariables.selectedItemCatPROCESADORadd = newValue :
  campo == 'TIPOEQUIPOCOMP1' ?    GlobalVariables.selectedItemCatTIPOEQUIPOCOMP1add = newValue :
  campo == 'MODELOCOMP1' ?    GlobalVariables.selectedItemCatMODELOCOMP1add = newValue :
  campo == 'NUMSERIECOMP1' ?    GlobalVariables.selectedItemCatNUMSERIECOMP1add = newValue :
  campo == 'CRUCECLIENTECOMP1' ?    GlobalVariables.selectedItemCatCRUCECLIENTECOMP1add = newValue :
  campo == 'TIPOEQUIPOCOMP2' ?    GlobalVariables.selectedItemCatTIPOEQUIPOCOMP2add = newValue :
  campo == 'MODELOCOMP2' ?    GlobalVariables.selectedItemCatMODELOCOMP2add = newValue :
  campo == 'NUMSERIECOMP2' ?    GlobalVariables.selectedItemCatNUMSERIECOMP2add = newValue :
  campo == 'CRUCECLIENTECOMP2' ?    GlobalVariables.selectedItemCatCRUCECLIENTECOMP2add = newValue :
  campo == 'TIPOEQUIPOCOMP3' ?    GlobalVariables.selectedItemCatTIPOEQUIPOCOMP3add = newValue :
  campo == 'MODELOCOMP3' ?    GlobalVariables.selectedItemCatMODELOCOMP3add = newValue :
  campo == 'NUMSERIECOMP3' ?    GlobalVariables.selectedItemCatNUMSERIECOMP3add = newValue :
  campo == 'CRUCECLIENTECOMP3' ?    GlobalVariables.selectedItemCatCRUCECLIENTECOMP3add = newValue :
  campo == 'TIPOEQUIPOCOMP4' ?    GlobalVariables.selectedItemCatTIPOEQUIPOCOMP4add = newValue :
  campo == 'MODELOCOMP4' ?    GlobalVariables.selectedItemCatMODELOCOMP4add = newValue :
  campo == 'NUMSERIECOMP4' ?    GlobalVariables.selectedItemCatNUMSERIECOMP4add = newValue :
  campo == 'CRUCECLIENTECOMP4' ?    GlobalVariables.selectedItemCatCRUCECLIENTECOMP4add = newValue :
  campo == 'TIPOEQUIPOCOMP5' ?    GlobalVariables.selectedItemCatTIPOEQUIPOCOMP5add = newValue :
  campo == 'MODELOCOMP5' ?    GlobalVariables.selectedItemCatMODELOCOMP5add = newValue :
  campo == 'NUMSERIECOMP5' ?    GlobalVariables.selectedItemCatNUMSERIECOMP5add = newValue :
  campo == 'CRUCECLIENTECOMP5' ?    GlobalVariables.selectedItemCatCRUCECLIENTECOMP5add = newValue :
  campo == 'TIPOEQUIPOCOMP6' ?    GlobalVariables.selectedItemCatTIPOEQUIPOCOMP6add = newValue :
  campo == 'MODELOCOMP6' ?    GlobalVariables.selectedItemCatMODELOCOMP6add = newValue :
  campo == 'NUMSERIECOMP6' ?    GlobalVariables.selectedItemCatNUMSERIECOMP6add = newValue :
  campo == 'CRUCECLIENTECOMP6' ?    GlobalVariables.selectedItemCatCRUCECLIENTECOMP6add = newValue :
  campo == 'TIPOEQUIPOCOMP7' ?    GlobalVariables.selectedItemCatTIPOEQUIPOCOMP7add = newValue :
  campo == 'MODELOCOMP7' ?    GlobalVariables.selectedItemCatMODELOCOMP7add = newValue :
  campo == 'NUMSERIECOMP7' ?    GlobalVariables.selectedItemCatNUMSERIECOMP7add = newValue :
  campo == 'CRUCECLIENTECOMP7' ?    GlobalVariables.selectedItemCatCRUCECLIENTECOMP7add = newValue :
  campo == 'VALIDACIONCOMP1' ?    GlobalVariables.selectedItemCatVALIDACIONCOMP1add = newValue :
  campo == 'VALIDACIONCOMP2' ?    GlobalVariables.selectedItemCatVALIDACIONCOMP2add = newValue :
  campo == 'VALIDACIONCOMP3' ?    GlobalVariables.selectedItemCatVALIDACIONCOMP3add = newValue :
  campo == 'VALIDACIONCOMP4' ?    GlobalVariables.selectedItemCatVALIDACIONCOMP4add = newValue :
  campo == 'VALIDACIONCOMP5' ?    GlobalVariables.selectedItemCatVALIDACIONCOMP5add = newValue :
  campo == 'VALIDACIONCOMP6' ?    GlobalVariables.selectedItemCatVALIDACIONCOMP6add = newValue :
  campo == 'VALIDACIONCOMP7' ?    GlobalVariables.selectedItemCatVALIDACIONCOMP7add = newValue :
  campo == 'VALIDADOSCOMP' ?    GlobalVariables.selectedItemCatVALIDADOSCOMPadd = newValue :
  campo == 'TECNICONOMBRE' ?    GlobalVariables.selectedItemCatTECNICONOMBREadd = newValue :
  campo == 'DIA' ?    GlobalVariables.selectedItemCatDIAadd = newValue :
  campo == 'MES' ?    GlobalVariables.selectedItemCatMESadd = newValue :
  campo == 'ANIO' ?    GlobalVariables.selectedItemCatANIOadd = newValue :
  campo == 'REQESPECIAL1' ?    GlobalVariables.selectedItemCatREQESPECIAL1add = newValue :
  campo == 'REQESPECIAL2' ?    GlobalVariables.selectedItemCatREQESPECIAL2add = newValue :
  campo == 'OBSINV' ?    GlobalVariables.selectedItemCatOBSINVadd = newValue :
  campo == 'OBSRESGUARDO' ?    GlobalVariables.selectedItemCatOBSRESGUARDOadd = newValue :
  campo == 'OBSEXTRAS1' ?    GlobalVariables.selectedItemCatOBSEXTRAS1add = newValue :
  campo == 'OBSEXTRAS2' ?    GlobalVariables.selectedItemCatOBSEXTRAS2add = newValue :
  campo == 'ESTATUS' ?    GlobalVariables.selectedItemCatESTATUSadd = newValue :
  campo == 'FESCALACION' ?    GlobalVariables.selectedItemCatFESCALACIONadd = newValue :
  campo == 'COMENTARIOSESCALACION' ?    GlobalVariables.selectedItemCatCOMENTARIOSESCALACIONadd = newValue :
  null;
  //GlobalVariables.valorEdit = GlobalVariables.selectedItemCatPISOadd.descripcion;
}

  static Widget getInputNumSerie(BuildContext context, String campo, String patter, String mensaje) {
    print('campo numero serie:: ' + campo);
    return Container(
      padding: EdgeInsets.fromLTRB(8, 1, 1, 1),
      child: SizedBox(
        height: MediaQuery.of(context).size.height * 0.07,
        width: double.infinity,
        child: TextFormField(
          autocorrect: true,
          showCursor: true,
          textCapitalization: TextCapitalization.characters,
          style: TextStyle(
            color: Colors.black,
          ),
          decoration: new InputDecoration(
            fillColor: Colors.white,
            border: new OutlineInputBorder(
              borderRadius: new BorderRadius.circular(6.0),
              borderSide: new BorderSide(),
            ),
          ),

          controller:
          campo == 'NUMSERIEEQUIPO'?    GlobalVariables.controllerNUMSERIEEQUIPO:
          campo == 'NUMSERIEMONITOR'?    GlobalVariables.controllerNUMSERIEMONITOR:
          campo == 'NUMSERIETECLADO'?    GlobalVariables.controllerNUMSERIETECLADO:
          campo == 'NUMSERIEMOUSE'?    GlobalVariables.controllerNUMSERIEMOUSE:
          campo == 'NUMSERIEUPS'?    GlobalVariables.controllerNUMSERIEUPS:
          campo == 'NUMSERIEMALETIN'?    GlobalVariables.controllerNUMSERIEMALETIN:
          campo == 'NUMSERIECANDADO'?    GlobalVariables.controllerNUMSERIECANDADO:
          campo == 'NUMSERIEBOCINAS'?    GlobalVariables.controllerNUMSERIEBOCINAS:
          campo == 'NUMSERIECMARA'?    GlobalVariables.controllerNUMSERIECMARA:
          campo == 'NUMSERIEMONITOR2'?    GlobalVariables.controllerNUMSERIEMONITOR2:
          campo == 'NUMSERIEACCESORIO'?    GlobalVariables.controllerNUMSERIEACCESORIO:
          campo == 'NUMSERIECOMP1'?    GlobalVariables.controllerNUMSERIECOMP1:
          campo == 'NUMSERIECOMP2'?    GlobalVariables.controllerNUMSERIECOMP2:
          campo == 'NUMSERIECOMP3'?    GlobalVariables.controllerNUMSERIECOMP3:
          campo == 'NUMSERIECOMP4'?    GlobalVariables.controllerNUMSERIECOMP4:
          campo == 'NUMSERIECOMP5'?    GlobalVariables.controllerNUMSERIECOMP5:
          campo == 'NUMSERIECOMP6'?    GlobalVariables.controllerNUMSERIECOMP6:
          campo == 'NUMSERIECOMP7'?    GlobalVariables.controllerNUMSERIECOMP7:
          null,
          autovalidate: true,
          validator: (value) {
            if (campo == 'NUMSERIEEQUIPO') {
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if (campo == 'NUMSERIEMONITOR') {
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if (campo == 'NUMSERIETECLADO') {
              // print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if (campo == 'NUMSERIEMOUSE') {
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if (campo == 'NUMSERIEUPS') {
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if (campo == 'NUMSERIEMALETIN') {
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if (campo == 'NUMSERIECANDADO') {
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if (campo == 'NUMSERIEBOCINAS') {
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if (campo == 'NUMSERIECMARA') {
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if (campo == 'NUMSERIEMONITOR2') {
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if (campo == 'NUMSERIEACCESORIO') {
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if (campo == 'NUMSERIECOMP1') {
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if (campo == 'NUMSERIECOMP2') {
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if (campo == 'NUMSERIECOMP3') {
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if (campo == 'NUMSERIECOMP4') {
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if (campo == 'NUMSERIECOMP5') {
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if (campo == 'NUMSERIECOMP6') {
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if (campo == 'NUMSERIECOMP7') {
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
          }
        ),
        //),
      ),
    );
  }

  static Widget getInputAlfaNumeric(BuildContext context, String campo,
      String patter, String mensaje) {
    return Container(
      padding: EdgeInsets.fromLTRB(8, 1, 1, 1),
      child: SizedBox(
        height: MediaQuery.of(context).size.height * 0.07,
        width: double.infinity,
        //  child: Form(
//          key: campo == 'APATERNO'? _formKeyAPELLIDOS :
//         campo == 'AMATERNO'? _formKeyAPELLIDOS2 :
//          campo == 'NOMBRES'? _formKeyNOMBRES :
//          campo == 'PUESTO'? _formKeyPUESTO :
//          campo == 'CORREO'? _formKeyCORREO :
//          campo == 'EDIFICIO'? _formKeyEDIFICIO:
//          campo == 'AREA'? _formKeyAREA :
//          campo == 'UBICACIONJEFE'? _formKeyUBICACIONJEFE :
//         campo == 'NOMBREJEFEINMEDIATO'? _formKeyNOMBREJEFEINMEDIATO:
//         campo == 'APELLIDOSRESGUARDO'? _formKeyAPELLIDOSRESGUARDO:
//          campo == 'NOMBRESRESGUARDO'? _formKeyNOMBRESRESGUARDO:
//          campo == 'NUMSERIEEQUIPO'? _formKeyNUMSERIEEQUIPO:
//          campo == 'NUMSERIEMONITOR'? _formKeyNUMSERIEMONITOR:
//          campo == 'NUMSERIETECLADO'? _formKeyNUMSERIETECLADO:
//          campo == 'NUMSERIEMOUSE'? _formKeyNUMSERIEMOUSE:
//          campo == 'NUMSERIEUPS'? _formKeyNUMSERIEUPS:
//          campo == 'NUMSERIEMALETIN'? _formKeyNUMSERIEMALETIN:
//          campo == 'NUMSERIEMONITOR2'? _formKeyNUMSERIEMONITOR2:
//          campo == 'NUMSERIEACCESORIO'? _formKeyNUMSERIEACCESORIO:
//          campo == 'NUMSERIECOMP1'? _formKeyNUMSERIECOMP1:
//          campo == 'CRUCECLIENTECOMP1'? _formKeyCRUCECLIENTECOMP1:
//          campo == 'NUMSERIECOMP2'? _formKeyNUMSERIECOMP2:
//          campo == 'CRUCECLIENTECOMP2'? _formKeyCRUCECLIENTECOMP2:
//          campo == 'NUMSERIECOMP3'? _formKeyNUMSERIECOMP3:
//          campo == 'CRUCECLIENTECOMP3'? _formKeyCRUCECLIENTECOMP3:
//          campo == 'NUMSERIECOMP4'? _formKeyNUMSERIECOMP4:
//          campo == 'CRUCECLIENTECOMP4'? _formKeyCRUCECLIENTECOMP4:
//          campo == 'NUMSERIECOMP5'? _formKeyNUMSERIECOMP5:
//          campo == 'CRUCECLIENTECOMP5'? _formKeyCRUCECLIENTECOMP5:
//          campo == 'NUMSERIECOMP6'? _formKeyNUMSERIECOMP6:
//          campo == 'CRUCECLIENTECOMP6'? _formKeyCRUCECLIENTECOMP6:
//          campo == 'NUMSERIECOMP7'? _formKeyNUMSERIECOMP7:
//          campo == 'CRUCECLIENTECOMP7'? _formKeyCRUCECLIENTECOMP7:
//          campo == 'TECNICONOMBRE'? _formKeyTECNICONOMBRE:
//          null,
          child: TextFormField(
          autocorrect: true,
          showCursor: true,
          textCapitalization: campo == 'CORREO' ? TextCapitalization.none : TextCapitalization.characters,
          style: TextStyle(
            color: Colors.black,
          ),
          decoration: new InputDecoration(
            fillColor: Colors.white,
            border: new OutlineInputBorder(
              borderRadius: new BorderRadius.circular(6.0),
              borderSide: new BorderSide(),
            ),
          ),

          controller:
          campo == 'PROYECTO'?    GlobalVariables.controllerPROYECTO:
          campo == 'PROYECTODESCRIPCION'?    GlobalVariables.controllerPROYECTODESCRIPCION:
          campo == 'FCREACON'?    GlobalVariables.controllerFCREACON:
          campo == 'FOLIO'?    GlobalVariables.controllerFOLIO:
          campo == 'ID'?    GlobalVariables.controllerID:
          campo == 'APATERNO'?    GlobalVariables.controllerAPELLIDOS:
          campo == 'AMATERNO'?    GlobalVariables.controllerAPELLIDOS2:
          campo == 'NOMBRES'?    GlobalVariables.controllerNOMBRES:
          campo == 'NOMBRECOMPLETO'?    GlobalVariables.controllerNOMBRECOMPLETO:
          campo == 'NUMEMPLEADO'?    GlobalVariables.controllerNUMEMPLEADO:
          campo == 'VIP'?    GlobalVariables.controllerVIP:
          campo == 'PUESTO'?    GlobalVariables.controllerPUESTO:
          campo == 'DIRECCION'?    GlobalVariables.controllerDIRECCION:
          campo == 'SUBDIRECCION'?    GlobalVariables.controllerSUBDIRECCION:
          campo == 'CLAVESUBDIRECCION'?    GlobalVariables.controllerCLAVESUBDIRECCION:
          campo == 'GERENCIA'?    GlobalVariables.controllerGERENCIA:
          campo == 'CLAVEGERENCIA'?    GlobalVariables.controllerCLAVEGERENCIA:
          campo == 'DEPTO'?    GlobalVariables.controllerDEPTO:
          campo == 'CLAVECENTROTRABAJO'?    GlobalVariables.controllerCLAVECENTROTRABAJO:
          campo == 'CORREO'?    GlobalVariables.controllerCORREO:
          campo == 'TELEFONO'?    GlobalVariables.controllerTELEFONO:
          campo == 'EXT'?    GlobalVariables.controllerEXT:
          campo == 'UBICACION'?    GlobalVariables.controllerUBICACION:
          campo == 'ESTADO'?    GlobalVariables.controllerESTADO:
          campo == 'CP'?    GlobalVariables.controllerCP:
          campo == 'COLONIA'?    GlobalVariables.controllerCOLONIA:
          campo == 'UBICACIONCOMPLETA'?    GlobalVariables.controllerUBICACIONCOMPLETA:
          campo == 'ZONA'?    GlobalVariables.controllerZONA:
          campo == 'LOCALIDAD'?    GlobalVariables.controllerLOCALIDAD:
          campo == 'EDIFICIO'?    GlobalVariables.controllerEDIFICIO:
          campo == 'PISO'?    GlobalVariables.controllerPISO:
          campo == 'AREA'?    GlobalVariables.controllerAREA:
          campo == 'ADSCRIPCION'?    GlobalVariables.controllerADSCRIPCION:
          campo == 'APELLIDOSJEFE'?    GlobalVariables.controllerAPELLIDOSJEFE:
          campo == 'APELLIDOS2JEFE'?    GlobalVariables.controllerAPELLIDOS2JEFE:
          campo == 'NOMBRESJEFE'?    GlobalVariables.controllerNOMBRESJEFE:
          campo == 'NOMBRECOMPLETOJEFE'?    GlobalVariables.controllerNOMBRECOMPLETOJEFE:
          campo == 'FICHAJEFE'?    GlobalVariables.controllerFICHAJEFE:
          campo == 'EXTJEFE'?    GlobalVariables.controllerEXTJEFE:
          campo == 'UBICACIONJEFE'?    GlobalVariables.controllerUBICACIONJEFE:
          campo == 'NOMBREJEFEINMEDIATO'?    GlobalVariables.controllerNOMBREJEFEINMEDIATO:
          campo == 'APELLIDOSRESGUARDO'?    GlobalVariables.controllerAPELLIDOSRESGUARDO:
          campo == 'APELLIDOS2RESGUARDO'?    GlobalVariables.controllerAPELLIDOS2RESGUARDO:
          campo == 'NOMBRESRESGUARDO'?    GlobalVariables.controllerNOMBRESRESGUARDO:
          campo == 'NOMBRECOMPLETORESGUARDO'?    GlobalVariables.controllerNOMBRECOMPLETORESGUARDO:
          campo == 'ADSCRIPCIONRESGUARDO'?    GlobalVariables.controllerADSCRIPCIONRESGUARDO:
          campo == 'EXTRESGUARDO'?    GlobalVariables.controllerEXTRESGUARDO:
          campo == 'APELLIDOSRESPONSABLE'?    GlobalVariables.controllerAPELLIDOSRESPONSABLE:
          campo == 'APELLIDOS2RESPONSABLE'?    GlobalVariables.controllerAPELLIDOS2RESPONSABLE:
          campo == 'NOMBRESRESPONSABLE'?    GlobalVariables.controllerNOMBRESRESPONSABLE:
          campo == 'NOMBRECOMPLETORESPONSABLE'?    GlobalVariables.controllerNOMBRECOMPLETORESPONSABLE:
          campo == 'APELLIDOSPEMEX'?    GlobalVariables.controllerAPELLIDOSPEMEX:
          campo == 'APELLIDOS2PEMEX'?    GlobalVariables.controllerAPELLIDOS2PEMEX:
          campo == 'NOMBRESPEMEX'?    GlobalVariables.controllerNOMBRESPEMEX:
          campo == 'NOMBRECOMPLETOPEMEX'?    GlobalVariables.controllerNOMBRECOMPLETOPEMEX:
          campo == 'TIPOEQUIPO'?    GlobalVariables.controllerTIPOEQUIPO:
          campo == 'EQUIPO'?    GlobalVariables.controllerEQUIPO:
          campo == 'MARCAEQUIPO'?    GlobalVariables.controllerMARCAEQUIPO:
          campo == 'MODELOEQUIPO'?    GlobalVariables.controllerMODELOEQUIPO:
          campo == 'NUMSERIEEQUIPO'?    GlobalVariables.controllerNUMSERIEEQUIPO:
          campo == 'EQUIPOCOMPLETO'?    GlobalVariables.controllerEQUIPOCOMPLETO:
          campo == 'MONITOR'?    GlobalVariables.controllerMONITOR:
          campo == 'MARCAMONITOR'?    GlobalVariables.controllerMARCAMONITOR:
          campo == 'MODELOMONITOR'?    GlobalVariables.controllerMODELOMONITOR:
          campo == 'NUMSERIEMONITOR'?    GlobalVariables.controllerNUMSERIEMONITOR:
          campo == 'MONITORCOMPLETO'?    GlobalVariables.controllerMONITORCOMPLETO:
          campo == 'TECLADO'?    GlobalVariables.controllerTECLADO:
          campo == 'MARCATECLADO'?    GlobalVariables.controllerMARCATECLADO:
          campo == 'MODELOTECLADO'?    GlobalVariables.controllerMODELOTECLADO:
          campo == 'NUMSERIETECLADO'?    GlobalVariables.controllerNUMSERIETECLADO:
          campo == 'TECLADOCOMPLETO'?    GlobalVariables.controllerTECLADOCOMPLETO:
          campo == 'MOUSE'?    GlobalVariables.controllerMOUSE:
          campo == 'MARCAMOUSE'?    GlobalVariables.controllerMARCAMOUSE:
          campo == 'MODELOMAUSE'?    GlobalVariables.controllerMODELOMAUSE:
          campo == 'NUMSERIEMOUSE'?    GlobalVariables.controllerNUMSERIEMOUSE:
          campo == 'MOUSECOMPLETO'?    GlobalVariables.controllerMOUSECOMPLETO:
          campo == 'UPS'?    GlobalVariables.controllerUPS:
          campo == 'MARCAUPS'?    GlobalVariables.controllerMARCAUPS:
          campo == 'MODELOUPS'?    GlobalVariables.controllerMODELOUPS:
          campo == 'NUMSERIEUPS'?    GlobalVariables.controllerNUMSERIEUPS:
          campo == 'UPSCOMPLETO'?    GlobalVariables.controllerUPSCOMPLETO:
          campo == 'MALETIN'?    GlobalVariables.controllerMALETIN:
          campo == 'MARCAMALETIN'?    GlobalVariables.controllerMARCAMALETIN:
          campo == 'MODELOMALETIN'?    GlobalVariables.controllerMODELOMALETIN:
          campo == 'NUMSERIEMALETIN'?    GlobalVariables.controllerNUMSERIEMALETIN:
          campo == 'MALETINCOMLETO'?    GlobalVariables.controllerMALETINCOMLETO:
          campo == 'CANDADO'?    GlobalVariables.controllerCANDADO:
          campo == 'MARCACANDADO'?    GlobalVariables.controllerMARCACANDADO:
          campo == 'MODELOCANDADO'?    GlobalVariables.controllerMODELOCANDADO:
          campo == 'NUMSERIECANDADO'?    GlobalVariables.controllerNUMSERIECANDADO:
          campo == 'CANDADOCOMPLETO'?    GlobalVariables.controllerCANDADOCOMPLETO:
          campo == 'BOCINAS'?    GlobalVariables.controllerBOCINAS:
          campo == 'MARCABOCINAS'?    GlobalVariables.controllerMARCABOCINAS:
          campo == 'MODELOBOCINAS'?    GlobalVariables.controllerMODELOBOCINAS:
          campo == 'NUMSERIEBOCINAS'?    GlobalVariables.controllerNUMSERIEBOCINAS:
          campo == 'BOCINASCOMPLETO'?    GlobalVariables.controllerBOCINASCOMPLETO:
          campo == 'CAMARA'?    GlobalVariables.controllerCAMARA:
          campo == 'MARCACAMARA'?    GlobalVariables.controllerMARCACAMARA:
          campo == 'MODELOCAMARA'?    GlobalVariables.controllerMODELOCAMARA:
          campo == 'NUMSERIECMARA'?    GlobalVariables.controllerNUMSERIECMARA:
          campo == 'CAMARACOMPLETO'?    GlobalVariables.controllerCAMARACOMPLETO:
          campo == 'MONITOR2'?    GlobalVariables.controllerMONITOR2:
          campo == 'MARCAMONITOR2'?    GlobalVariables.controllerMARCAMONITOR2:
          campo == 'MODELOMONITOR2'?    GlobalVariables.controllerMODELOMONITOR2:
          campo == 'NUMSERIEMONITOR2'?    GlobalVariables.controllerNUMSERIEMONITOR2:
          campo == 'MONITOR2COMPLETO'?    GlobalVariables.controllerMONITOR2COMPLETO:
          campo == 'ACCESORIO'?    GlobalVariables.controllerACCESORIO:
          campo == 'MARCAACCESORIO'?    GlobalVariables.controllerMARCAACCESORIO:
          campo == 'MODELOACCESORIO'?    GlobalVariables.controllerMODELOACCESORIO:
          campo == 'NUMSERIEACCESORIO'?    GlobalVariables.controllerNUMSERIEACCESORIO:
          campo == 'ACCESORIOCOMPLETO'?    GlobalVariables.controllerACCESORIOCOMPLETO:
          campo == 'RAM'?    GlobalVariables.controllerRAM:
          campo == 'DISCODURO'?    GlobalVariables.controllerDISCODURO:
          campo == 'PROCESADOR'?    GlobalVariables.controllerPROCESADOR:
          campo == 'TIPOEQUIPOCOMP1'?    GlobalVariables.controllerTIPOEQUIPOCOMP1:
          campo == 'MODELOCOMP1'?    GlobalVariables.controllerMODELOCOMP1:
          campo == 'NUMSERIECOMP1'?    GlobalVariables.controllerNUMSERIECOMP1:
          campo == 'CRUCECLIENTECOMP1'?    GlobalVariables.controllerCRUCECLIENTECOMP1:
          campo == 'TIPOEQUIPOCOMP2'?    GlobalVariables.controllerTIPOEQUIPOCOMP2:
          campo == 'MODELOCOMP2'?    GlobalVariables.controllerMODELOCOMP2:
          campo == 'NUMSERIECOMP2'?    GlobalVariables.controllerNUMSERIECOMP2:
          campo == 'CRUCECLIENTECOMP2'?    GlobalVariables.controllerCRUCECLIENTECOMP2:
          campo == 'TIPOEQUIPOCOMP3'?    GlobalVariables.controllerTIPOEQUIPOCOMP3:
          campo == 'MODELOCOMP3'?    GlobalVariables.controllerMODELOCOMP3:
          campo == 'NUMSERIECOMP3'?    GlobalVariables.controllerNUMSERIECOMP3:
          campo == 'CRUCECLIENTECOMP3'?    GlobalVariables.controllerCRUCECLIENTECOMP3:
          campo == 'TIPOEQUIPOCOMP4'?    GlobalVariables.controllerTIPOEQUIPOCOMP4:
          campo == 'MODELOCOMP4'?    GlobalVariables.controllerMODELOCOMP4:
          campo == 'NUMSERIECOMP4'?    GlobalVariables.controllerNUMSERIECOMP4:
          campo == 'CRUCECLIENTECOMP4'?    GlobalVariables.controllerCRUCECLIENTECOMP4:
          campo == 'TIPOEQUIPOCOMP5'?    GlobalVariables.controllerTIPOEQUIPOCOMP5:
          campo == 'MODELOCOMP5'?    GlobalVariables.controllerMODELOCOMP5:
          campo == 'NUMSERIECOMP5'?    GlobalVariables.controllerNUMSERIECOMP5:
          campo == 'CRUCECLIENTECOMP5'?    GlobalVariables.controllerCRUCECLIENTECOMP5:
          campo == 'TIPOEQUIPOCOMP6'?    GlobalVariables.controllerTIPOEQUIPOCOMP6:
          campo == 'MODELOCOMP6'?    GlobalVariables.controllerMODELOCOMP6:
          campo == 'NUMSERIECOMP6'?    GlobalVariables.controllerNUMSERIECOMP6:
          campo == 'CRUCECLIENTECOMP6'?    GlobalVariables.controllerCRUCECLIENTECOMP6:
          campo == 'TIPOEQUIPOCOMP7'?    GlobalVariables.controllerTIPOEQUIPOCOMP7:
          campo == 'MODELOCOMP7'?    GlobalVariables.controllerMODELOCOMP7:
          campo == 'NUMSERIECOMP7'?    GlobalVariables.controllerNUMSERIECOMP7:
          campo == 'CRUCECLIENTECOMP7'?    GlobalVariables.controllerCRUCECLIENTECOMP7:
          campo == 'VALIDACIONCOMP1'?    GlobalVariables.controllerVALIDACIONCOMP1:
          campo == 'VALIDACIONCOMP2'?    GlobalVariables.controllerVALIDACIONCOMP2:
          campo == 'VALIDACIONCOMP3'?    GlobalVariables.controllerVALIDACIONCOMP3:
          campo == 'VALIDACIONCOMP4'?    GlobalVariables.controllerVALIDACIONCOMP4:
          campo == 'VALIDACIONCOMP5'?    GlobalVariables.controllerVALIDACIONCOMP5:
          campo == 'VALIDACIONCOMP6'?    GlobalVariables.controllerVALIDACIONCOMP6:
          campo == 'VALIDACIONCOMP7'?    GlobalVariables.controllerVALIDACIONCOMP7:
          campo == 'VALIDADOSCOMP'?    GlobalVariables.controllerVALIDADOSCOMP:
          campo == 'TECNICONOMBRE'?    GlobalVariables.controllerTECNICONOMBRE:
          campo == 'DIA'?    GlobalVariables.controllerDIA:
          campo == 'MES'?    GlobalVariables.controllerMES:
          campo == 'ANIO'?    GlobalVariables.controllerANIO:
          campo == 'REQESPECIAL1'?    GlobalVariables.controllerREQESPECIAL1:
          campo == 'REQESPECIAL2'?    GlobalVariables.controllerREQESPECIAL2:
          campo == 'OBSINV'?    GlobalVariables.controllerOBSINV:
          campo == 'OBSRESGUARDO'?    GlobalVariables.controllerOBSRESGUARDO:
          campo == 'OBSEXTRAS1'?    GlobalVariables.controllerOBSEXTRAS1:
          campo == 'OBSEXTRAS2'?    GlobalVariables.controllerOBSEXTRAS2:
          campo == 'ESTATUS'?    GlobalVariables.controllerESTATUS:
          campo == 'FESCALACION'?    GlobalVariables.controllerFESCALACION:
          campo == 'COMENTARIOSESCALACION'?    GlobalVariables.controllerCOMENTARIOSESCALACION:
          campo == 'CAMPOLIBRE1'?    GlobalVariables.controllerCAMPOLIBRE1:
          campo == 'CAMPOLIBRE2'?    GlobalVariables.controllerCAMPOLIBRE2:
          campo == 'CAMPOLIBRE3'?    GlobalVariables.controllerCAMPOLIBRE3:
          campo == 'CAMPOLIBRE4'?    GlobalVariables.controllerCAMPOLIBRE4:
          campo == 'CAMPOLIBRE5'?    GlobalVariables.controllerCAMPOLIBRE5:
          campo == 'CAMPOLIBRE6'?    GlobalVariables.controllerCAMPOLIBRE6:
          campo == 'CAMPOLIBRE7'?    GlobalVariables.controllerCAMPOLIBRE7:
          campo == 'CAMPOLIBRE8'?    GlobalVariables.controllerCAMPOLIBRE8:
          campo == 'CAMPOLIBRE9'?    GlobalVariables.controllerCAMPOLIBRE9:
          campo == 'CAMPOLIBRE10'?    GlobalVariables.controllerCAMPOLIBRE10:
          campo == 'CAMPOLIBRE11'?    GlobalVariables.controllerCAMPOLIBRE11:
          campo == 'CAMPOLIBRE12'?    GlobalVariables.controllerCAMPOLIBRE12:
          campo == 'CAMPOLIBRE13'?    GlobalVariables.controllerCAMPOLIBRE13:
          campo == 'CAMPOLIBRE14'?    GlobalVariables.controllerCAMPOLIBRE14:
          campo == 'CAMPOLIBRE15'?    GlobalVariables.controllerCAMPOLIBRE15:
          campo == 'CAMPOLIBRE16'?    GlobalVariables.controllerCAMPOLIBRE16:
          campo == 'CAMPOLIBRE17'?    GlobalVariables.controllerCAMPOLIBRE17:
          campo == 'CAMPOLIBRE18'?    GlobalVariables.controllerCAMPOLIBRE18:
          campo == 'CAMPOLIBRE19'?    GlobalVariables.controllerCAMPOLIBRE19:
          campo == 'CAMPOLIBRE20'?    GlobalVariables.controllerCAMPOLIBRE20:
          campo == 'CAMPOID'?    GlobalVariables.controllerCAMPOID:

          null,
          autovalidate: true,
          validator: (value){

           if(campo == 'APATERNO'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'AMATERNO'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'NOMBRES'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'PUESTO'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'DIRECCION'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'SUBDIRECCION'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'GERENCIA'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'DEPTO'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CORREO'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              String p =r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
              RegExp regex = new RegExp(p);
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'UBICACION'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'ZONA'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'EDIFICIO'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'AREA'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'ADSCRIPCION'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'APELLIDOSJEFE'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
           else if(campo == 'APELLIDOS2JEFE'){
             //print('campo:: ' + campo + ' patter: ' + patter);
             RegExp regex = new RegExp(patter.toString());
             if (!regex.hasMatch(value))
               return mensaje;
             else
               return null;
           }
            else if(campo == 'NOMBRESJEFE'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'UBICACIONJEFE'){
             // print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'NOMBREJEFEINMEDIATO'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'APELLIDOSRESGUARDO'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
           else if(campo == 'APELLIDOS2RESGUARDO'){
             //print('campo:: ' + campo + ' patter: ' + patter);
             RegExp regex = new RegExp(patter.toString());
             if (!regex.hasMatch(value))
               return mensaje;
             else
               return null;
           }
            else if(campo == 'NOMBRESRESGUARDO'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'ADSCRIPCIONRESGUARDO'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'APELLIDOSRESPONSABLE'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
           else if(campo == 'APELLIDOS2RESPONSABLE'){
             //print('campo:: ' + campo + ' patter: ' + patter);
             RegExp regex = new RegExp(patter.toString());
             if (!regex.hasMatch(value))
               return mensaje;
             else
               return null;
           }
            else if(campo == 'NOMBRESRESPONSABLE'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'APELLIDOSPEMEX'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
           else if(campo == 'APELLIDOS2PEMEX'){
             //print('campo:: ' + campo + ' patter: ' + patter);
             RegExp regex = new RegExp(patter.toString());
             if (!regex.hasMatch(value))
               return mensaje;
             else
               return null;
           }
            else if(campo == 'NOMBRESPEMEX'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'NUMSERIEEQUIPO'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'NUMSERIEMONITOR'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'NUMSERIETECLADO'){
             // print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'NUMSERIEMOUSE'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'NUMSERIEUPS'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'NUMSERIEMALETIN'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'NUMSERIEMONITOR2'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'NUMSERIEACCESORIO'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'PROCESADOR'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'NUMSERIECOMP1'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CRUCECLIENTECOMP1'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'NUMSERIECOMP2'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CRUCECLIENTECOMP2'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'NUMSERIECOMP3'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CRUCECLIENTECOMP3'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'NUMSERIECOMP4'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CRUCECLIENTECOMP4'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'NUMSERIECOMP5'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CRUCECLIENTECOMP5'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'NUMSERIECOMP6'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CRUCECLIENTECOMP6'){
              print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'NUMSERIECOMP7'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CRUCECLIENTECOMP7'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'TECNICONOMBRE'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'REQESPECIAL1'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'REQESPECIAL2'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'OBSINV'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'OBSRESGUARDO'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'OBSEXTRAS1'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'OBSEXTRAS2'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'COMENTARIOSESCALACION'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CAMPOLIBRE1'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CAMPOLIBRE2'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CAMPOLIBRE3'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CAMPOLIBRE4'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CAMPOLIBRE5'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CAMPOLIBRE6'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CAMPOLIBRE7'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CAMPOLIBRE8'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CAMPOLIBRE9'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CAMPOLIBRE10'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CAMPOLIBRE11'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CAMPOLIBRE12'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CAMPOLIBRE13'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CAMPOLIBRE14'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CAMPOLIBRE15'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CAMPOLIBRE16'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CAMPOLIBRE17'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CAMPOLIBRE18'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CAMPOLIBRE19'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
            else if(campo == 'CAMPOLIBRE20'){
              //print('campo:: ' + campo + ' patter: ' + patter);
              RegExp regex = new RegExp(patter.toString());
              if (!regex.hasMatch(value))
                return mensaje;
              else
                return null;
            }
           else if(campo == 'CAMPOID'){
             //print('campo:: ' + campo + ' patter: ' + patter);
             RegExp regex = new RegExp(patter.toString());
             if (!regex.hasMatch(value))
               return mensaje;
             else
               return null;
           }



          },
        ),
        //),
      ),
    );
  }

  static Widget getInputNumeric(BuildContext context,String campo, String patter,String mensaje){
   // print('patter:: ' + patter== null ? '' : campo + ' patter: ' + patter);
    return  Container(

      padding: EdgeInsets.fromLTRB(8, 1, 1, 1),
      child: SizedBox(
        height: MediaQuery.of(context).size.height * 0.09,
        width: double.infinity,

//        child: Form(
//          key: campo == 'NUMEMPLEADO'? _formKeyNumEmpleado :
//          campo == 'TELEFONO'? _formKeyTel :
//          campo == 'FICHAJEFE'? _formKeyFichaJefe :
//          campo == 'EXT'? _formKeyEXT :null,
          child:TextFormField(
          keyboardType: TextInputType.number,
          textCapitalization: TextCapitalization.characters,
          style: TextStyle(
            color: Colors.black,
          ),
          decoration: new InputDecoration(
            fillColor: Colors.white,
            border: new OutlineInputBorder(
              borderRadius: new BorderRadius.circular(6.0),
              borderSide: new BorderSide(),
            ),
          ),
          controller:
          campo == 'PROYECTO'?    GlobalVariables.controllerPROYECTO:
          campo == 'PROYECTODESCRIPCION'?    GlobalVariables.controllerPROYECTODESCRIPCION:
          campo == 'FCREACON'?    GlobalVariables.controllerFCREACON:
          campo == 'FOLIO'?    GlobalVariables.controllerFOLIO:
          campo == 'ID'?    GlobalVariables.controllerID:
          campo == 'APATERNO'?    GlobalVariables.controllerAPELLIDOS:
          campo == 'AMATERNO'?    GlobalVariables.controllerAPELLIDOS2:
          campo == 'NOMBRES'?    GlobalVariables.controllerNOMBRES:
          campo == 'NOMBRECOMPLETO'?    GlobalVariables.controllerNOMBRECOMPLETO:
          campo == 'NUMEMPLEADO'?    GlobalVariables.controllerNUMEMPLEADO:
          campo == 'VIP'?    GlobalVariables.controllerVIP:
          campo == 'PUESTO'?    GlobalVariables.controllerPUESTO:
          campo == 'DIRECCION'?    GlobalVariables.controllerDIRECCION:
          campo == 'SUBDIRECCION'?    GlobalVariables.controllerSUBDIRECCION:
          campo == 'CLAVESUBDIRECCION'?    GlobalVariables.controllerCLAVESUBDIRECCION:
          campo == 'GERENCIA'?    GlobalVariables.controllerGERENCIA:
          campo == 'CLAVEGERENCIA'?    GlobalVariables.controllerCLAVEGERENCIA:
          campo == 'DEPTO'?    GlobalVariables.controllerDEPTO:
          campo == 'CLAVECENTROTRABAJO'?    GlobalVariables.controllerCLAVECENTROTRABAJO:
          campo == 'CORREO'?    GlobalVariables.controllerCORREO:
          campo == 'TELEFONO'?    GlobalVariables.controllerTELEFONO:
          campo == 'EXT'?    GlobalVariables.controllerEXT:
          campo == 'UBICACION'?    GlobalVariables.controllerUBICACION:
          campo == 'ESTADO'?    GlobalVariables.controllerESTADO:
          campo == 'CP'?    GlobalVariables.controllerCP:
          campo == 'COLONIA'?    GlobalVariables.controllerCOLONIA:
          campo == 'UBICACIONCOMPLETA'?    GlobalVariables.controllerUBICACIONCOMPLETA:
          campo == 'ZONA'?    GlobalVariables.controllerZONA:
          campo == 'LOCALIDAD'?    GlobalVariables.controllerLOCALIDAD:
          campo == 'EDIFICIO'?    GlobalVariables.controllerEDIFICIO:
          campo == 'PISO'?    GlobalVariables.controllerPISO:
          campo == 'AREA'?    GlobalVariables.controllerAREA:
          campo == 'ADSCRIPCION'?    GlobalVariables.controllerADSCRIPCION:
          campo == 'APELLIDOSJEFE'?    GlobalVariables.controllerAPELLIDOSJEFE:
          campo == 'NOMBRESJEFE'?    GlobalVariables.controllerNOMBRESJEFE:
          campo == 'NOMBRECOMPLETOJEFE'?    GlobalVariables.controllerNOMBRECOMPLETOJEFE:
          campo == 'FICHAJEFE'?    GlobalVariables.controllerFICHAJEFE:
          campo == 'EXTJEFE'?    GlobalVariables.controllerEXTJEFE:
          campo == 'UBICACIONJEFE'?    GlobalVariables.controllerUBICACIONJEFE:
          campo == 'NOMBREJEFEINMEDIATO'?    GlobalVariables.controllerNOMBREJEFEINMEDIATO:
          campo == 'APELLIDOSRESGUARDO'?    GlobalVariables.controllerAPELLIDOSRESGUARDO:
          campo == 'NOMBRESRESGUARDO'?    GlobalVariables.controllerNOMBRESRESGUARDO:
          campo == 'NOMBRECOMPLETORESGUARDO'?    GlobalVariables.controllerNOMBRECOMPLETORESGUARDO:
          campo == 'ADSCRIPCIONRESGUARDO'?    GlobalVariables.controllerADSCRIPCIONRESGUARDO:
          campo == 'EXTRESGUARDO'?    GlobalVariables.controllerEXTRESGUARDO:
          campo == 'APELLIDOSRESPONSABLE'?    GlobalVariables.controllerAPELLIDOSRESPONSABLE:
          campo == 'NOMBRESRESPONSABLE'?    GlobalVariables.controllerNOMBRESRESPONSABLE:
          campo == 'NOMBRECOMPLETORESPONSABLE'?    GlobalVariables.controllerNOMBRECOMPLETORESPONSABLE:
          campo == 'APELLIDOSPEMEX'?    GlobalVariables.controllerAPELLIDOSPEMEX:
          campo == 'NOMBRESPEMEX'?    GlobalVariables.controllerNOMBRESPEMEX:
          campo == 'NOMBRECOMPLETOPEMEX'?    GlobalVariables.controllerNOMBRECOMPLETOPEMEX:
          campo == 'TIPOEQUIPO'?    GlobalVariables.controllerTIPOEQUIPO:
          campo == 'EQUIPO'?    GlobalVariables.controllerEQUIPO:
          campo == 'MARCAEQUIPO'?    GlobalVariables.controllerMARCAEQUIPO:
          campo == 'MODELOEQUIPO'?    GlobalVariables.controllerMODELOEQUIPO:
          campo == 'NUMSERIEEQUIPO'?    GlobalVariables.controllerNUMSERIEEQUIPO:
          campo == 'EQUIPOCOMPLETO'?    GlobalVariables.controllerEQUIPOCOMPLETO:
          campo == 'MONITOR'?    GlobalVariables.controllerMONITOR:
          campo == 'MARCAMONITOR'?    GlobalVariables.controllerMARCAMONITOR:
          campo == 'MODELOMONITOR'?    GlobalVariables.controllerMODELOMONITOR:
          campo == 'NUMSERIEMONITOR'?    GlobalVariables.controllerNUMSERIEMONITOR:
          campo == 'MONITORCOMPLETO'?    GlobalVariables.controllerMONITORCOMPLETO:
          campo == 'TECLADO'?    GlobalVariables.controllerTECLADO:
          campo == 'MARCATECLADO'?    GlobalVariables.controllerMARCATECLADO:
          campo == 'MODELOTECLADO'?    GlobalVariables.controllerMODELOTECLADO:
          campo == 'NUMSERIETECLADO'?    GlobalVariables.controllerNUMSERIETECLADO:
          campo == 'TECLADOCOMPLETO'?    GlobalVariables.controllerTECLADOCOMPLETO:
          campo == 'MOUSE'?    GlobalVariables.controllerMOUSE:
          campo == 'MARCAMOUSE'?    GlobalVariables.controllerMARCAMOUSE:
          campo == 'MODELOMAUSE'?    GlobalVariables.controllerMODELOMAUSE:
          campo == 'NUMSERIEMOUSE'?    GlobalVariables.controllerNUMSERIEMOUSE:
          campo == 'MOUSECOMPLETO'?    GlobalVariables.controllerMOUSECOMPLETO:
          campo == 'UPS'?    GlobalVariables.controllerUPS:
          campo == 'MARCAUPS'?    GlobalVariables.controllerMARCAUPS:
          campo == 'MODELOUPS'?    GlobalVariables.controllerMODELOUPS:
          campo == 'NUMSERIEUPS'?    GlobalVariables.controllerNUMSERIEUPS:
          campo == 'UPSCOMPLETO'?    GlobalVariables.controllerUPSCOMPLETO:
          campo == 'MALETIN'?    GlobalVariables.controllerMALETIN:
          campo == 'MARCAMALETIN'?    GlobalVariables.controllerMARCAMALETIN:
          campo == 'MODELOMALETIN'?    GlobalVariables.controllerMODELOMALETIN:
          campo == 'NUMSERIEMALETIN'?    GlobalVariables.controllerNUMSERIEMALETIN:
          campo == 'MALETINCOMLETO'?    GlobalVariables.controllerMALETINCOMLETO:
          campo == 'CANDADO'?    GlobalVariables.controllerCANDADO:
          campo == 'MARCACANDADO'?    GlobalVariables.controllerMARCACANDADO:
          campo == 'MODELOCANDADO'?    GlobalVariables.controllerMODELOCANDADO:
          campo == 'NUMSERIECANDADO'?    GlobalVariables.controllerNUMSERIECANDADO:
          campo == 'CANDADOCOMPLETO'?    GlobalVariables.controllerCANDADOCOMPLETO:
          campo == 'BOCINAS'?    GlobalVariables.controllerBOCINAS:
          campo == 'MARCABOCINAS'?    GlobalVariables.controllerMARCABOCINAS:
          campo == 'MODELOBOCINAS'?    GlobalVariables.controllerMODELOBOCINAS:
          campo == 'NUMSERIEBOCINAS'?    GlobalVariables.controllerNUMSERIEBOCINAS:
          campo == 'BOCINASCOMPLETO'?    GlobalVariables.controllerBOCINASCOMPLETO:
          campo == 'CAMARA'?    GlobalVariables.controllerCAMARA:
          campo == 'MARCACAMARA'?    GlobalVariables.controllerMARCACAMARA:
          campo == 'MODELOCAMARA'?    GlobalVariables.controllerMODELOCAMARA:
          campo == 'NUMSERIECMARA'?    GlobalVariables.controllerNUMSERIECMARA:
          campo == 'CAMARACOMPLETO'?    GlobalVariables.controllerCAMARACOMPLETO:
          campo == 'MONITOR2'?    GlobalVariables.controllerMONITOR2:
          campo == 'MARCAMONITOR2'?    GlobalVariables.controllerMARCAMONITOR2:
          campo == 'MODELOMONITOR2'?    GlobalVariables.controllerMODELOMONITOR2:
          campo == 'NUMSERIEMONITOR2'?    GlobalVariables.controllerNUMSERIEMONITOR2:
          campo == 'MONITOR2COMPLETO'?    GlobalVariables.controllerMONITOR2COMPLETO:
          campo == 'ACCESORIO'?    GlobalVariables.controllerACCESORIO:
          campo == 'MARCAACCESORIO'?    GlobalVariables.controllerMARCAACCESORIO:
          campo == 'MODELOACCESORIO'?    GlobalVariables.controllerMODELOACCESORIO:
          campo == 'NUMSERIEACCESORIO'?    GlobalVariables.controllerNUMSERIEACCESORIO:
          campo == 'ACCESORIOCOMPLETO'?    GlobalVariables.controllerACCESORIOCOMPLETO:
          campo == 'RAM'?    GlobalVariables.controllerRAM:
          campo == 'DISCODURO'?    GlobalVariables.controllerDISCODURO:
          campo == 'PROCESADOR'?    GlobalVariables.controllerPROCESADOR:
          campo == 'TIPOEQUIPOCOMP1'?    GlobalVariables.controllerTIPOEQUIPOCOMP1:
          campo == 'MODELOCOMP1'?    GlobalVariables.controllerMODELOCOMP1:
          campo == 'NUMSERIECOMP1'?    GlobalVariables.controllerNUMSERIECOMP1:
          campo == 'CRUCECLIENTECOMP1'?    GlobalVariables.controllerCRUCECLIENTECOMP1:
          campo == 'TIPOEQUIPOCOMP2'?    GlobalVariables.controllerTIPOEQUIPOCOMP2:
          campo == 'MODELOCOMP2'?    GlobalVariables.controllerMODELOCOMP2:
          campo == 'NUMSERIECOMP2'?    GlobalVariables.controllerNUMSERIECOMP2:
          campo == 'CRUCECLIENTECOMP2'?    GlobalVariables.controllerCRUCECLIENTECOMP2:
          campo == 'TIPOEQUIPOCOMP3'?    GlobalVariables.controllerTIPOEQUIPOCOMP3:
          campo == 'MODELOCOMP3'?    GlobalVariables.controllerMODELOCOMP3:
          campo == 'NUMSERIECOMP3'?    GlobalVariables.controllerNUMSERIECOMP3:
          campo == 'CRUCECLIENTECOMP3'?    GlobalVariables.controllerCRUCECLIENTECOMP3:
          campo == 'TIPOEQUIPOCOMP4'?    GlobalVariables.controllerTIPOEQUIPOCOMP4:
          campo == 'MODELOCOMP4'?    GlobalVariables.controllerMODELOCOMP4:
          campo == 'NUMSERIECOMP4'?    GlobalVariables.controllerNUMSERIECOMP4:
          campo == 'CRUCECLIENTECOMP4'?    GlobalVariables.controllerCRUCECLIENTECOMP4:
          campo == 'TIPOEQUIPOCOMP5'?    GlobalVariables.controllerTIPOEQUIPOCOMP5:
          campo == 'MODELOCOMP5'?    GlobalVariables.controllerMODELOCOMP5:
          campo == 'NUMSERIECOMP5'?    GlobalVariables.controllerNUMSERIECOMP5:
          campo == 'CRUCECLIENTECOMP5'?    GlobalVariables.controllerCRUCECLIENTECOMP5:
          campo == 'TIPOEQUIPOCOMP6'?    GlobalVariables.controllerTIPOEQUIPOCOMP6:
          campo == 'MODELOCOMP6'?    GlobalVariables.controllerMODELOCOMP6:
          campo == 'NUMSERIECOMP6'?    GlobalVariables.controllerNUMSERIECOMP6:
          campo == 'CRUCECLIENTECOMP6'?    GlobalVariables.controllerCRUCECLIENTECOMP6:
          campo == 'TIPOEQUIPOCOMP7'?    GlobalVariables.controllerTIPOEQUIPOCOMP7:
          campo == 'MODELOCOMP7'?    GlobalVariables.controllerMODELOCOMP7:
          campo == 'NUMSERIECOMP7'?    GlobalVariables.controllerNUMSERIECOMP7:
          campo == 'CRUCECLIENTECOMP7'?    GlobalVariables.controllerCRUCECLIENTECOMP7:
          campo == 'VALIDACIONCOMP1'?    GlobalVariables.controllerVALIDACIONCOMP1:
          campo == 'VALIDACIONCOMP2'?    GlobalVariables.controllerVALIDACIONCOMP2:
          campo == 'VALIDACIONCOMP3'?    GlobalVariables.controllerVALIDACIONCOMP3:
          campo == 'VALIDACIONCOMP4'?    GlobalVariables.controllerVALIDACIONCOMP4:
          campo == 'VALIDACIONCOMP5'?    GlobalVariables.controllerVALIDACIONCOMP5:
          campo == 'VALIDACIONCOMP6'?    GlobalVariables.controllerVALIDACIONCOMP6:
          campo == 'VALIDACIONCOMP7'?    GlobalVariables.controllerVALIDACIONCOMP7:
          campo == 'VALIDADOSCOMP'?    GlobalVariables.controllerVALIDADOSCOMP:
          campo == 'TECNICONOMBRE'?    GlobalVariables.controllerTECNICONOMBRE:
          campo == 'DIA'?    GlobalVariables.controllerDIA:
          campo == 'MES'?    GlobalVariables.controllerMES:
          campo == 'ANIO'?    GlobalVariables.controllerANIO:
          campo == 'REQESPECIAL1'?    GlobalVariables.controllerREQESPECIAL1:
          campo == 'REQESPECIAL2'?    GlobalVariables.controllerREQESPECIAL2:
          campo == 'OBSINV'?    GlobalVariables.controllerOBSINV:
          campo == 'OBSRESGUARDO'?    GlobalVariables.controllerOBSRESGUARDO:
          campo == 'OBSEXTRAS1'?    GlobalVariables.controllerOBSEXTRAS1:
          campo == 'OBSEXTRAS2'?    GlobalVariables.controllerOBSEXTRAS2:
          campo == 'ESTATUS'?    GlobalVariables.controllerESTATUS:
          campo == 'FESCALACION'?    GlobalVariables.controllerFESCALACION:
          campo == 'COMENTARIOSESCALACION'?    GlobalVariables.controllerCOMENTARIOSESCALACION:
          null,
          autovalidate: true,
            maxLength:
            //campo == 'NUMEMPLEADO' ? 6 :
            campo == 'TELEFONO' ? 10 :
            //campo == 'FICHAJEFE' ? 6 :
            //campo == 'EXTJEFE' ? 6 :
            //campo == 'EXTRESGUARDO' ? 6 :

            null,
          validator: (value){
          if(campo == 'NUMEMPLEADO'){
            RegExp regex = new RegExp(patter.toString());
            if (!regex.hasMatch(value))
              return mensaje;
            else
              return null;
          }
          else if(campo == 'CLAVESUBDIRECCION'){
            RegExp regex = new RegExp(patter.toString());
            if (!regex.hasMatch(value))
              return mensaje;
            else
              return null;
          }
          else if(campo == 'CLAVEGERENCIA'){
            RegExp regex = new RegExp(patter.toString());
            if (!regex.hasMatch(value))
              return mensaje;
            else
              return null;
          }
          else if(campo == 'CLAVECENTROTRABAJO'){
            RegExp regex = new RegExp(patter.toString());
            if (!regex.hasMatch(value))
              return mensaje;
            else
              return null;
          }
          else if(campo == 'TELEFONO'){

            RegExp regex = new RegExp(patter.toString());
            if (!regex.hasMatch(value))
              return mensaje;
            else
              return null;
          }
          else if(campo == 'EXT'){
            RegExp regex = new RegExp(patter.toString());
            if (!regex.hasMatch(value))
              return mensaje;
            else
              return null;
          }
          else if(campo == 'FICHAJEFE'){
            RegExp regex = new RegExp(patter.toString());
            if (!regex.hasMatch(value))
              return mensaje;
            else
              return null;
          }
          else if(campo == 'EXTJEFE'){
            RegExp regex = new RegExp(patter.toString());
            if (!regex.hasMatch(value))
              return mensaje;
            else
              return null;
          }
          else if(campo == 'EXTRESGUARDO'){
            RegExp regex = new RegExp(patter.toString());
            if (!regex.hasMatch(value))
              return mensaje;
            else
              return null;
          }
          },
        ),
      //),
      ),
    );
  }


  static Widget getInputCPWidget(BuildContext context,String campo){
    return  Container(
      padding: EdgeInsets.fromLTRB(8, 1, 1, 1),
      child: SizedBox(
        height: MediaQuery.of(context).size.height * 0.05,
        width: double.infinity,
        child: TextFormField(
          keyboardType: TextInputType.number,
          textCapitalization: TextCapitalization.characters,
          style: TextStyle(
            color: Colors.black,
          ),
          onChanged: (text) {
            //print("First text field: $text");

            getCPKeyPreseed(context, text);
          },
          decoration: new InputDecoration(
            fillColor: Colors.white,
            border: new OutlineInputBorder(
              borderRadius: new BorderRadius.circular(6.0),
              borderSide: new BorderSide(),
            ),
          ),
          controller:
          campo == 'CP' ?    GlobalVariables.controllerCP:
          null,
          validator: (String value) {
//                return value.contains('@') ? 'Do not use the @ char.' : null;
            if (value.isEmpty) {
              return 'Campo vacio!';
            }
          },
        ),
      ),
    );
  }


  static getCPKeyPreseed(BuildContext context, String text) async{

    print(text);
    print(GlobalVariables.controllerCP.text);
    if(GlobalVariables.controllerCP.text.length > 4){
      ImcRBloc.getAdressCP(context,GlobalVariables.controllerCP.text);

      Future.delayed(Duration(seconds: 2), () => {

        //print(GlobalVariables.adressCP.colonia),

        //  unicodedata.normalize("NFKD", s1)\.encode("ascii","ignore").decode("ascii")\.replace("#", "ñ").replace("%", "Ñ");
        GlobalVariables.controllerCOLONIA.text = GlobalVariables.adressCP.colonia,
        GlobalVariables.controllerLOCALIDAD.text = GlobalVariables.adressCP.localidad,
        GlobalVariables.controllerESTADO.text = GlobalVariables.adressCP.estado,




        ImcRBloc.getCatalogosAdress(context,ApiDefinition.wsAdressCatEstadoCP,'',4),
        ImcRBloc.getCatalogosAdress(context,ApiDefinition.wsAdressCatLocalidadCP, GlobalVariables.adressCP.estado,7),
        ImcRBloc.getCatalogosAdress(context,ApiDefinition.wsAdressCatColoniaCP, GlobalVariables.controllerCP.text,6),
        //print('Lista estado: ' + GlobalVariables.listCatalogos4.toString()),
        print('Lista localidad: '),
        print('Lista colonia: '),



      });



    }else{
      GlobalVariables.controllerCOLONIA.text ='';
      GlobalVariables.controllerLOCALIDAD.text = '';
      GlobalVariables.controllerESTADO.text = '';

    }
  }
  static Widget getInputAddCPWidget(BuildContext context,String campo){
    return  Container(
      padding: EdgeInsets.fromLTRB(8, 1, 1, 1),
      child: SizedBox(
        height: MediaQuery.of(context).size.height * 0.05,
        width: double.infinity,
        child: TextFormField(
          keyboardType: TextInputType.number,
          textCapitalization: TextCapitalization.characters,
          style: TextStyle(
            color: Colors.black,
          ),
          onChanged: (text) {
            //print("First text field: $text");

            getCPKeyPreseed(context, text);
          },
          decoration: new InputDecoration(
            fillColor: Colors.white,
            border: new OutlineInputBorder(
              borderRadius: new BorderRadius.circular(6.0),
              borderSide: new BorderSide(),
            ),
          ),
          controller:
          campo == 'CP' ?    GlobalVariables.controllerCPadd:
          null,
          validator: (String value) {
//                return value.contains('@') ? 'Do not use the @ char.' : null;
            if (value.isEmpty) {
              return 'Campo vacio!';
            }
          },
        ),
      ),
    );
  }

  static showAlertDialog(BuildContext context) {

    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancelar"),
      onPressed:  () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Aceptar"),
      onPressed:  () {
        ImcRBloc.insertInvet(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Alerta"),
      content: Text("¿Esta seguro de guardar el inventario?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }


 static Widget addLeadingIcon(){
    return new Container(
      height: 25.0,
      width: 25.0,
      padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
      margin: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
      child: new Stack(
        alignment: AlignmentDirectional.center,
        children: <Widget>[

          new FlatButton(
              onPressed: (){

              }
          )
        ],
      ),
    );
  }
}
