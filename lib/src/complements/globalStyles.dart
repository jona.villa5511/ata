library ATA.globals;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ATA/src/complements/globalVariables.dart';

class GlobalStyles{
  static String textFL = "";

  static const textTitle = TextStyle(
    color: Color.fromRGBO(36, 90, 149, 1),
  wordSpacing: 1.6,
    fontWeight: FontWeight.bold,
    fontSize: 35,
  );

  static const dateG = TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.bold,
    fontSize: 15,
  );

  static const titleChartG = TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.bold,
    fontSize: 22,
  );

  static const subTitleChartG = TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.normal,
    fontSize: 17,
  );

  static decorationTFLogin(String helperText){
    //bool _validate = false;
    return InputDecoration(
      contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
      helperText: helperText,
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Color(0xFFDBDBDB), width: 3.0),
      ),
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Color(0xff006341),width: 3.0),
      ),
      //errorText: _validate ? 'Value Can\'t Be Empty' : null
    );
  }
  static decorationTFAdd(String helperText){
    //bool _validate = false;
    return InputDecoration(
      contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
      helperText: helperText,
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Color.fromRGBO(9, 99, 141, 1), width: 5.0),
      ),
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Color.fromRGBO(9, 99, 141, 1),width: 5.0),
      ),
      //errorText: _validate ? 'Value Can\'t Be Empty' : null
    );
  }

  static titleLogin(String word){
    return Expanded(

      child: Text(word, textAlign: TextAlign.left,
          style: GlobalStyles.textTitle

      ),
    );
  }

  static lineBottom(double width){
    return BoxDecoration(
      border: Border(
        bottom: BorderSide(color: Color(0xFFDBDBDB), width: width),
      ),
    );
  }


}
