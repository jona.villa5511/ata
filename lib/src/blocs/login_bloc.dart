import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ATA/src/blocs/ATA/ATA_bloc.dart';
import 'package:ATA/src/complements/globalVariables.dart';
import 'package:ATA/src/database/userData_database.dart';
import 'package:ATA/src/models/userData_model.dart';
import 'package:progress_dialog/progress_dialog.dart';

class LoginBloc {


  static insertData() async {
    final dbHelper = UserDataDatabase.instance;
    final countInfo = await dbHelper.queryRowCount();
    //dbHelper.deleteUser(1);
    if(countInfo != 0) {
      final infoU = await dbHelper.getDataUser('userInfo');
      final infoP = await dbHelper.getDataUser('passInfo');

      GlobalVariables.textNEmp.text = infoU[0]['userInfo'];
      GlobalVariables.textPass.text = infoP[0]['passInfo'];

      //print(infoU[0]['userInfo']);
      //print(infoP[0]['passInfo']);

    } else {
      print('sin info');
    }
  }
  static loadingI(BuildContext context, bool status) async {

    ProgressDialog pr;
    pr = ProgressDialog(

      context,

      type: ProgressDialogType.Normal,


    );
    pr.style(
        message: '                    Cargando...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 12.0, fontWeight: FontWeight.w200),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 12.0, fontWeight: FontWeight.w600)
    );
    if(status == true) {
      await pr.show();
    } else {
      await pr.hide();
    }
  }
  static Downloading(BuildContext context, bool status) async {

    ProgressDialog pr;
    pr = ProgressDialog(

      context,

      type: ProgressDialogType.Normal,


    );
    pr.style(
        message: '                    Descargando...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 12.0, fontWeight: FontWeight.w200),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 12.0, fontWeight: FontWeight.w600)
    );
    if(status == true) {
      await pr.show();
    } else {
      await pr.hide();
    }
  }
  static login(BuildContext context,int opcion, int olvido){
    if(GlobalVariables.textNEmp.text.length == 0  || GlobalVariables.textPass.text.length == 0){
      print('ingresa los datos');
      showDialog(context: context, builder: (context) => AlertDialog(content: Text('Favor ingresar todos los datos')));

    } else {


      loadingI(context, true);
      ImcRBloc.validateUserLogin(context,opcion, olvido);

    }
  }
  static loginUpdatePass(BuildContext context){
    if( GlobalVariables.textPassNew.text.length == 0 || GlobalVariables.textPassConfir.text.length == 0){
      print('ingresa los datos');
      showDialog(context: context, builder: (context) => AlertDialog(content: Text('Favor ingresar todos los datos')));

    } else {
      //LoginBloc.addUserData();
      loadingI(context, true);
      ImcRBloc.updateUserLogin(context);

    }
  }
  static loginUpdatePassReset(BuildContext context){
    if( GlobalVariables.textPassNewReset.text.length == 0 || GlobalVariables.textPassConfirReset .text.length == 0){
      print('ingresa los datos');
      showDialog(context: context, builder: (context) => AlertDialog(content: Text('Favor ingresar todos los datos')));

    } else {
      //LoginBloc.addUserData();
      loadingI(context, true);
      ImcRBloc.updateUserResetLogin(context);

    }
  }
  static envioMailReset(BuildContext context){
    if( GlobalVariables.userReset.text.length == 0){
      print('ingresa los datos');
      showDialog(context: context, builder: (context) => AlertDialog(content: Text('Favor ingresar el usuario')));

    } else {
      //LoginBloc.addUserData();
      loadingI(context, true);
      ImcRBloc.resetPass(context);

      loadingI(context, true);

      print('asas');
      print('asa');
      LoginBloc.loadingI(context,false);
    }
  }

  static addUserData() async {
    final dbHelper = UserDataDatabase.instance;

    final count = await dbHelper.queryRowCount();
    if(count == 0) {
      final us1 = UserDataModel(
        id: 0,
        userInfo: GlobalVariables.textNEmp.text,//'84574',
        passInfo: GlobalVariables.textPass.text,//GS2020',
      );

      await dbHelper.insertUser(us1);

      print('insert objet Us in DB');
    } else {
      print('Sin datos nuevos para Us-bd');
    }

  }

}