import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'src/app.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp,DeviceOrientation.portraitDown])
      .then((_){
    runApp(App());
  });
}